# -*- coding: utf-8 -*-
##############################################################################
#
#    Development by Erdenebold
#    Copyright (C) 2014 Erdenebold. All Rights Reserved
#
#    Email : erdenebold10@gmail.com
#    Phone : 976 + 96693490
#
##############################################################################
from openerp.osv import fields, osv
from openerp import tools
from openerp.osv.orm import except_orm
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from datetime import date
import time, datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
from openerp.osv import fields, osv, expression


class product_template(osv.osv):
    _inherit = 'product.template'
    
    _columns = {
                'loss_day':fields.float(u'Алдангийн хоног'),
                'loss_percent':fields.float(u'Алдангийн хувь'),
                'local_type':fields.selection([('counter',u'Тоолууртай эсэх'),
                                               ('day',u'Хоногоор эсэх /Айл өрх/'),
                                               ('com_day',u'Хоногоор эсэх /ААН/'),
                                               ('elect',u'Цахилгаан'),
                                               ],u'Төрөл'),
                'warm_loss_per':fields.float(u'Шугамын дулааны алдагдлын хувь'),
                'per':fields.float(u'Ккал үржих коэф'),
                'is_ztb':fields.boolean(u'ЦТП эсэх'),
                
        }
    _defaults = {
                'loss_day':40.0,
                'loss_percent':0.5,
                'type':'service',
                    }
    
    