# -*- coding: utf-8 -*-
##############################################################################
#
#    Development by Erdenebold
#    Copyright (C) 2014 Erdenebold. All Rights Reserved
#
#    Email : erdenebold10@gmail.com
#    Phone : 976 + 96693490
#
##############################################################################
from openerp.osv import fields, osv
from openerp import tools
from openerp.osv.orm import except_orm
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import calendar
import time, datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
from openerp.osv import fields, osv, expression
import openerp.addons.decimal_precision as dp
from openerp import SUPERUSER_ID

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class sale_order(osv.osv):
    _inherit = 'sale.order'
    
    def _amount_line_tax(self, cr, uid, line, context=None):
        val = 0.0
        line_obj = self.pool['sale.order.line']
        qty = line_obj._calc_line_quantity(cr, uid, line, context=context)
        if line.order_id.partner_id.type == 'work' or line.order_id.partner_id.type == 'self':
            price = (line.price_subtotal)/qty
        else:
            price = (line.price_subtotal)/qty
        for c in self.pool['account.tax'].compute_all(
                cr, uid, line.tax_id, price, qty, line.product_id,
                line.order_id.partner_id)['taxes']:
            val += c.get('amount', 0.0)
        return val

    def _amount_all_wrapper(self, cr, uid, ids, field_name, arg, context=None):
         """ Wrapper because of direct method passing as parameter for function fields """
         return self._amount_all(cr, uid, ids, field_name, arg, context=context)


    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_untaxed': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
            }
            val = val1 = val2 = 0.0
            cur = order.pricelist_id.currency_id
            for line in order.order_line:
                if line.order_id.partner_id.type == 'nat_company' or line.order_id.partner_id.type == 'company':
                    if line.product_id.local_type =='elect':
                        val1 += line.price_subtotal+line.receipt_amount
                    else:
                        if order.partner_id.is_ail == True:
                            val1 += line.price_subtotal
                        else:
                            val1 += line.price_subtotal+line.doc_amount
                else:
                    val1 += line.price_subtotal
                val += self._amount_line_tax(cr, uid, line, context=context)
            res[order.id]['amount_tax'] = cur_obj.round(cr, uid, cur, val)
            res[order.id]['amount_untaxed'] = cur_obj.round(cr, uid, cur, val1)
            res[order.id]['amount_total'] = res[order.id]['amount_untaxed'] + res[order.id]['amount_tax']
        return res

    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('sale.order.line').browse(cr, uid, ids, context=context):
            result[line.order_id.id] = True
        return result.keys()
    
    def _get_days(self, cr, uid, ids, name, args, context=None):
        res = {}
        days = ''
        for inv in self.browse(cr, uid, ids, context):
            if inv.state == 'progress':
                if inv.date_order:
                    before = datetime.strptime(inv.date_order,'%Y-%m-%d %H:%M:%S')
                    now = datetime.now() 
                    day = before - now
                    days = float(day.days)
                    if days < 0.0:
                        days = days*(-1)
                        if days < 40:
                            days = 0
                        if days > 40:
                            days = days -40

            res[inv.id] = days
        return res
    
    _columns = {
                'inspector_id':fields.many2one('hr.employee',u'Байцаагч'),

                'amount_untaxed': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Untaxed Amount',
           store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
           },
            multi='sums', help="The amount without tax.", track_visibility='always'),
        'amount_tax': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Taxes',
           store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
           },
           multi='sums', help="The tax amount."),
        'amount_total': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Total',
           store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
           },
           multi='sums', help="The total amount."),
                'ztb_id':fields.many2one('product.product',u'ЦТП',domain=[('is_ztb', '=', True)]),
                 'day':fields.function(_get_days, type='integer',string=u'Алдангийн хоног'),
                
        }
    
    def action_button_confirm(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        #self.signal_workflow(cr, uid, ids, 'order_confirm')
        
        if context.get('send_email'):
            self.force_quotation_send(cr, uid, ids, context=context)
        self.write(cr, uid, ids, {'state':'manual'})
        return True
    
    def action_cancel(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        sale_order_line_obj = self.pool.get('sale.order.line')
        account_invoice_obj = self.pool.get('account.invoice')
        for sale in self.browse(cr, uid, ids, context=context):
            for inv in sale.invoice_ids:
                if inv.state not in ('draft', 'cancel'):
                    raise osv.except_osv(
                        _('Cannot cancel this sales order!'),
                        _('First cancel all invoices attached to this sales order.'))
                account_invoice_obj.unlink(cr, uid,inv.id)
            line_ids = [l.id for l in sale.order_line if l.state != 'cancel']
            sale_order_line_obj.button_cancel(cr, uid, line_ids, context=context)
        self.write(cr, uid, ids, {'state': 'draft'})
        return True
    
    def create_invoice_view(self, cr, uid, ids, context=None):
        account_invoice_obj = self.pool.get('account.invoice')
        account_invoice_line_obj = self.pool.get('account.invoice.line')
        
        
        inv_ids = False
        sale = self.browse(cr, uid, ids)
        cr.execute("select invoice_id from sale_order_invoice_rel where order_id = %s"%sale.id)
        account_rel_id = cr.fetchone()
        if account_rel_id:
            raise osv.except_osv(u'Анхааруулга',u'Борлуулалтын нэхэмжлэл үүссэн байгаа тул ахиж үүсгэхгүй!')
        invoice_id = account_invoice_obj.create(cr, uid, {
                                            'name': sale.client_order_ref or sale.name,
                                            'origin': sale.name,
                                            'type': 'out_invoice',
                                            'reference': False,
                                            'account_id': sale.partner_id.property_account_receivable.id,
                                            'partner_id': sale.partner_invoice_id.id,
                                            'currency_id': sale.pricelist_id.currency_id.id,
                                            'comment': sale.note,
                                            'tstp_id':sale.ztb_id.id,
                                            'payment_term': sale.payment_term.id,
                                            'fiscal_position': sale.fiscal_position.id or sale.partner_id.property_account_position.id,
                                            'section_id': sale.section_id.id,
                                                })
        for line in sale.order_line:
            account_id = False
            if not line.invoiced:
                if not account_id:
                    if line.product_id:
                        account_id = line.product_id.property_account_income.id
                        if not account_id:
                            account_id = line.product_id.categ_id.property_account_income_categ.id
                        if not account_id:
                            raise osv.except_osv(_('Error!'),
                                    _('Please define income account for this product: "%s" (id:%d).') % \
                                        (line.product_id.name, line.product_id.id,))
                    else:
                        prop = self.pool.get('ir.property').get(cr, uid,
                                'property_account_income_categ', 'product.category',
                                context=context)
                        account_id = prop and prop.id or False
            fpos = line.order_id.fiscal_position or False
            account_id = self.pool.get('account.fiscal.position').map_account(cr, uid, fpos, account_id)
            account_invoice_line_obj.create(cr, uid, {
                                                        'invoice_id':invoice_id,
                                                        'name': line.name,
                                                        'sequence': line.sequence,
                                                        'origin': line.order_id.name,
                                                        'account_id': account_id,
                                                        'price_unit': line.price_unit,
                                                        'warm_day':line.warm_day,
                                                        'warmed_day':line.warmed_day,
                                                        'receipt_amount':line.receipt_amount,
                                                        'loss_amount':line.loss_amount,
                                                        'more_and_less_amount':line.more_and_loss_amount,
                                                        'line_loss_amount':line.line_loss_amount,
                                                        'subtotal_amount':line.price_subtotal-line.receipt_amount-line.doc_amount-line.line_loss_amount,
                                                        'diff_amount':line.diff_amount,
                                                        'doc_amount':line.doc_amount,
                                                        'quantity': line.product_uom_qty,
                                                        'discount': line.discount,
                                                        'uos_id': line.product_uos.id,
                                                        
                                                        'product_id': line.product_id.id or False,
                                                        'invoice_line_tax_id': [(6, 0, [x.id for x in line.tax_id])],
                                                        'account_analytic_id': line.order_id.project_id and line.order_id.project_id.id or False,
                                                        })

        self.write(cr, uid, ids, {'invoice_ids': [(4, invoice_id)]}, context=context)
        #if context.get('open_invoices', False):
        #    return self.open_invoices( cr, uid, ids, inv_ids, context=context)
        return self.open_invoices( cr, uid, ids, [invoice_id], context=context)
    
    def open_invoices(self, cr, uid, ids, invoice_ids, context=None):
        """ open a view on one of the given invoice_ids """
        ir_model_data = self.pool.get('ir.model.data')
        form_res = ir_model_data.get_object_reference(cr, uid, 'account', 'invoice_form')
        form_id = form_res and form_res[1] or False
        tree_res = ir_model_data.get_object_reference(cr, uid, 'account', 'invoice_tree')
        tree_id = tree_res and tree_res[1] or False

        return {
            'name': _('Advance Invoice'),
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'account.invoice',
            'res_id': invoice_ids[0],
            'view_id': False,
            'views': [(form_id, 'form'), (tree_id, 'tree')],
            'context': "{'type': 'out_invoice'}",
            'type': 'ir.actions.act_window',
        }
    
    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        if not part:
            return {'value': {'partner_invoice_id': False, 'partner_shipping_id': False,  'payment_term': False, 'fiscal_position': False}}

        part = self.pool.get('res.partner').browse(cr, uid, part, context=context)
        addr = self.pool.get('res.partner').address_get(cr, uid, [part.id], ['delivery', 'invoice', 'contact'])
        pricelist = part.property_product_pricelist and part.property_product_pricelist.id or False
        invoice_part = self.pool.get('res.partner').browse(cr, uid, addr['invoice'], context=context)
        payment_term = invoice_part.property_payment_term and invoice_part.property_payment_term.id or False
        dedicated_salesman = part.user_id and part.user_id.id or uid
        val = {
            'partner_invoice_id': addr['invoice'],
            'partner_shipping_id': addr['delivery'],
            'payment_term': payment_term,
            'user_id': dedicated_salesman,
            'client_order_ref':part.code,
            'ztb_id':part.tstp_id.id,
        }
        delivery_onchange = self.onchange_delivery_id(cr, uid, ids, False, part.id, addr['delivery'], False,  context=context)
        val.update(delivery_onchange['value'])
        if pricelist:
            val['pricelist_id'] = pricelist
        if not self._get_default_section_id(cr, uid, context=context) and part.section_id:
            val['section_id'] = part.section_id.id
        sale_note = self.get_salenote(cr, uid, ids, part.id, context=context)
        if sale_note: val.update({'note': sale_note})  
        return {'value': val}
    
class sale_order_line(osv.osv):
    _inherit = 'sale.order.line'
    
    
    def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
        """Prepare the dict of values to create the new invoice line for a
           sales order line. This method may be overridden to implement custom
           invoice generation (making sure to call super() to establish
           a clean extension chain).

           :param browse_record line: sale.order.line record to invoice
           :param int account_id: optional ID of a G/L account to force
               (this is used for returning products including service)
           :return: dict of values to create() the invoice line
        """
        res = {}
        if not line.invoiced:
            if not account_id:
                if line.product_id:
                    account_id = line.product_id.property_account_income.id
                    if not account_id:
                        account_id = line.product_id.categ_id.property_account_income_categ.id
                    if not account_id:
                        raise osv.except_osv(_('Error!'),
                                _('Please define income account for this product: "%s" (id:%d).') % \
                                    (line.product_id.name, line.product_id.id,))
                else:
                    prop = self.pool.get('ir.property').get(cr, uid,
                            'property_account_income_categ', 'product.category',
                            context=context)
                    account_id = prop and prop.id or False
            uosqty = self._get_line_qty(cr, uid, line, context=context)
            uos_id = self._get_line_uom(cr, uid, line, context=context)
            pu = 0.0
            if uosqty:
                pu = round(line.price_unit * line.product_uom_qty / uosqty,
                        self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Price'))
            fpos = line.order_id.fiscal_position or False
            account_id = self.pool.get('account.fiscal.position').map_account(cr, uid, fpos, account_id)
            if not account_id:
                raise osv.except_osv(_('Error!'),
                            _('There is no Fiscal Position defined or Income category account defined for default properties of Product categories.'))
            res = {
                'name': line.name,
                'sequence': line.sequence,
                'origin': line.order_id.name,
                'account_id': account_id,
                'price_unit': pu,
                'warm_day':line.warm_day,
                'warmed_day':line.warmed_day,
                'loss_amount':line.loss_amount,
                'more_and_loss_amount':line.more_and_loss_amount,
                'line_loss_amount':line.line_loss_amount,
                'diff_amount':line.diff_amount,
                'doc_amount':line.doc_amount,
                'receipt_amount':line.receipt_amount,
                'tstp_id':line.order_id.ztb_id.id,
                'quantity': uosqty,
                'discount': line.discount,
                'uos_id': uos_id,
                'product_id': line.product_id.id or False,
                'invoice_line_tax_id': [(6, 0, [x.id for x in line.tax_id])],
                'account_analytic_id': line.order_id.project_id and line.order_id.project_id.id or False,
            }
        return res
    
    def _calc_line_base_price(self, cr, uid, line, context=None):
        total = 0.0
        sub = 0.0
        if line.product_id.local_type == 'day' or line.product_id.local_type == 'com_day':
            if line.order_id.partner_id.type == 'work' or line.order_id.partner_id.type == 'self':
                total = (line.product_uom_qty*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))/line.warm_day*line.warmed_day+line.doc_amount+line.loss_amount
            else:
                if line.order_id.partner_id.is_ail == True:
                    total = (line.product_uom_qty*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))/line.warm_day*line.warmed_day+line.loss_amount+line.doc_amount+line.receipt_amount
                else:
                    total = (line.product_uom_qty*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))/line.warm_day*line.warmed_day+line.loss_amount+line.receipt_amount
        elif line.product_id.local_type == 'counter':
            qty = (line.warmed_day-line.warm_day)*line.product_id.per
            if line.order_id.partner_id.type == 'work' or line.order_id.partner_id.type == 'self':
                total = ((line.price_unit * (1 - (line.discount or 0.0) / 100.0))*qty)+line.loss_amount
            else:
                if line.order_id.partner_id.is_ail == True:
                    total = ((line.price_unit * (1 - (line.discount or 0.0) / 100.0))*qty)+line.loss_amount+line.doc_amount+line.receipt_amount
                else:
                    total = ((line.price_unit * (1 - (line.discount or 0.0) / 100.0))*qty)+line.loss_amount
            sub = (total/100.0)*line.product_id.warm_loss_per
            total = total+sub+line.receipt_amount
        elif line.product_id.local_type == 'elect':
            total = line.price_unit
        else:
            total = line.price_unit
        return total
    
    def _calc_line_quantity(self, cr, uid, line, context=None):
        return line.product_uom_qty
    
    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        res = {}
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            price = self._calc_line_base_price(cr, uid, line, context=context)
            qty = self._calc_line_quantity(cr, uid, line, context=context)
            if line.order_id.partner_id.is_day == True or line.order_id.partner_id.is_size == True:
                if line.order_id.partner_id.square_amount == 0.0:
                     raise osv.except_osv(_(u'Анхааруулга'), _(u'%s харилцагчийн талбайг оруулаагүй байгаа тул оруулана уу?'%line.order_id.partner_id.name))
            taxes = tax_obj.compute_all(cr, uid, line.tax_id, price/qty, qty,
                                        line.product_id,
                                        line.order_id.partner_id)
            cur = line.order_id.pricelist_id.currency_id
            res[line.id] = cur_obj.round(cr, uid, cur, taxes['total'])
        return res
    
    def _get_more_loss_amount(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids):
            pre_id = self.pool.get('account.invoice').search(cr, uid, [('state','not in', ('draft','cancel','sent','done')),('date_invoice','<',line.order_id.date_order),('partner_id','=',line.order_id.partner_id.id)])
            if pre_id !=[] and pre_id !=False:
                if line.product_id.local_type == 'counter' or line.product_id.local_type == 'day' or line.product_id.local_type == 'com_day' or line.product_id.local_type == 'elect':
                    res[line.id] = line.order_id.partner_id.credit-line.order_id.partner_id.debit
                else:
                    res[line.id] = 0.0
        return res
    
    def _get_line_loss_amount(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        total = 0.0
        for line in self.browse(cr, uid, ids):
            if line.product_id.local_type == 'counter':
                total = ((line.price_unit*line.product_uom_qty)/100)*line.product_id.warm_loss_per
            else:
                total = 0.0
            res[line.id] = total
        return res
    
    def _get_diff(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids):
            if line.product_id.local_type == 'counter':
                res[line.id] = line.warmed_day-line.warm_day
            else:
                res[line.id] = 0.0
        return res
    def _get_loss_amount(self,cr, uid, ids, field_name, arg, context=None):
        res = {}
        sale_obj = self.pool.get('sale.order')
        account_obj = self.pool.get('account.invoice')
        DATE_FORMAT = "%Y-%m-%d"
        total = 0.0
        for line in self.browse(cr, uid, ids):
            if line.product_id.local_type == 'counter' or line.product_id.local_type == 'day' or line.product_id.local_type == 'com_day' or line.product_id.local_type == 'elect':
                invoice_id = account_obj.search(cr, uid, [('partner_id','=',line.order_id.partner_id.id),('date_invoice','<',line.order_id.date_order),('state','not in',('draft','done','cancel'))],order='date_invoice asc',limit=1)
                if invoice_id !=[]:
                    new_day = 0.0
                    invoice = account_obj.browse(cr, uid, invoice_id)
                    before = datetime.strptime(invoice.date_invoice,'%Y-%m-%d')
                    now = datetime.strptime(line.order_id.date_order, '%Y-%m-%d %H:%M:%S')
                    day = before - now
                    if float(day.days)< 0.0:
                        new_day = float(day.days)*(-1)
                    if new_day >= 40.0:
                        loss_day = new_day-40.0
                        total = (line.more_and_loss_amount*line.loss_day)/100.0*line.product_id.loss_percent
                else:
                     total = 0.0
            res[line.id] = total
        return res
    
    _columns = {
                'loss_amount':fields.function(_get_loss_amount,type='float',string=u'Алданги'),
                'warm_day':fields.float(u'Халаах хоног/заалт'),
                'warmed_day':fields.float(u'Халаасан хоног/Заалт'),
                'more_and_loss_amount':fields.function(_get_more_loss_amount,type='float',string=u'Илүү дутуу',method=True,  store=True),
                'price_subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
                'diff_amount':fields.function(_get_diff, string=u'Зөрүү'),
                'loss_day':fields.float(u'Алданги хоног'),
                'line_loss_amount':fields.function(_get_line_loss_amount, string=u'Шугамын алдагдал'),
                'doc_amount':fields.float(u'Суурь хураамж'),
                'receipt_amount':fields.float(u'Баримт'),
                'koff_amount':fields.float(u'Джоул-ыг ккал-руу шилжүүлэх коэф'),
}
    
    def diff_amount_change(self, cr, uid, ids, st, ed, qty,context=None):
        res = {}
        if qty != 0.0:
            res = {'value':{'product_uom_qty':qty}}
        else:
            res = {'value':{'product_uom_qty':1.0}}
        if ed:
            res['value'].update({'product_uom_qty':((ed-st)*qty)})
        return res
    
    def button_cancel(self, cr, uid, ids, context=None):
        lines = self.browse(cr, uid, ids, context=context)
        for line in lines:
            if line.invoiced:
                raise osv.except_osv(_('Invalid Action!'), _('You cannot cancel a sales order line that has already been invoiced.'))
        procurement_obj = self.pool['procurement.order']
        procurement_obj.cancel(cr, uid, sum([l.procurement_ids.ids for l in lines], []), context=context)
        return self.write(cr, uid, ids, {'state': 'draft'})
    
    def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
            uom=False, qty_uos=0, uos=False, name='', partner_id=False,
            lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None):
        context = context or {}
        lang = lang or context.get('lang', False)
        if not partner_id:
            raise osv.except_osv(_('No Customer Defined!'), _('Before choosing a product,\n select a customer in the sales form.'))
        warning = False
        product_uom_obj = self.pool.get('product.uom')
        partner_obj = self.pool.get('res.partner')
        product_obj = self.pool.get('product.product')
        partner = partner_obj.browse(cr, uid, partner_id)
        lang = partner.lang
        context_partner = context.copy()
        context_partner.update({'lang': lang, 'partner_id': partner_id})

        if not product:
            return {'value': {'th_weight': 0,
                'product_uos_qty': qty}, 'domain': {'product_uom': [],
                   'product_uos': []}}
        if not date_order:
            date_order = time.strftime(DEFAULT_SERVER_DATE_FORMAT)

        result = {}
        warning_msgs = ''
        product_obj = product_obj.browse(cr, uid, product, context=context_partner)

        uom2 = False
        if uom:
            uom2 = product_uom_obj.browse(cr, uid, uom)
            if product_obj.uom_id.category_id.id != uom2.category_id.id:
                uom = False
        if uos:
            if product_obj.uos_id:
                uos2 = product_uom_obj.browse(cr, uid, uos)
                if product_obj.uos_id.category_id.id != uos2.category_id.id:
                    uos = False
            else:
                uos = False

        fpos = False
        if not fiscal_position:
            fpos = partner.property_account_position or False
        else:
            fpos = self.pool.get('account.fiscal.position').browse(cr, uid, fiscal_position)
        if update_tax: #The quantity only have changed
            # The superuser is used by website_sale in order to create a sale order. We need to make
            # sure we only select the taxes related to the company of the partner. This should only
            # apply if the partner is linked to a company.
            if uid == SUPERUSER_ID and context.get('company_id'):
                taxes = product_obj.taxes_id.filtered(lambda r: r.company_id.id == context['company_id'])
            else:
                taxes = product_obj.taxes_id
            result['tax_id'] = self.pool.get('account.fiscal.position').map_tax(cr, uid, fpos, taxes)

        if not flag:
            result['name'] = self.pool.get('product.product').name_get(cr, uid, [product_obj.id], context=context_partner)[0][1]
            if product_obj.description_sale:
                result['name'] += '\n'+product_obj.description_sale
        domain = {}
        if (not uom) and (not uos):
            result['product_uom'] = product_obj.uom_id.id
            if product_obj.uos_id:
                result['product_uos'] = product_obj.uos_id.id
                result['product_uos_qty'] = qty * product_obj.uos_coeff
                uos_category_id = product_obj.uos_id.category_id.id
            else:
                result['product_uos'] = False
                result['product_uos_qty'] = qty
                uos_category_id = False
            result['th_weight'] = qty * product_obj.weight
            domain = {'product_uom':
                        [('category_id', '=', product_obj.uom_id.category_id.id)],
                        'product_uos':
                        [('category_id', '=', uos_category_id)]}
        elif uos and not uom: # only happens if uom is False
            result['product_uom'] = product_obj.uom_id and product_obj.uom_id.id
            result['product_uom_qty'] = qty_uos / product_obj.uos_coeff
            result['th_weight'] = result['product_uom_qty'] * product_obj.weight
        elif uom: # whether uos is set or not
            default_uom = product_obj.uom_id and product_obj.uom_id.id
            q = product_uom_obj._compute_qty(cr, uid, uom, qty, default_uom)
            if product_obj.uos_id:
                result['product_uos'] = product_obj.uos_id.id
                result['product_uos_qty'] = qty * product_obj.uos_coeff
            else:
                result['product_uos'] = False
                result['product_uos_qty'] = qty
            result['th_weight'] = q * product_obj.weight        # Round the quantity up

        if not uom2:
            uom2 = product_obj.uom_id
        # get unit price

        if not pricelist:
            warn_msg = _('You have to select a pricelist or a customer in the sales form !\n'
                    'Please set one before choosing a product.')
            warning_msgs += _("No Pricelist ! : ") + warn_msg +"\n\n"
        else:
            ctx = dict(
                context,
                uom=uom or result.get('product_uom'),
                date=date_order,
            )

            price = self.pool.get('product.pricelist').price_get(cr, uid, [pricelist],
                    product, qty or 1.0, partner_id, ctx)[pricelist]
            
            if price is False:
                warn_msg = _("Cannot find a pricelist line matching this product and quantity.\n"
                        "You have to change either the product, the quantity or the pricelist.")

                warning_msgs += _("No valid pricelist line found ! :") + warn_msg +"\n\n"
            else:
                if update_tax:
                    price = self.pool['account.tax']._fix_tax_included_price(cr, uid, price, taxes, result['tax_id'])
                result.update({'price_unit': price})
                if context.get('uom_qty_change', False):
                    values = {'price_unit': price}
                    if result.get('product_uos_qty'):
                        values['product_uos_qty'] = result['product_uos_qty']
                    return {'value': values, 'domain': {}, 'warning': False}
        
        if partner.is_day == True or partner.is_size == True:
            if product_obj.local_type == 'day' or product_obj.local_type == 'com_day':
                result['product_uom_qty'] = partner.square_amount
        else:
            if product_obj.local_type == 'counter':
                result['koff_amount'] = product_obj.per
        if partner.type == 'nat_company' or partner.type =='company':
            if partner.is_ail == True:
                 if product_obj.local_type == 'day' or product_obj.local_type == 'counter' or product_obj.local_type == 'com_day':
                    if 0 < partner.square_amount <= 40.0:
                        result['doc_amount'] = 3000.0
                    elif 40.0 < partner.square_amount < 80.0:
                        result['doc_amount'] = 5000.0
                    else:
                        result['doc_amount'] = 10000.0
            if product_obj.local_type == 'day' or product_obj.local_type == 'counter' or product_obj.local_type == 'com_day':
                result['receipt_amount'] = 90.91
            else:
                result['receipt_amount'] = 0.0
        else:
            if partner.is_day == True or partner.is_size == True:
                if product_obj.local_type == 'day':
                    if 0 < partner.square_amount <= 40.0:
                        result['doc_amount'] = 3000
                    elif 40.0 < partner.square_amount < 80.0:
                        result['doc_amount'] = 5000
                    else:
                        result['doc_amount'] = 10000
        if product_obj.local_type =='day' or product_obj.local_type == 'com_day':
            now = date_order
            days = calendar.monthrange(int(now[:4]), int(now[5:7]))[1]
            result['warm_day'] = days
        if warning_msgs:
            warning = {
                       'title': _('Configuration Error!'),
                       'message' : warning_msgs
                    }
        return {'value': result, 'domain': domain, 'warning': warning}