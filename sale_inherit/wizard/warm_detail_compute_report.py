# -*- coding: utf-8 -*-
##############################################################################
#
#    Tengersoft, Enterprise Management Solution    
#    Copyright (C) 2016-2020 Tengersoft Co.,ltd (<http://www.tengersoft.mn>). All Rights Reserved
#
#    Address : Suhkbaatar District, National ITPark, Tengersoft LLC
#    Email : info@tengersoft.mn
#    Phone : 976 + 99345677
#
##############################################################################

from datetime import timedelta
from lxml import etree
from openerp.osv import fields, osv
from openerp.tools.translate import _
from operator import itemgetter
import xlrd, base64, time, datetime, xlwt, openerp.netsvc, datetime
from StringIO import StringIO

class warm_detail_compute_report(osv.osv_memory):
    """
        АЙЛ ӨРХИЙН ДУЛААНЫ ТООЦООНЫ ДЭЛГЭРЭНГҮЙ БҮРТГЭЛ
        
    """
    _inherit = "abstract.report.excel"
    _name = "warm.detail.compute.report"
    _description = ""
    
    _columns = {
        'company_id': fields.many2one('res.company', 'Company',readonly=True),
        'period_id': fields.many2one('account.period',u"Мөчлөг", required=True),
        "type":fields.selection([('self',u'Айл өрх'),
                                 ('company',u'ААН Байгууллага'),
                                 ('nat_company',u'Төсөвт Байгууллага')],u'Төрөл',required=True),
        
    }
    
    _defaults = {
        'company_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),
    }
    
    
    def get_export_data(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        form = self.browse(cr, uid, ids[0], context=context)
        datas = {'ids': context.get('active_ids', [])}
        datas['form'] = self.read(cr, uid, ids)[0]
        data = datas['form']

        company = self.pool.get('res.company').browse(cr, uid, data['company_id'][0], context=context)
        period_obj = self.pool.get('account.period')
        sale_obj = self.pool.get('account.invoice')
        partner_obj = self.pool.get('res.partner')
        
        period = period_obj.browse(cr, uid, data['period_id'][0])
        sale_id = sale_obj.search(cr, uid, [('company_id','=',company.id),('state','not in', ('cancel','draft')),('date_invoice','>=',period.date_start),('date_invoice','<=',period.date_stop)])
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('Sale report')
        styles = self.get_easyxf_styles()
        ezxf = xlwt.easyxf

        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')


        sheet.write(0, 1, u'Байгууллагын нэр: %s' % (company.name,), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 240'))
        
        
        sheet.write_merge(2, 2, 0, 3, u'Тайлант хугацаа: %s'%period.date_start[:7] , ezxf('font:bold on;align:wrap off,vert centre,horiz right;font: height 240'))
        type = u''
        invoice_obj = self.pool.get('account.invoice')
        voucher_obj = self.pool.get('account.voucher')
        if data['type'] == 'self':
            type = u'Айл өрх'
            text_title = u'АЙЛ ӨРХИЙН ДУЛААНЫ ТООЦООНЫ ДЭЛГЭРЭНГҮЙ БҮРТГЭЛ'
            rowx = 4
            sheet.write_merge(rowx, rowx+1, 0, 0, u'Д/д', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 1, 1, u'Харилцагчийн нэр', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 2, 2, u'Харилцагчийн хаяг', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 3, 3, u'Харилцагчийн код', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 4, 4, u'Талбай', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 5, 5, u'Байцаагч', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 6, 6, u'Халаах хоног /Эхний заалт', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 7, 7, u'Халаасан хоног /Эцсийн заалт', styles['heading_xf'])
            sheet.write_merge(rowx, rowx, 8, 9, u'Эхний үлдэгдэл', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 8, 8, u'Дт', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 9, 9, u'Кд', styles['heading_xf'])
            sheet.write_merge(rowx, rowx, 10, 13, u'Дт гүйлгээ', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 10, 10, u'Дулаан 5101-0300', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 11, 11, u'ҮАБО 0-40 3000 40-80 5000 80-дээс 10000', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 12, 12, u'Хэрэглээний халуун ус', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 13, 13, u'НӨАТ 3152-0000', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 14, 14, u'Алданги', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 15, 15, u'Дүн', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 16, 16, u'Хаан 5585039897', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 17, 17, u'Хас', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 18, 18, u'Төрийн', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 19, 19, u'Голомж', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 20, 20, u'Капитрон', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 21, 21, u'Капитал', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 22, 22, u'Хаан 5585384710', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 23, 23, u'ХХБанк', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 24, 24, u'ДХ хоорондын тооцоо', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 25, 25, u'Касс', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 26, 26, u'Цалингаас', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 27, 27, u'Хоорондын тооцоо', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 28, 28, u'Дүн', styles['heading_xf'])
            sheet.write_merge(rowx, rowx, 29, 30, u'Эцсийн үлдэгдэл', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 29, 29, u'Дт', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 30, 30, u'Кд', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 31, 31, u'Утасны дугаар', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 32, 32, u'Гар утас', styles['heading_xf'])
            
            
            rowx = 6
            num = 0
            
            
            partner_id = partner_obj.search(cr, uid, [('type','=',data['type'])])
            
            for part in partner_obj.browse(cr, uid, partner_id):
                num +=1
                khanbank97 = 0.0
                khasbank = 0.0
                toriinbank = 0.0
                golomtbank = 0.0
                capitronbank = 0.0
                capitalbank = 0.0
                khanbank10 = 0.0
                hhbank = 0.0
                cash = 0.0
                salary = 0.0
                dhb = 0.0
                dh = 0.0
                phone = ''
                mobile = ''
                subtotal = 0.0
                loss_amount = 0.0
                total_amount = 0.0
                total_debit = 0.0
                total_credit = 0.0
                warm_day = 0.0
                warmed_day = 0.0
                doc_amount = 0.0
                price_subtotal = 0.0
                tax_amount =0.0
                water = 0.0
                debit_amount = 0.0
                credit_amount = 0.0
                inspector_id = u''
                sheet.write(rowx, 0, '%s'%num, data_right)
                sheet.write(rowx, 1, u'%s'%part.name, data_right)
                sheet.write(rowx, 2, u'%s'%part.contract_number or '', data_right)
                sheet.write(rowx, 3, u'%s'%part.code or '', data_right)
                sheet.write(rowx, 4, u'%s'%part.square_amount, data_right)
                invoice_id = invoice_obj.search(cr, uid, [('partner_id','=',part.id),('type','=','out_invoice'),('period_id','=',period.id),('state','in',('open','paid'))])
                if invoice_id:
                    for invoice in invoice_obj.browse(cr, uid,invoice_id):
                        if invoice.inspector_id.name:
                            inspector_id = invoice.inspector_id.name
                        if invoice.partner_id.phone !=False:
                            phone = invoice.partner_id.phone
                        if invoice.partner_id.mobile !=False:
                            mobile = invoice.partner_id.mobile
                        tax_amount = invoice.amount_tax
                        for inv_line in invoice.invoice_line:
                            warm_day = inv_line.warm_day
                            warmed_day = inv_line.warmed_day
                            price_subtotal = inv_line.price_subtotal-inv_line.doc_amount-inv_line.loss_amount-inv_line.receipt_amount
                            doc_amount = inv_line.doc_amount
                            loss_amount = inv_line.loss_amount
                pre_invoice_id = invoice_obj.search(cr, uid, [('partner_id','=',part.id),('type','in',('out_invoice','in_invoice')),('period_id','<',period.id),('state','in',('open','paid'))])
                
                
                if pre_invoice_id:
                    for pre_invoice in invoice_obj.browse(cr, uid, pre_invoice_id):
                        if pre_invoice.type == 'out_invoice':
                            debit_amount +=pre_invoice.amount_total
                        else:
                            credit_amount +=pre_invoice.amount_total
                subtotal = price_subtotal+doc_amount+loss_amount+tax_amount
                
                sheet.write(rowx, 5, u'%s'%inspector_id, data_right)
                sheet.write(rowx, 6, warm_day, data_right)
                sheet.write(rowx, 7, warmed_day, data_right)
                sheet.write(rowx, 8, debit_amount, data_right)
                sheet.write(rowx, 9, credit_amount, data_right)
                sheet.write(rowx, 10, price_subtotal, data_right)
                sheet.write(rowx, 11, doc_amount, data_right)
                sheet.write(rowx, 12, water, data_right)
                sheet.write(rowx, 13, tax_amount, data_right)
                sheet.write(rowx, 14, loss_amount, data_right)
                sheet.write(rowx, 15, subtotal, data_right)
                voucher_id = voucher_obj.search(cr, uid, [('partner_id','=',part.id),('period_id','=',period.id),('state','=','posted')])
                now_invoice_id = invoice_obj.search(cr, uid, [('partner_id','=',part.id),('type','=','in_invoice'),('period_id','=',period.id),('state','=','open')])
                if now_invoice_id:
                    for lis in invoice_obj.browse(cr, uid, now_invoice_id):
                        if lis.journal_id.code =='khan7':
                            khanbank97 +=lis.amount_total
                            
                        elif lis.journal_id.code == 'khas':
                            khasbank +=lis.amount_total
                            
                        elif lis.journal_id.code == 'torin':
                           toriinbank +=lis.amount_total 

                        elif lis.journal_id.code == 'golom':
                           golomtbank +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'capit':
                           capitronbank +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'catal':
                           capitalbank +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'khan0':
                           khanbank10 +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'hhban':
                           hhbank +=lis.amount_total 
                        
                        elif lis.journal_id.code == 'cash':
                            cash +=lis.amount_total
                    
                        elif lis.journal_id.code == 'dhb':
                            dhb +=lis.amount_total
                            
                        elif lis.journal_id.code == 'dh':
                            dh +=lis.amount_total
                
                if voucher_id:
                    for voucher in voucher_obj.browse(cr, uid,voucher_id):
                        
                        if voucher.journal_id.code =='khan7':
                            khanbank97 +=voucher.amount
                            
                        elif voucher.journal_id.code == 'khas':
                            khasbank +=voucher.amount
                            
                        elif voucher.journal_id.code == 'torin':
                           toriinbank +=voucher.amount 

                        elif voucher.journal_id.code == 'golom':
                           golomtbank +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'capit':
                           capitronbank +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'catal':
                           capitalbank +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'khan0':
                           khanbank10 +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'hhban':
                           hhbank +=voucher.amount 
                        
                        elif voucher.journal_id.code == 'cash':
                            cash +=voucher.amount
                    
                        elif voucher.journal_id.code == 'dhb':
                            dhb +=voucher.amount
                            
                        elif voucher.journal_id.code == 'dh':
                            dh +=voucher.amount
                        
                total_amount = khanbank97+khasbank+toriinbank+golomtbank+capitronbank+capitalbank+khanbank10+dhb+cash+hhbank+dh
                total = (debit_amount +subtotal)-(credit_amount+total_amount)
                if total > 0.0:
                    total_debit = total
                else:
                    total_credit = total*(-1)
                sheet.write(rowx, 16, khanbank97,data_right)
                sheet.write(rowx, 17, khasbank,data_right)
                sheet.write(rowx, 18, toriinbank,data_right)
                sheet.write(rowx, 19, golomtbank,data_right)
                sheet.write(rowx, 20, capitronbank,data_right)
                sheet.write(rowx, 21, capitalbank,data_right)
                sheet.write(rowx, 22, khanbank10,data_right)
                sheet.write(rowx, 23, hhbank,data_right)
                sheet.write(rowx, 24, dhb,data_right)
                sheet.write(rowx, 25, cash,data_right)
                sheet.write(rowx, 26, salary,data_right)
                sheet.write(rowx, 27, dh,data_right)
                sheet.write(rowx, 28, total_amount,data_right)
                sheet.write(rowx, 29, total_debit,data_right)
                sheet.write(rowx, 30, total_credit,data_right)
                sheet.write(rowx, 31, phone,data_right)
                sheet.write(rowx, 32, mobile,data_right)
                
                rowx +=1
            
            
            
            
            
        elif data['type'] =='company':
            type = u'ААН Байгууллага'
            text_title = u'ААН БАЙГУУЛЛАГЫН ДУЛААНЫ ТООЦООНЫ ДЭЛГЭРЭНГҮЙ БҮРТГЭЛ'
            rowx = 4
            sheet.write_merge(rowx, rowx+1, 0, 0, u'Д/д', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 1, 1, u'Байгууллага нэр', styles['heading_xf'])            
            sheet.write_merge(rowx, rowx+1, 2, 2, u'Талбай', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 3, 3, u'Баримтын №', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 4, 4, u'Байцаагч', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 5, 5, u'Халаах хоног /Эхний заалт', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 6, 6, u'Халаасан хоног /Эцсийн заалт', styles['heading_xf'])
            sheet.write_merge(rowx, rowx, 7, 8, u'Эхний үлдэгдэл', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 7, 7, u'Дт', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 8, 8, u'Кд', styles['heading_xf'])
            sheet.write_merge(rowx, rowx, 9, 11, u'Дт гүйлгээ', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 9, 9, u'Бичилт', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 10, 10, u'НӨАТ', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 11, 11, u'Маягт', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 12, 12, u'Алданги', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 13, 13, u'Дүн', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 14, 14, u'Хаан 5585039897', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 15, 15, u'Хас', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 16, 16, u'Төрийн', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 17, 17, u'Голомж', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 18, 18, u'Капитрон', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 19, 19, u'Капитал', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 20, 20, u'Хаан 5585384710', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 21, 21, u'ДХ хоорондын тооцоо', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 22, 22, u'Касс', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 23, 23, u'ХХБанк', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 24, 24, u'Хоорондын тооцоо', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 25, 25, u'Дүн', styles['heading_xf'])
            sheet.write_merge(rowx, rowx, 26, 27, u'Эцсийн үлдэгдэл', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 26, 26, u'Дт', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 27, 27, u'Кд', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 28, 28, u'Утасны дугаар', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 29, 29, u'Гар утас', styles['heading_xf'])
            
            
            
            rowx = 6
            num = 0
            
            partner_id = partner_obj.search(cr, uid, [('type','=',data['type'])])
            
            for part in partner_obj.browse(cr, uid, partner_id):
                num +=1
                khanbank97 = 0.0
                khasbank = 0.0
                toriinbank = 0.0
                golomtbank = 0.0
                capitronbank = 0.0
                capitalbank = 0.0
                khanbank10 = 0.0
                hhbank = 0.0
                cash = 0.0
                salary = 0.0
                dhb = 0.0
                dh = 0.0
                total_amount = 0.0
                phone = ''
                mobile = ''
                inspector = u''
                total_credit = 0.0
                total_debit = 0.0
                debit_amount = 0.0
                credit_amount = 0.0
                warm_day = 0.0
                warmed_day = 0.0
                receipt_amount = 0.0
                price_subtotal = 0.0
                tax_amount = 0.0
                loss_amount = 0.0
                subtotal = 0.0
                sheet.write(rowx, 0, '%s'%num, data_right)
                sheet.write(rowx, 1, u'%s'%part.name, data_right)
                sheet.write(rowx, 2, u'%s'%part.square_amount, data_right)
                invoice_id = invoice_obj.search(cr, uid, [('partner_id','=',part.id),('type','=','out_invoice'),('period_id','=',period.id),('state','in',('open','paid'))])
                if invoice_id:
                    for invoice in invoice_obj.browse(cr, uid,invoice_id):
                        sheet.write(rowx, 3, u'%s'%invoice.name, data_right)
                        if invoice.inspector_id.name: 
                            inspector = invoice.inspector_id.name
                        sheet.write(rowx, 4, u'%s'%inspector, data_right)
                        if invoice.partner_id.phone:
                            phone = invoice.partner_id.phone
                        if invoice.partner_id.mobile:
                            phone = invoice.partner_id.mobile
                        
                        for inv_line in invoice.invoice_line:
                            warm_day = inv_line.warm_day
                            warmed_day =inv_line.warmed_day
                            price_subtotal = inv_line.price_subtotal-inv_line.doc_amount-inv_line.loss_amount
                            receipt_amount = inv_line.receipt_amount
                            loss_amount = inv_line.loss_amount
                        tax_amount = invoice.amount_tax
                pre_invoice_id = invoice_obj.search(cr, uid, [('partner_id','=',part.id),('type','in',('out_invoice','in_invoice')),('period_id','<',period.id),('state','in',('open','paid'))])
                
                if pre_invoice_id:
                    for pre_invoice in invoice_obj.browse(cr, uid, pre_invoice_id):
                        if pre_invoice.partner_id.id==part.id:
                            if pre_invoice.type =='out_invoice':
                                credit_amount +=pre_invoice.balance
                            else:
                                debit_amount +=pre_invoice.balance
                
                subtotal = price_subtotal+tax_amount
                sheet.write(rowx, 7, debit_amount, data_right)
                sheet.write(rowx, 8, credit_amount, data_right)
                sheet.write(rowx, 5, warm_day, data_right)
                sheet.write(rowx, 6, warmed_day, data_right)
                sheet.write(rowx, 9, price_subtotal, data_right)
                sheet.write(rowx, 10, tax_amount, data_right)
                sheet.write(rowx, 11, receipt_amount, data_right)
                sheet.write(rowx, 12, loss_amount, data_right)
                sheet.write(rowx, 13, subtotal, data_right)
                
                voucher_id = voucher_obj.search(cr, uid, [('partner_id','=',part.id),('period_id','=',period.id),('state','=','posted')])
                now_invoice_id = invoice_obj.search(cr, uid, [('partner_id','=',part.id),('type','=','in_invoice'),('period_id','=',period.id),('state','=','open')])
                if now_invoice_id:
                    for lis in invoice_obj.browse(cr, uid, now_invoice_id):
                        if lis.journal_id.code =='khan7':
                            khanbank97 +=lis.amount_total
                            
                        elif lis.journal_id.code == 'khas':
                            khasbank +=lis.amount_total
                            
                        elif lis.journal_id.code == 'torin':
                           toriinbank +=lis.amount_total 

                        elif lis.journal_id.code == 'golom':
                           golomtbank +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'capit':
                           capitronbank +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'catal':
                           capitalbank +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'khan0':
                           khanbank10 +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'hhban':
                           hhbank +=lis.amount_total 
                        
                        elif lis.journal_id.code == 'cash':
                            cash +=lis.amount_total
                    
                        elif lis.journal_id.code == 'dhb':
                            dhb +=lis.amount_total
                            
                        elif lis.journal_id.code == 'dh':
                            dh +=lis.amount_total
                
                if voucher_id:
                    for voucher in voucher_obj.browse(cr, uid,voucher_id):
                        
                        if voucher.journal_id.code =='khan7':
                            khanbank97 +=voucher.amount
                            
                        elif voucher.journal_id.code == 'khas':
                            khasbank +=voucher.amount
                            
                        elif voucher.journal_id.code == 'torin':
                           toriinbank +=voucher.amount 

                        elif voucher.journal_id.code == 'golom':
                           golomtbank +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'capit':
                           capitronbank +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'catal':
                           capitalbank +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'khan0':
                           khanbank10 +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'hhban':
                           hhbank +=voucher.amount 
                        
                        elif voucher.journal_id.code == 'cash':
                            cash +=voucher.amount
                            
                        elif voucher.journal_id.code == 'dhb':
                            dhb +=voucher.amount
                            
                        elif voucher.journal_id.code == 'dh':
                            dh +=voucher.amount
                        
                total_amount = khanbank97+khasbank+toriinbank+golomtbank+capitronbank+capitalbank+khanbank10+dhb+cash+hhbank+dh
                total = (debit_amount+subtotal)-(credit_amount+total_amount)
                if total >0.0:
                    total_debit = total
                else:
                    total_credit = total*(-1)
                sheet.write(rowx, 14, khanbank97,data_right)
                sheet.write(rowx, 15, khasbank,data_right)
                sheet.write(rowx, 16, toriinbank,data_right)
                sheet.write(rowx, 17, golomtbank,data_right)
                sheet.write(rowx, 18, capitronbank,data_right)
                sheet.write(rowx, 19, capitalbank,data_right)
                sheet.write(rowx, 20, khanbank10,data_right)
                sheet.write(rowx, 21, dhb,data_right)
                sheet.write(rowx, 22, cash,data_right)
                sheet.write(rowx, 23, hhbank,data_right)
                sheet.write(rowx, 24, dh,data_right)
                sheet.write(rowx, 25, total_amount,data_right)
                sheet.write(rowx, 26, total_debit,data_right)
                sheet.write(rowx, 27, total_credit,data_right)
                sheet.write(rowx, 28, u'%s'%phone,data_right)
                sheet.write(rowx, 29, u'%s'%mobile,data_right)
                rowx +=1
            
            

            
        else:
            type = u'Төсөвт Байгууллага'
            text_title = u'ТӨСӨВТ БАЙГУУЛЛАГЫН ДУЛААНЫ ТООЦООНЫ ДЭЛГЭРЭНГҮЙ БҮРТГЭЛ'
            rowx = 4
            sheet.write_merge(rowx, rowx+1, 0, 0, u'Д/д', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 1, 1, u'Байгууллага нэр', styles['heading_xf'])            
            sheet.write_merge(rowx, rowx+1, 2, 2, u'Талбай', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 3, 3, u'Баримтын №', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 4, 4, u'Байцаагч', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 5, 5, u'Халаах хоног /Эхний заалт', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 6, 6, u'Халаасан хоног /Эцсийн заалт', styles['heading_xf'])
            sheet.write_merge(rowx, rowx, 7, 8, u'Эхний үлдэгдэл', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 7, 7, u'Дт', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 8, 8, u'Кд', styles['heading_xf'])
            sheet.write_merge(rowx, rowx, 9, 11, u'Дт гүйлгээ', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 9, 9, u'Бичилт', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 10, 10, u'НӨАТ', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 11, 11, u'Маягт', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 12, 12, u'Алданги', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 13, 13, u'Дүн', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 14, 14, u'Хаан 5585039897', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 15, 15, u'Хас', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 16, 16, u'Төрийн', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 17, 17, u'Голомж', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 18, 18, u'Капитрон', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 19, 19, u'Капитал', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 20, 20, u'Хаан 5585384710', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 21, 21, u'ДХ хоорондын тооцоо', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 22, 22, u'Касс', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 23, 23, u'ХХБанк', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 24, 24, u'Хоорондын тооцоо', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 25, 25, u'Дүн', styles['heading_xf'])
            sheet.write_merge(rowx, rowx, 26, 27, u'Эцсийн үлдэгдэл', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 26, 26, u'Дт', styles['heading_xf'])
            sheet.write_merge(rowx+1, rowx+1, 27, 27, u'Кд', styles['heading_xf'])
            sheet.write_merge(rowx, rowx+1, 28, 28, u'Утасны дугаар', styles['heading_xf'])
            
            
            rowx = 6
            num = 0
            
            partner_id = partner_obj.search(cr, uid, [('type','=',data['type'])])
            
            for part in partner_obj.browse(cr, uid, partner_id):
                num +=1
                khanbank97 = 0.0
                khasbank = 0.0
                toriinbank = 0.0
                golomtbank = 0.0
                capitronbank = 0.0
                capitalbank = 0.0
                khanbank10 = 0.0
                hhbank = 0.0
                cash = 0.0
                salary = 0.0
                dhb = 0.0
                dh = 0.0
                total_amount = 0.0
                phone = ''
                inspector = u''
                total_credit = 0.0
                total_debit = 0.0
                debit_amount = 0.0
                credit_amount = 0.0
                warm_day = 0.0
                warmed_day = 0.0
                receipt_amount = 0.0
                price_subtotal = 0.0
                tax_amount = 0.0
                loss_amount = 0.0
                subtotal = 0.0
                sheet.write(rowx, 0, '%s'%num, data_right)
                sheet.write(rowx, 1, u'%s'%part.name, data_right)
                sheet.write(rowx, 2, u'%s'%part.square_amount, data_right)
                invoice_id = invoice_obj.search(cr, uid, [('partner_id','=',part.id),('type','=','out_invoice'),('period_id','=',period.id),('state','=','open')])
                if invoice_id:
                    for invoice in invoice_obj.browse(cr, uid,invoice_id):
                        sheet.write(rowx, 3, u'%s'%invoice.name, data_right)
                        if invoice.inspector_id.name: 
                            inspector = invoice.inspector_id.name
                        sheet.write(rowx, 4, u'%s'%inspector, data_right)
                        if invoice.partner_id.phone:
                            phone = invoice.partner_id.phone
                        
                        for inv_line in invoice.invoice_line:
                            warm_day = inv_line.warm_day
                            warmed_day =inv_line.warmed_day
                            price_subtotal = inv_line.price_subtotal-inv_line.doc_amount-inv_line.loss_amount
                            receipt_amount = inv_line.receipt_amount
                            loss_amount = inv_line.loss_amount
                        tax_amount = invoice.amount_tax
                pre_invoice_id = invoice_obj.search(cr, uid, [('partner_id','=',part.id),('type','in',('out_invoice','in_invoice')),('period_id','<',period.id),('state','in',('open','paid'))])
                
                if pre_invoice_id:
                    for pre_invoice in invoice_obj.browse(cr, uid, pre_invoice_id):
                        if pre_invoice.partner_id.id==part.id:
                            if part.id==11865:
                                print "logg",pre_invoice.balance
                            if pre_invoice.type =='out_invoice':
                                credit_amount +=pre_invoice.balance
                            else:
                                debit_amount +=pre_invoice.balance
                
                subtotal = price_subtotal+tax_amount
                sheet.write(rowx, 7, debit_amount, data_right)
                sheet.write(rowx, 8, credit_amount, data_right)
                sheet.write(rowx, 5, warm_day, data_right)
                sheet.write(rowx, 6, warmed_day, data_right)
                sheet.write(rowx, 9, price_subtotal, data_right)
                sheet.write(rowx, 10, tax_amount, data_right)
                sheet.write(rowx, 11, receipt_amount, data_right)
                sheet.write(rowx, 12, loss_amount, data_right)
                sheet.write(rowx, 13, subtotal, data_right)
                
                voucher_id = voucher_obj.search(cr, uid, [('partner_id','=',part.id),('period_id','=',period.id),('state','=','posted')])
                now_invoice_id = invoice_obj.search(cr, uid, [('partner_id','=',part.id),('type','=','in_invoice'),('period_id','=',period.id),('state','=','open')])
                if now_invoice_id:
                    for lis in invoice_obj.browse(cr, uid, now_invoice_id):
                        if lis.journal_id.code =='khan7':
                            khanbank97 +=lis.amount_total
                            
                        elif lis.journal_id.code == 'khas':
                            khasbank +=lis.amount_total
                            
                        elif lis.journal_id.code == 'torin':
                           toriinbank +=lis.amount_total 

                        elif lis.journal_id.code == 'golom':
                           golomtbank +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'capit':
                           capitronbank +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'catal':
                           capitalbank +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'khan0':
                           khanbank10 +=lis.amount_total 
                           
                        elif lis.journal_id.code == 'hhban':
                           hhbank +=lis.amount_total 
                        
                        elif lis.journal_id.code == 'cash':
                            cash +=lis.amount_total
                    
                        elif lis.journal_id.code == 'dhb':
                            dhb +=lis.amount_total
                            
                        elif lis.journal_id.code == 'dh':
                            dh +=lis.amount_total
                if voucher_id:
                    for voucher in voucher_obj.browse(cr, uid,voucher_id):
                        
                        if voucher.journal_id.code =='khan7':
                            khanbank97 +=voucher.amount
                            
                        elif voucher.journal_id.code == 'khas':
                            khasbank +=voucher.amount
                            
                        elif voucher.journal_id.code == 'torin':
                           toriinbank +=voucher.amount 

                        elif voucher.journal_id.code == 'golom':
                           golomtbank +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'capit':
                           capitronbank +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'catal':
                           capitalbank +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'khan0':
                           khanbank10 +=voucher.amount 
                           
                        elif voucher.journal_id.code == 'hhban':
                           hhbank +=voucher.amount 
                        
                        elif voucher.journal_id.code == 'cash':
                            cash +=voucher.amount
                            
                        elif voucher.journal_id.code == 'dhb':
                            dhb +=voucher.amount
                            
                        elif voucher.journal_id.code == 'dh':
                            dh +=voucher.amount
                        
                total_amount = khanbank97+khasbank+toriinbank+golomtbank+capitronbank+capitalbank+khanbank10+dhb+cash+hhbank+dh
                total = (debit_amount+subtotal)-(credit_amount+total_amount)
                if total >=0.0:
                    total_debit = total
                else:
                    total_credit = total*(-1)
                sheet.write(rowx, 14, khanbank97,data_right)
                sheet.write(rowx, 15, khasbank,data_right)
                sheet.write(rowx, 16, toriinbank,data_right)
                sheet.write(rowx, 17, golomtbank,data_right)
                sheet.write(rowx, 18, capitronbank,data_right)
                sheet.write(rowx, 19, capitalbank,data_right)
                sheet.write(rowx, 20, khanbank10,data_right)
                sheet.write(rowx, 21, dhb,data_right)
                sheet.write(rowx, 22, cash,data_right)
                sheet.write(rowx, 23, hhbank,data_right)
                sheet.write(rowx, 24, dh,data_right)
                sheet.write(rowx, 25, total_amount,data_right)
                sheet.write(rowx, 26, total_debit,data_right)
                sheet.write(rowx, 27, total_credit,data_right)
                sheet.write(rowx, 28, u'%s'%phone,data_right)
                rowx +=1            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
        sheet.write(1, 10, u'%s'%text_title, ezxf('font: bold on; align: wrap off, vert centre, horiz left;font: height 200'))

        
        
        
        
        
        
        inch = 1000
        sheet.col(0).width = 2 * inch
        sheet.col(1).width = 6 * inch
        sheet.col(2).width = 5 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 6 * inch
        sheet.col(5).width = 6 * inch
        sheet.col(6).width = 3 * inch
        sheet.col(7).width = 4 * inch
        sheet.col(8).width = 3 * inch
        sheet.col(9).width = 3 * inch
        sheet.col(10).width = 3 * inch
        sheet.col(11).width = 3 * inch
        sheet.col(12).width = 3 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.col(16).width = 3 * inch
        sheet.col(17).width = 3 * inch
        sheet.col(18).width = 3 * inch
        sheet.col(19).width = 3 * inch
        sheet.col(20).width = 3 * inch
        sheet.col(21).width = 3 * inch
        sheet.col(22).width = 3 * inch
        sheet.col(23).width = 3 * inch
        sheet.col(24).width = 3 * inch
        sheet.col(25).width = 3 * inch
        sheet.col(26).width = 3 * inch
        sheet.col(27).width = 3 * inch
        sheet.col(28).width = 3 * inch
        sheet.row(5).height = 900
        
        

        return {'data':book,}

warm_detail_compute_report()