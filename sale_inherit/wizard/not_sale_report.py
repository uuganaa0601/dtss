# -*- coding: utf-8 -*-
##############################################################################
#
#    Tengersoft, Enterprise Management Solution    
#    Copyright (C) 2016-2020 Tengersoft Co.,ltd (<http://www.tengersoft.mn>). All Rights Reserved
#
#    Address : Tengersoft LLC
#    Email : info@tengersoft.mn
#    Phone : 976 + 99345677
#
##############################################################################

from datetime import timedelta
from lxml import etree
from openerp.osv import fields, osv
from openerp.tools.translate import _
from operator import itemgetter
import xlrd, base64, time, datetime, xlwt, openerp.netsvc, datetime
from StringIO import StringIO
from openerp.addons.delivery.sale import sale_order

class not_sale_report(osv.osv_memory):
    """
        
    """
    _inherit = "abstract.report.excel"
    _name = "not.sale.report"
    _description = "Product Detail Register"
    
    _columns = {
        'company_id': fields.many2one('res.company', 'Company'),
        'period_id': fields.many2one("account.period",u'Мөчлөг', required=True),
        'inspector_id':fields.many2one('hr.employee',u'Байцаагч'),
        "type":fields.selection([('self',u'Айл өрх'),
                                 ('company',u'Байгууллага'),
                                 ('nat_company',u'Төсөвт байгууллага')],u'Төрөл',required=True),
    }
    
    _defaults = {
        'company_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),
    }
    
    
    def get_export_data(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        form = self.browse(cr, uid, ids[0], context=context)
        datas = {'ids': context.get('active_ids', [])}
        datas['form'] = self.read(cr, uid, ids)[0]
        data = datas['form']
        company = self.pool.get('res.company').browse(cr, uid, data['company_id'][0], context=context)   
        
        account_obj = self.pool.get('account.invoice')
        period_obj = self.pool.get('account.period')
        period = period_obj.browse(cr, uid, data['period_id'][0])
        if data['inspector_id']:
            account_id = account_obj.search(cr, uid, [('company_id','=',company.id),('state','not in',('cancel','draft','paid')),('inspector_id','=',data['inspector_id'][0]),('period_id','=',period.id),('type','=','out_invoice')])
        else:
            account_id = account_obj.search(cr, uid, [('company_id','=',company.id),('state','not in',('cancel','draft','paid')),('period_id','=',period.id),('type','=','out_invoice')])

            
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('Product Detailed Ledger')
        styles = self.get_easyxf_styles()
        ezxf = xlwt.easyxf
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')


        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name,), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 200'))
        
        sheet.write_merge(1, 1, 0, 9, u'Тухайн сарын төлбөрөө төлөөгүй харилцагчид', ezxf('font: bold on; align: wrap off, vert centre, horiz centre;font: height 200'))
        
        sheet.write_merge(2, 2, 5, 9, u'Тайлант хугацаа: %s'%period.date_start[:7] , ezxf('font:bold on;align:wrap off,vert centre,horiz right;font: height 200'))
        
        type = u''
        if data['type'] == 'self':
            type = u'Айл өрх'
        elif data['type'] =='company':
            type = u'Байгууллага'
        elif data['type'] == 'nat_company':
            type = u'Төсөвт байгууллага'
            
        sheet.write(3, 1, u'Харилцагчийн төрөл:  %s'%type, ezxf('font:bold on;align:wrap off,vert centre,horiz right;font: height 180'))
        
        
        rowx = 5
        sheet.write_merge(rowx, rowx+1, 0, 0, u'Д/д', styles['heading_xf'])
        sheet.write_merge(rowx, rowx+1, 1, 1, u'Харилцагчийн нэр', styles['heading_xf'])
        sheet.write_merge(rowx, rowx+1, 2, 2, u'Харилцагчийн код', styles['heading_xf'])
        sheet.write_merge(rowx, rowx+1, 3, 3, u'Тоот', styles['heading_xf'])
        sheet.write_merge(rowx, rowx+1, 4, 4, u'Байцаагч', styles['heading_xf'])
        sheet.write_merge(rowx, rowx+1, 5, 5, u'Гэрээний дугаар', styles['heading_xf'])
        sheet.write_merge(rowx, rowx+1, 6, 6, u'Тоолууртай эсэх', styles['heading_xf'])
        sheet.write_merge(rowx, rowx+1, 7, 7, u'Утас', styles['heading_xf'])
        sheet.write_merge(rowx, rowx+1, 8, 8, u'Өмнөх үлдэгдэл', styles['heading_xf'])
        sheet.write_merge(rowx, rowx+1, 9, 9, u'Гарсан төлбөр', styles['heading_xf'])
        sheet.write_merge(rowx, rowx+1, 10, 10, u'Төлөх төлбөр', styles['heading_xf'])
        
        rowx = 7
        num = 0
        for line in account_obj.browse(cr, uid, account_id):
            pre_amount = 0.0
            pre_invoice_id = account_obj.search(cr, uid, [('period_id','<',period.id),('type','=','out_invoice'),('partner_id','=',line.partner_id.id)])
            if pre_invoice_id:
                for pre_line in account_obj.browse(cr, uid,pre_invoice_id):
                    pre_amount +=pre_line.amount_total
            pay_invoice_id  = account_obj.search(cr, uid, [('period_id','<',period.id),('type','=','in_invoice'),('partner_id','=',line.partner_id.id)])
            if pay_invoice_id:
                for pay_line in account_obj.browse(cr, uid,pay_invoice_id):
                    pre_amount -=pay_line.amount_total
                    
            if line.partner_id.type == data['type']:
                num +=1
                phone = ''
                counter = u''
                sheet.write(rowx, 0, u'%s'%num, data_right)
                sheet.write(rowx, 1, u'%s'%line.partner_id.name, data_left)
                sheet.write(rowx, 2, u'%s'%line.partner_id.code, data_center)
                sheet.write(rowx, 3, u'%s'%line.partner_id.street, data_center)
                sheet.write(rowx, 4, u'%s'%line.inspector_id.name, data_right)
                sheet.write(rowx, 5, u'%s'%line.partner_id.contract_number, data_center)
                if line.partner_id.is_counter== True:
                    counter = u'Тоолууртай'
                else:
                    counter = u'Тоолуургүй'
                sheet.write(rowx, 6, u'%s'%counter, data_center)
                if line.partner_id.phone:
                    phone = line.partner_id.phone
                else:
                    phone = ''
                sheet.write(rowx, 7, u'%s'%phone, data_center)
                sheet.write(rowx, 8, u'%s'%pre_amount, data_right)
                sheet.write(rowx, 9, u'%s'%line.balance, data_right)
                sheet.write(rowx, 10, u'%s'%(line.balance+pre_amount), data_right)
                rowx +=1
            
        
        
        
        inch = 1000
        sheet.col(0).width = 2 * inch
        sheet.col(1).width = 5 * inch
        sheet.col(2).width = 3 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 6 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 3 * inch
        sheet.col(7).width = 4 * inch
        sheet.col(8).width = 3 * inch
        sheet.col(9).width = 3 * inch

        

        return {'data':book, 'directory_name':u'Төлбөр төлөөгүй харилцагчид', 
                }

not_sale_report()