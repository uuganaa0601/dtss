# -*- coding: utf-8 -*-
##############################################################################
#
#    
#
#    
#
##############################################################################

{
    "name": "Sale inherit module",
    "version": "1.0",
    "author": "Erdenebold",
    "category": "Hr",
    "description": """This module sale inherit.""",
    "depends" : [
                 "base",
                 "account",
                 "product",
                 "account_invoice_inherit",
                 ],
    'update_xml': [
                    'data/product_category_data.xml',
                   # 'sale_inherit_view.xml',
                    "auto_sale_order_view.xml",
                    "product_inherit_view.xml",
                    "absract_report_view.xml",
                    "product_category_view.xml",
                    "wizard/not_sale_report_view.xml",
                    "wizard/warm_detail_compute_view_report.xml",
                   # "report/sale_new_report_view.xml",
                    "sequence_view.xml",
                    "menu_view.xml",
                   ],

    'installable': True,
    'auto_install': False, 
    "icon": '/sale_inherit/static/src/img/mn_icon.png',
}