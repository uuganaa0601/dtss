# -*- coding: utf-8 -*-
##############################################################################
#  
##############################################################################

from openerp import tools
from openerp.osv import fields, osv

from openerp.tools.sql import drop_view_if_exists

class sale_new_report(osv.osv):
    _name = "sale.new.report"
    _auto = False
    
    _columns = {
                'partner_id':fields.many2one('res.partner',u'Харилцагч'),
                'ztb_id':fields.many2one('product.product',u'ЦТП'),
                'date':fields.char('Өдөр',readonly=True),
                'month':fields.char('Сар',readonly=True),
                'year':fields.char('Жил',readonly=True),
                'net_amount':fields.float(u'Цэвэр борлуулалт'),
                'total_amount':fields.float(u'Борлуулалтын дүн'),
                'untax_amount':fields.float(u'Татваргүй дүн'),
                'tax_amount':fields.float(u'Татварын дүн'),
                'type':fields.selection([('self',u'Айл өрх'),
                                         ('work',u'Айл өрх'),
                                             ('nat_company',u'Төсөвт Байгууллага'),
                                             ('company',u'ААН Байгууллага')],u'Харилцагчийн төрөл'),
                'local_type':fields.selection([('counter',u'Тоолууртай эсэх'),
                                               ('day',u'Хоногоор эсэх /Айл өрх/'),
                                               ('com_day',u'Хоногоор эсэх /ААН/'),
                                               ('elect',u'Цахилгаан'),
                                               ],u'Төрөл'),
                'state':fields.selection([('draft',u'Ноорог'),
                                          ('manual',u'Нэхэмжлэл ба борлуулалт'),
                                          ('progress',u'Нэхэмжлэл ба борлуулалт'),
                                          ('done',u'Дууссан')],u'Төлөв'),
                'inspector_id':fields.many2one('hr.employee',u'Байцаагч'),
        }
    
    def init(self, cr):
        drop_view_if_exists(cr, 'sale_new_report')
        cr.execute("""
            create or replace view sale_new_report as (
               SELECT n.id, n.inspector_id, n.state, n.partner_id, pt.local_type, n.ztb_id as ztb_id, rp.type, to_char(n.date_order, 'YYYY-MM-DD') as date, to_char(n.date_order, 'YYYY-MM') as month, 
                   to_char(n.date_order, 'YYYY') as year, sum(n.amount_total) as total_amount, sum(n.amount_untaxed) as untax_amount,sum(n.amount_tax) as tax_amount,
                   case when rp.type='company' then sum(n.amount_untaxed-sl.doc_amount)
                   when rp.type='nat_company' then sum(n.amount_untaxed-sl.doc_amount) 
                   else sum(n.amount_untaxed) 
                   end as net_amount
                   FROM
                  sale_order n
                 left join sale_order_line sl on (sl.order_id=n.id)
                 Left join product_product p on (p.id = sl.product_id)
                 left join product_template pt on (pt.id=p.product_tmpl_id)
                 left join res_partner rp on (rp.id=n.partner_id)
                 group by n.inspector_id, n.state, n.partner_id,n.date_order, pt.local_type, rp.type,n.ztb_id, n.id)""")
        
        
        