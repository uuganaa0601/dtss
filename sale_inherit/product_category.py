# -*- coding: utf-8 -*-
##############################################################################
#
#    Ten, Enterprise Management Solution    
#    Copyright (C) 2007-2014 ShineERP Co.,ltd (<http://www.serp.mn>). All Rights Reserved
#
#    Address : Suhkbaatar District, National ITPark, ShineERP LLC
#    Email : info@serp.mn
#    Phone : 976 + 88001963
#
##############################################################################

import pytz
from openerp import SUPERUSER_ID, workflow
from datetime import datetime
import time
from dateutil.relativedelta import relativedelta
from operator import attrgetter
from openerp.tools.safe_eval import safe_eval as eval
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.osv.orm import browse_record_list, browse_record, browse_null
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
import time

class product_category(osv.osv):
    _inherit = 'product.category'
    
    _columns = {
                'code':fields.char('Category code',size=7,required=True),
    }
    
    