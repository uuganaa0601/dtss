# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import base64
import os
import math
from openerp import tools
from openerp.osv import fields,osv
from openerp.tools.translate import _
from openerp.tools.misc import get_iso_codes
import calendar

from tempfile import NamedTemporaryFile
import time, datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
import xlwt, xlrd, time

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class auto_sale_order(osv.osv):
    _name = 'auto.sale.order'
    
    _columns = {
                'company_id':fields.many2one('res.company',u'Компани',required=True),
                'name':fields.char(u'Дугаар',readonly=True),
                'sale_user_id':fields.many2one('res.users',u'Борлуулалтын ажилтан'),
                'period_id':fields.many2one('account.period',u'Мөчлөг',required=True),
                'auto_date':fields.date(u'Огноо',required=True),
                'tstp_id':fields.many2one('product.product',u'ЦТП',required=True,domain=[('is_ztb', '=', True)]),
                'warm_day':fields.float(u'Халаасан хоног',required=True),
                'state':fields.selection([('draft',u'Ноорог'),
                                          ('confirm',u'Батлагдсан'),],u'Төлөв'),
                'line_id':fields.one2many('auto.sale.order.line','auto_order_id',u'Мөрүүд'),
                
        }
    
    _defaults = {
                'state':'draft',
                'sale_user_id':lambda self, cr, uid, c: uid,
                'company_id':lambda self, cr, uid, c: self.pool.get('res.company')._company_default_get(cr, uid, 'auto.sale.order', context=c),
                'auto_date':lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
        }
 
 
    def action_import(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids)
        partner_obj = self.pool.get('res.partner')
        line_obj = self.pool.get('auto.sale.order.line')
        partner_id = partner_obj.search(cr, uid, [('tstp_id','=',obj.tstp_id.id)])
        invoice_obj = self.pool.get('account.invoice')
        #-------------------------- invoice_id = invoice_obj.search(cr, uid, [])
        #----------------- for lines in invoice_obj.browse(cr, uid, invoice_id):
            #---------------------- invoice_obj.button_compute(cr, uid,lines.id)
            
        if partner_id !=[]:
            day = 0.0
            loss_day = 0.0
            for li in partner_obj.browse(cr, uid, partner_id):
                line = line_obj.search(cr, uid, [('partner_id','=',li.id),('auto_order_id','=',obj.id)])
                invoice_id = invoice_obj.search(cr, uid, [('partner_id','=',li.id),('date_invoice','<',obj.auto_date),('state','not in',('draft','done','cancel'))],order='date_invoice asc',limit=1)
                if line ==[]:
                    if invoice_id !=[]:
                        new_day = 0.0
                        invoice = invoice_obj.browse(cr, uid, invoice_id)
                        before = datetime.strptime(invoice.date_invoice,'%Y-%m-%d')
                        now = datetime.strptime(obj.auto_date, '%Y-%m-%d')
                        day = before - now
                        if float(day.days)< 0.0:
                            new_day = float(day.days)*(-1)
                        if new_day >= 40.0:
                            loss_day = new_day-40.0
                    else:
                         loss_day = 0.0
                    if li.is_day == True or li.is_size==True:
                        day = obj.warm_day
                    else:
                        day = 0.0
                    line_obj.create(cr, uid, {
                                                'auto_order_id':obj.id,
                                                'partner_id':li.id,
                                                'loss_day':loss_day,
                                                'warm_day':day,
                                                'company_id':obj.company_id.id,
                                            })
                    
        return True
    
    
    

    def action_confirm(self, cr, uid, ids, context=None):
        partner_obj = self.pool.get('res.partner')
        invoice_obj = self.pool.get('account.invoice')
        journal_obj = self.pool.get('account.journal')
        invoice_line_obj = self.pool.get('account.invoice.line')
        product_obj = self.pool.get('product.product')
        obj = self.browse(cr, uid, ids)
        for line in obj.line_id:
            if line.warm_day == 0.0 and line.f_point == 0.0 and line.l_point==0.0 and line.warm_day == 0.0:
                raise osv.except_osv(_(u'Анхааруулга'), _(u'%s харилцагчийн хоног хоосон байгаа тул харилцагчийн төрлийг шалгана уу?'%line.partner_id.name))
            sale_id = invoice_obj.search(cr, uid,[('partner_id','=',line.partner_id.id)])
            sale = invoice_obj.search(cr, uid, [('date_invoice','=',obj.auto_date),('partner_id','=',line.partner_id.id),('tstp_id','=',obj.tstp_id.id)])
            if sale != [] and sale !=False:
                raise osv.except_osv(_(u'Анхааруулга'), _(u'%s харилцагч дээр энэ хугацаанд борлуулалтын захиалга үүссэн байгаа тул шалгана уу?'%line.partner_id.name))
            if line.partner_id.is_day == False and line.partner_id.is_size == False and line.partner_id.is_counter==False and line.partner_id.is_dhg==False:
                raise osv.except_osv(_(u'Анхааруулга'), _(u'%s харилцагч дээр дулааны хэмжих нэгжийг сонгоогүй байгаа тул шалгана уу?'%line.partner_id.name))
            pricelist = line.partner_id.property_product_pricelist and line.partner_id.property_product_pricelist.id or False
            doc_amount = 0.0
            receipt_amount = 0.0
            pre_amount = 0.0
            
            pre_id = self.pool.get('account.invoice').search(cr, uid, [('state','not in', ('draft','cancel','sent','done')),('date_invoice','<',obj.auto_date),('partner_id','=',line.partner_id.id)])
            if pre_id !=[] and pre_id !=False:
                pre_amount = line.partner_id.credit-line.partner_id.debit

            journal_id = journal_obj.search(cr, uid,[('type','=','sale')])
            inv_id = invoice_obj.create(cr, uid, {
                                            'inspector_id':line.partner_id.inspector_id.id or False,
                                            'name':line.partner_id.code,
                                            'date_invoice':obj.auto_date,
                                            'type': 'out_invoice',
                                            'more_and_less_amount':pre_amount,
                                            'account_id': line.partner_id.property_account_receivable.id,
                                            'partner_id': line.partner_id.id,
                                            'journal_id': journal_id[0],
                                            'period_id':obj.period_id.id,
                                            'tstp_id':obj.tstp_id.id,
                                            'payment_term': False,
                                            'company_id': obj.company_id.id,
                                            'user_id': obj.sale_user_id and obj.sale_user_id.id or False
                                            })
            if line.partner_id.is_day == True and line.partner_id.is_counter == True:
                raise osv.except_osv(_(u'Анхааруулга'), _(u'%s харилцагч дээр талбай эсэх /м2/ болон Тоолууртай эсэх /Гкал/ гэж 2 тооцоолох арга сонгосон байгаа тул шалгана уу?'%line.partner_id.name))
                fpos = sline.order_id.fiscal_position or False
                account_id = line.product_id.property_account_income.id
                if not account_id:
                    account_id = line.product_id.categ_id.property_account_income_categ.id
                account_id = self.pool.get('account.fiscal.position').map_account(cr, uid, fpos, account_id)
            invoice_id = invoice_obj.browse(cr, uid, inv_id)
            if line.partner_id.is_day == True:
                now = obj.auto_date
                days = calendar.monthrange(int(now[:4]), int(now[5:7]))[1]
                
                product_id = product_obj.search(cr, uid,[('local_type','=','day')])
                product = product_obj.browse(cr, uid, product_id)
                if line.partner_id.type == 'nat_company' or line.partner_id.type =='company':
                    if line.partner_id.is_ail == True:
                         if product.local_type == 'day':
                            if 0 < line.partner_id.square_amount <= 40.0:
                                doc_amount = 3000.0
                            elif 40.0 < line.partner_id.square_amount < 80.0:
                                doc_amount = 5000.0
                            else:
                                doc_amount = 10000.0
                    if product.local_type == 'day' or product.local_type == 'counter' or product.local_type == 'com_day':
                        receipt_amount = 90.91
                    else:
                        receipt_amount = 0.0
                else:
                    if line.partner_id.is_day == True or line.partner_id.is_size == True:
                        if product.local_type == 'day':
                            if 0 < line.partner_id.square_amount <= 40.0:
                                doc_amount = 3000
                            elif 40.0 < line.partner_id.square_amount < 80.0:
                                doc_amount = 5000
                            else:
                                doc_amount = 10000
                                
                if line.partner_id.square_amount >0.0:
                    invoice_line_obj.create(cr, uid,{
                                                     'name':product.name,
                                                    'invoice_id':inv_id,
                                                    'product_id':product_id[0],
                                                    'warm_day':days,
                                                    'warmed_day':line.warm_day,
                                                    'price_unit':product.list_price,
                                                    'more_and_less_amount':pre_amount,
                                                    'company_id':obj.company_id.id,
                                                    'invoice_line_tax_id': [(6, 0, [x.id for x in product.taxes_id])],
                                                    'loss_day':line.loss_day,
                                                     'uos_id':product.uom_id.id,
                                                     'receipt_amount':receipt_amount,
                                                     'doc_amount':doc_amount,
                                                    'quantity':line.partner_id.square_amount,
                                                    })
                else:
                    raise osv.except_osv(_(u'Анхааруулга'), _(u'%s харилцагчийн талбайг оруулаагүй байгаа тул оруулна уу?'%line.partner_id.name))
            
            
            if line.partner_id.is_size == True:
                now = obj.auto_date
                days = calendar.monthrange(int(now[:4]), int(now[5:7]))[1]
                product_id = product_obj.search(cr, uid,[('local_type','=','com_day')])
                product = product_obj.browse(cr, uid, product_id)
                if line.partner_id.type == 'nat_company' or line.partner_id.type =='company':
                    if line.partner_id.is_ail == True:
                         if product.local_type == 'com_day':
                            if 0 < line.partner_id.square_amount <= 40.0:
                                doc_amount = 3000.0
                            elif 40.0 < line.partner_id.square_amount < 80.0:
                                doc_amount = 5000.0
                            else:
                                doc_amount = 10000.0
                    if product.local_type == 'day' or product.local_type == 'counter' or product.local_type == 'com_day':
                        receipt_amount = 90.91
                    else:
                        receipt_amount = 0.0
                else:
                    if line.partner_id.is_day == True or line.partner_id.is_size == True:
                        if product.local_type == 'day':
                            if 0 < line.partner_id.square_amount <= 40.0:
                                doc_amount = 3000
                            elif 40.0 < line.partner_id.square_amount < 80.0:
                                doc_amount = 5000
                            else:
                                doc_amount = 10000
                if line.partner_id.square_amount >0.0:
                    invoice_line_obj.create(cr, uid,{
                                                     'name':product.name,
                                                    'invoice_id':inv_id,
                                                    'product_id':product_id[0],
                                                    'warm_day':days,
                                                    'warmed_day':line.warm_day,
                                                    'more_and_less_amount':pre_amount,
                                                    'company_id':obj.company_id.id,
                                                    'price_unit':product.list_price,
                                                    'invoice_line_tax_id': [(6, 0, [x.id for x in product.taxes_id])],
                                                    'doc_amount':doc_amount,
                                                    'receipt_amount':receipt_amount,
                                                    'uos_id':product.uom_id.id,
                                                    'loss_day':line.loss_day,
                                                    'quantity':line.partner_id.square_amount,
                                                    })
                else:
                    raise osv.except_osv(_(u'Анхааруулга'), _(u'%s харилцагчийн талбайг оруулаагүй байгаа тул оруулна уу?'%line.partner_id.name))
            if line.partner_id.is_counter == True or line.partner_id.is_dhg == True:
                product_id = product_obj.search(cr, uid,[('local_type','=','counter')])
                product = product_obj.browse(cr, uid, product_id)
                qty = (line.l_point-line.f_point)*product.per
                line_loss_amount = (qty*product.list_price)/100.0*product.warm_loss_per
                if product.local_type == 'day' or product.local_type == 'counter' or product.local_type == 'com_day':
                    receipt_amount = 90.91
                else:
                    receipt_amount = 0.0
                invoice_line_obj.create(cr, uid,{
                                                'invoice_id':inv_id,
                                                'name':product.name,
                                                'product_id':product_id[0],
                                                'warm_day':line.f_point,
                                                'company_id':obj.company_id.id,
                                                'price_unit':product.list_price,
                                                'warmed_day':line.l_point,
                                                'loss_day':line.loss_day,
                                                'more_and_less_amount':pre_amount,
                                                'receipt_amount':receipt_amount,
                                                'uos_id':product.uom_id.id,
                                                'doc_amount':doc_amount,
                                                'invoice_line_tax_id': [(6, 0, [x.id for x in product.taxes_id])],
                                                'quantity':qty,
                                                })
  
            if line.partner_id.type == 'self' or line.partner_id.type == 'work':
               invoice = invoice_obj.browse(cr, uid, inv_id, context=context)
               invoice.signal_workflow('invoice_open')
            self.write(cr, uid, ids,{ 'name':self.pool['ir.sequence'].get(cr, uid, 'auto.sale.order', context=context),
                                    'state':'confirm'})
        return True
        
class auto_sale_order_line(osv.osv):
    _name = 'auto.sale.order.line'
    
    _columns = {
                'company_id':fields.many2one('res.company','Company'),
                'auto_order_id':fields.many2one('auto.sale.order',u'Борлуулалт'),
                'partner_id':fields.many2one('res.partner',u'Харилцагч'),
                'warm_day':fields.float(u'Халаасан хоног'),
                'loss_day':fields.integer(u'Алдангийн хоног'),
                'f_point':fields.float(u'Эхний заалт'),
                'l_point':fields.float(u'Эцсийн заалт'),
                        
        }
    
    def partner_onchange(self, cr, uid, ids,partner_id):
        obj = self.browse(cr, uid,ids)
        account_obj = self.pool.get('account.invoice')
        
        loss_day = 0.0
        day = obj.auto_order_id.warm_day
        return { 'value':{
                            'auto_order_id':obj.auto_order_id.id,
                            'partner_id':partner_id,
                            'loss_day':loss_day,
                            'warm_day':day,
                            'company_id':obj.company_id.id,
                            }
            }
    