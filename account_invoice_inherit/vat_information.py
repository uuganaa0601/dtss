# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal
import time
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import json
#import vatbridge

class vat_information(osv.osv):
    _name = "vat.information"
    _description = "vat data send"
    
    _columns = {
        'company_id':fields.many2one('res.company','Company'),
        'state':fields.selection([('draft','Draft'),
                                  ('send','Send data')],'State'),
        'date': fields.datetime('Date', select=True, required=True),
        'text':fields.text('Info'),
        'vat_data_id':fields.one2many('vat.data','vat_info_id','Vat data',domain=[('is_active','=',False)] ),
                }
    
    _defaults = {
        'date': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
        'company_id': lambda self, cr, uid, c: self.pool.get('res.company')._company_default_get(cr, uid, 'vat.information', context=c),

    }
    
    def action_getInformation(self, cr, uid, ids, context=None):
#------------------------------------------------------------------------------ 
        #--------------------------------- datainfo = vatbridge.getInformation()
        #---------------------------------- new_data_info = json.loads(datainfo)
        #---------------------------------------- if new_data_info['extraInfo']:
            #--------- if int(new_data_info['extraInfo']['countBill']) >= 19990:
                # raise Warning(_(u'Баримтын дугаар дуусч байгаа тул илгээх товч дээр дарна уу'))
            #------ elif int(new_data_info['extraInfo']['countLottery']) <= 100:
                # raise Warning(_(u'Баримтын дугаар дуусч байгаа тул илгээх товч дээр дарна уу'))
        datainfo = ''
        return self.write(cr, uid, ids, {'text':datainfo})    
    
    
    def action_checkAPI(self, cr, uid, ids, context=None):

        #data = vatbridge.checkApi()
        data = ''
        return self.write(cr, uid, ids, {'text':data})    

    def action_callFunction(self, cr, uid, ids, context=None):

        #data = vatbridge.callFunction()
        data = ''
        return self.write(cr, uid, ids, {'text':data})    

    def action_returnBill(self, cr, uid, ids, context=None):

        #data = vatbridge.returnBill()
        data = ''
        return self.write(cr, uid, ids, {'text':data})    
                    
    def action_sendData(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids)
        vat_data_obj = self.pool.get('vat.data')
        #data = vatbridge.sendData()
        #------------------------------------------- new_data = json.loads(data)
        #--------------------------------------- if new_data['success'] == True:
            #--------------------------------------- if obj.vat_data_id !=False:
                #---------------------------------- for line in obj.vat_data_id:
                    #--------------------------------- if line.state == 'draft':
                        #----------------- vat_data_obj.write(cr, uid, line.id,{
                                                            # 'state':'send_data',
                                                            #- 'is_active':True,
                                                            #---------------- })
        #else:
         #   raise Warning(_(u'%s'%data))
        data = ''       
        return self.write(cr, uid, ids, {'text':data})    
    
vat_information()

class vat_data(osv.osv):
    _name = 'vat.data'
    
    _columns = {
        'order_name':fields.char('Pos order name'),
        'invoice_id':fields.many2one('account.invoice','Invoice'),
        'is_active':fields.boolean('Acitve'),
        'vat_info_id':fields.many2one('vat.information','Vat info'),
        'state':fields.selection([('draft','Draft'),
                                  ('send_data','Send data'),
                                  ('refund','Refund')],'State'),
        'success':fields.boolean('Success'),
        'registerNo':fields.char('Register no'),
        'billId':fields.char('Bill Id'),
        'date':fields.datetime('Date'),
        'internalCode':fields.char('Internal code'),
        'lotteryWarningMsg':fields.char('lotteryWarningMsg'),
        'errorCode':fields.char('Адааны мэдээлэл'),
        'message':fields.char('message'),
        'amount':fields.float('Amount'),
        'vat':fields.float('Vat'),
        'cashAmount':fields.float('Cash amount'),
        'nonCashAmount':fields.float('None cash amount'),
        'cityTax':fields.float('City tax'),
        'districtCode':fields.float('District code'),
        'posNo':fields.char('Pos number'),
        'billIdSuffix':fields.integer('Bill id suffix'),
        'returnBillId':fields.integer('Return Bill Id'),
        'taxType':fields.integer('Tax Type'),
        'invoiceId':fields.integer('Invoice Id'),
        'reportMonth':fields.char('Өмнөх сар'),
        'branchNo':fields.integer('Branch number'),
        'billType':fields.char('Bill type'),
        'customerNo':fields.char('Customer number'),
        'line_id':fields.one2many('vat.data.line','vat_id','Vat data line'),
        'no_lottery_note':fields.text('Сугалааны дугаар хэвлээгүй тэмдэглэл'),
        'no_lottery':fields.boolean('Сугалааны дугаар хэвлээгүй эсэх'),
        'info':fields.text('Send info'),
        }
    
    
    _defaults = {
                'state':'draft',
        }
    
    def action_send(self, cr, uid, ids, context=None):
        data = vatbridge.sendData()
        return self.write(cr, uid, ids, {
            'info':data,
            'state':'send_data',
            }) 
    
    def action_refund(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids)
        bill_id = '%s'%obj.billId
        bill_date = '%s'%obj.date
        return_data = {
                        "returnBillId":bill_id,
                        "date":bill_date,
                        }
        jsondata= json.dumps(return_data, ensure_ascii=False).encode('utf8')
        data = vatbridge.returnBill(jsondata)
        rec_data = json.loads(data)
        if rec_data != None:
            if rec_data['success'] == True:
                self.write(cr, uid, ids, {
                                    'info':rec_data,
                                    'state':'refund',
                                    })
            else:
                raise Warning(_(u'%s'%rec_data['message']))
        else:
            raise Warning(_(u'Та татвар луу мэдээ илгээгүй байгаа тул буцаах боломжгүй'))
        
        return True
class vat_data_line(osv.osv):
        _name = 'vat.data.line'
        
        _columns = {
                    'vat_id':fields.many2one('vat.data','Vat'),
                    'code':fields.char('Product code'),
                    'name':fields.char('Product name'),
                    'measureUnit':fields.char('Measure unit'),
                    'qty':fields.float('Quantity'),
                    'unitPrice':fields.float('Unit price'),
                    'totalAmount':fields.float('Total amount'),
                    'cityTax':fields.float('City tax'),
                    'vat':fields.float('Vat'),
                    'barCode':fields.char('Barcode'),
                    }
        
class get_log_info(osv.osv):
    _name = 'get.log.info'
    
    _columns = {
                'name':fields.char('Get log info'),
                'company_id':fields.many2one('res.company','Company'),
                'info':fields.text('Info'),
                'date':fields.datetime('Log date'),
                'product_ids':fields.many2many('product.product','vat_zero_number_rel','product_id','order_id','Product ids'),
                }
    
class vat_free_register(osv.osv):
    _name = 'vat.free.register'

    _columns = {
                'company_id':fields.many2one('res.company','Компани'),
        }
    

    