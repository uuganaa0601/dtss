# -*- coding: utf-8 -*-
##############################################################################
#  
##############################################################################

from openerp import tools
from openerp.osv import fields, osv

from openerp.tools.sql import drop_view_if_exists

class invoice_new_payable_report(osv.osv):
    _name = "invoice.new.payable.report"
    _auto = False
    
    _columns = {
                'partner_id':fields.many2one('res.partner',u'Харилцагч'),
                'date':fields.char('Өдөр',readonly=True),
                'month':fields.char('Сар',readonly=True),
                'year':fields.char('Жил',readonly=True),
                'tstp_id':fields.many2one('product.product',u'ЦТП'),
                'paid_amount':fields.float(u'Төлсөн дүн'),
                'net_amount':fields.float(u'Илүү төлөлт'),
                'total_amount':fields.float(u'Өглөгийн дүн'),
                'untax_amount':fields.float(u'Татваргүй дүн'),
                'balance_amount':fields.float(u'Үлдэгдэл'),
                'tax_amount':fields.float(u'Татварын дүн'),
                'type':fields.selection([('self',u'Айл өрх'),
                                         ('work',u'Ажилтан'),
                                         ('nat_company',u'Төсөвт Байгууллага'),
                                         ('company',u'ААН Байгууллага')],u'Харилцагчийн төрөл'),
                'local_type':fields.selection([('counter',u'Тоолууртай эсэх'),
                                               ('day',u'Хоногоор эсэх /Айл өрх/'),
                                               ('com_day',u'Хоногоор эсэх /ААН/'),
                                               ('elect',u'Цахилгаан'),
                                               ],u'Төрөл'),
                'state':fields.selection([('draft',u'Ноорог'),
                                          ('open',u'Нээлттэй'),
                                          ('paid',u'Төлсөн')],u'Төлөв'),
                'inspector_id':fields.many2one('hr.employee',u'Байцаагч'),
        }
    
    def init(self, cr):
        drop_view_if_exists(cr, 'invoice_new_payable_report')
        cr.execute("""
           create or replace view invoice_new_payable_report as (
               SELECT n.id, n.state, n.partner_id, pt.local_type, rp.type, n.inspector_id,n.tstp_id, to_char(n.date_invoice, 'YYYY-MM-DD') as date, to_char(n.date_invoice, 'YYYY-MM') as month, 
                   to_char(n.date_invoice, 'YYYY') as year, n.amount_total as total_amount, n.amount_untaxed as untax_amount,n.amount_tax as tax_amount,
           case when rp.type='company' then round(n.amount_untaxed-sl.doc_amount::numeric,2)
                   when rp.type='nat_company' then round(n.amount_untaxed-sl.doc_amount::numeric,2)
                   else round(n.amount_untaxed::numeric,2)
                   end as net_amount,
                   n.paid_amount as paid_amount,
                   round(n.amount_total - n.paid_amount::numeric,2) as balance_amount
                   FROM
                  account_invoice n
                 left join account_invoice_line sl on (sl.invoice_id=n.id)
                 Left join product_product p on (p.id = sl.product_id)
                 left join product_template pt on (pt.id=p.product_tmpl_id)
                 left join res_partner rp on (rp.id=n.partner_id)
                 left join account_move am on (am.id=n.move_id)
                 left join account_move_line aml on (aml.move_id=am.id)
                 where n.state in ('open','paid') and rp.supplier = True and  n.type='in_invoice'
                 group by n.state, n.partner_id,n.date_invoice, pt.local_type, rp.type,n.inspector_id, n.tstp_id, n.id,sl.doc_amount)""")