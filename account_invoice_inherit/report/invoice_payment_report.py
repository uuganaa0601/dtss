# -*- coding: utf-8 -*-
##############################################################################
#  
##############################################################################

from openerp import tools
from openerp.osv import fields, osv

from openerp.tools.sql import drop_view_if_exists

class invoice_payment_report(osv.osv):
    _name = "invoice.payment.report"
    _auto = False
    
    _columns = {
                'partner_id':fields.many2one('res.partner',u'Харилцагч'),
                'date':fields.char('Өдөр',readonly=True),
                'ognoo':fields.date('Огноо',readonly=True),
                'month':fields.char('Сар',readonly=True),
                'year':fields.char('Жил',readonly=True),
                'tstp_id':fields.many2one('product.product',u'ЦТП'),
                'doc_amount':fields.float(u'Суурь хураамж'),
                'receipt_amount':fields.float(u'Баримт'),
                'paid_amount':fields.float(u'Төлсөн дүн'),
                'net_amount':fields.float(u'Цэвэр борлуулалт'),
                'total_amount':fields.float(u'Авлагын дүн'),
                'journal_id':fields.many2one('account.journal',u'Төлбөрийн хэлбэр'), 
                'untax_amount':fields.float(u'Татваргүй дүн'),
                'more_amount':fields.float(u'Илүү төлөлт'),
                'total_paid':fields.float(u'Нийт төлбөр'),
                'balance_amount':fields.float(u'Үлдэгдэл'),
                'tax_amount':fields.float(u'Татварын дүн'),
                'type':fields.selection([('self',u'Айл өрх'),
                                         ('work',u'Ажилтан'),
                                         ('nat_company',u'Төсөвт Байгууллага'),
                                         ('company',u'ААН Байгууллага')],u'Харилцагчийн төрөл'),
                'local_type':fields.selection([('counter',u'Тоолууртай эсэх'),
                                               ('day',u'Хоногоор эсэх /Айл өрх/'),
                                               ('com_day',u'Хоногоор эсэх /ААН/'),
                                               ('elect',u'Цахилгаан'),
                                               ],u'Төрөл'),
                'state':fields.selection([('draft',u'Ноорог'),
                                          ('open',u'Нээлттэй'),
                                          ('paid',u'Төлсөн')],u'Төлөв'),
                'inspector_id':fields.many2one('hr.employee',u'Байцаагч'),
        }
    
    def init(self, cr):
        drop_view_if_exists(cr, 'invoice_payment_report')
        cr.execute("""
                     create or replace view invoice_payment_report as (
               SELECT n.id, n.state, n.partner_id, pt.local_type,  rp.type, n.inspector_id,n.tstp_id, n.date_invoice as ognoo, to_char(n.date_invoice, 'YYYY-MM-DD') as date, to_char(n.date_invoice, 'YYYY-MM') as month, 
                   to_char(n.date_invoice, 'YYYY') as year, n.amount_total as total_amount, n.amount_untaxed as untax_amount,n.amount_tax as tax_amount,
                   sl.receipt_amount as receipt_amount, sl.doc_amount as doc_amount,
            case when rp.type='company' then round((n.amount_untaxed-sl.doc_amount-sl.receipt_amount)::numeric,2)
                   when rp.type='nat_company' then round((n.amount_untaxed-sl.doc_amount-sl.receipt_amount)::numeric,2)
                   else round((n.amount_untaxed-sl.doc_amount-sl.receipt_amount)::numeric,2)
                   end as net_amount,
                   round(n.more_amount::numeric,2) as more_amount,
                   round(av.amount+n.more_amount::numeric,2) as total_paid,
                   round(av.amount::numeric,2) as paid_amount,
                   round(n.amount_total - av.amount-n.more_amount::numeric,2) as balance_amount,
                   aj.id as journal_id
                   FROM
                  account_invoice n
                 left join account_invoice_line sl on (sl.invoice_id=n.id)
                 Left join product_product p on (p.id = sl.product_id)
                 left join product_template pt on (pt.id=p.product_tmpl_id)
                 left join res_partner rp on (rp.id=n.partner_id)
                 left join account_move am on (am.partner_id=rp.id)
                 left join account_journal aj on (am.journal_id=aj.id)
                 left join account_voucher av on (av.journal_id=aj.id)
                 where n.state in ('open','paid') and rp.customer = True and  n.type='out_invoice' 
                 and aj.type in ('bank','cash') and aj.code !='MRP' and av.partner_id=rp.id and av.type='receipt' and av.period_id=n.period_id
                 group by n.state, n.partner_id,n.date_invoice, pt.local_type, rp.type,n.inspector_id, n.tstp_id, n.id,sl.doc_amount,sl.receipt_amount,n.more_amount,aj.id,av.amount)""")