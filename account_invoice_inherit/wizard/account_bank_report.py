# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import timedelta
from lxml import etree
from openerp.osv import fields, osv
from openerp.tools.translate import _
from operator import itemgetter
import xlrd, base64, time, datetime, xlwt, openerp.netsvc, datetime
from StringIO import StringIO
from openerp.addons.delivery.sale import sale_order


class account_bank_report(osv.osv_memory):
    _inherit = "abstract.report.excel"
    _name = 'account.bank.report'
    _description = 'Account bank invoice Report'
    
    _columns = {
                'company_id':fields.many2one('res.company',u'Компани'),
                'bank_type':fields.selection([
                                              ('khas',u'Хас банк'),
                                              ('hhb',u'ХХБанк'),
                                              ('capitron',u'Капитрон банк'),
                                             ],u'Банк',required=True),
                "type":fields.selection([('self',u'Айл өрх'),
                                 ('company',u'ААН Байгууллага'),
                                 ],u'Төрөл',required=True),
                'period_id':fields.many2one('account.period',u'Сар',required=True),
        }
    
    _defaults = {
            'company_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'account.bank.report', context=ctx),
        }
    
    def get_export_data(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        form = self.browse(cr, uid, ids[0], context=context)
        datas = {'ids': context.get('active_ids', [])}
        datas['form'] = self.read(cr, uid, ids)[0]
        data = datas['form']
        company = self.pool.get('res.company').browse(cr, uid, data['company_id'][0], context=context)   
        pre_account_id = False
        account_obj = self.pool.get('account.invoice')
        period_obj = self.pool.get('account.period')
        period = period_obj.browse(cr, uid, data['period_id'][0])
        account_id = account_obj.search(cr, uid, [('period_id','=',data['period_id'][0])])
        pre_period_id = period_obj.search(cr, uid, [('id','<',period.id)],limit=1)
        if pre_period_id:
            pre_period = period_obj.browse(cr, uid, pre_period_id)
            pre_account_id = account_obj.search(cr, uid, [('period_id','=',pre_period.id)])
        
        
        book = xlwt.Workbook(encoding='utf8')
        months = 'OCT'
        
        sheet = book.add_sheet('%s'%months)
        styles = self.get_easyxf_styles()
        ezxf = xlwt.easyxf
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold on; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')


        if data['bank_type'] == 'khas':
            
            sheet.write(1, 0, u'%s' %period.date_start[:4], ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 180'))
                    
            sheet.write(1, 1, u'%s'%period.date_start[5:7] , ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 180'))
            
            rowx = 4
            sheet.write(rowx,  0, u'Байр', data_center)
            sheet.write(rowx,  1, u'Корпус', data_center)
            sheet.write(rowx,  2, u'Хаалга', data_center)
            sheet.write(rowx,  3, u'Өрх', data_center)
            sheet.write(rowx,  4, u'Оршин суугчдын нэрс', data_center)
            sheet.write(rowx,  5, u'Ам бүл', data_center)
            sheet.write(rowx,  6, u'Код', data_center)
            sheet.write(rowx,  7, u'Талбай', data_center)
            sheet.write(rowx,  8, u'Холбоо барих', data_center)
            sheet.write(rowx,  9, u'Дулаан', data_center)
            sheet.write(rowx,  10, u'Засвар үйлчилгээ', data_center)
            sheet.write(rowx,  11, u'Суурь хураамж', data_center)
            sheet.write(rowx,  12, u'Нийт', data_center)
            
            rowx = 5
            
            
            if data['type'] == 'self':
                for line in account_obj.browse(cr, uid, account_id):
                    total_amount = 0.0
                    if line.partner_id.type == 'self' or line.partner_id.type == 'work':
                        doc_amount = 0.0
                        subtotal = 0.0
                        pre_amount = 0.0
                        sheet.write(rowx, 0, line.partner_id.street, data_left)
                        sheet.write(rowx, 1, 0, data_left)
                        sheet.write(rowx, 2, line.partner_id.street2, data_left)
                        sheet.write(rowx, 3, 0, data_left)
                        sheet.write(rowx, 4, u'%s'%line.partner_id.name, data_right)
                        sheet.write(rowx, 5, 0, data_left)
                        sheet.write(rowx, 6, line.partner_id.code, data_left)
                        sheet.write(rowx, 7, line.partner_id.square_amount, data_left)
                        sheet.write(rowx, 8, line.partner_id.phone, data_left)
                        
                        for lin in line.invoice_line:
                            subtotal +=lin.price_subtotal
                            sheet.write(rowx, 9, lin.price_subtotal-lin.doc_amount, data_right)
                            sheet.write(rowx, 10, 0.0, data_left)
                            doc_amount +=lin.doc_amount
                            sheet.write(rowx, 11, lin.doc_amount, data_right)
                        if pre_account_id !=False:
                            for lines in account_obj.browse(cr, uid, pre_account_id):
                                if lines.type =='out_invoice':
                                    if line.partner_id.id == lines.partner_id.id:
                                        pre_amount = lines.amount_total
                                elif lines.type =='in_invoice':
                                    if line.partner_id.id == lines.partner_id.id:
                                        pre_amount = -lines.amount_total
                                
                        sheet.write(rowx, 12, line.amount_tax+line.amount_untaxed+pre_amount, data_right)
                        rowx +=1
            elif data['type']=='company':
                for line in account_obj.browse(cr, uid, account_id):
                    total_amount = 0.0
                    if line.partner_id.type == 'company':
                        doc_amount = 0.0
                        subtotal = 0.0
                        pre_amount = 0.0
                        sheet.write(rowx, 0, line.partner_id.street, data_left)
                        sheet.write(rowx, 1, 0, data_left)
                        sheet.write(rowx, 2, line.partner_id.street2, data_left)
                        sheet.write(rowx, 3, 0, data_left)
                        sheet.write(rowx, 4, u'%s'%line.partner_id.name, data_right)
                        sheet.write(rowx, 5, 0, data_left)
                        sheet.write(rowx, 6, line.partner_id.code, data_left)
                        sheet.write(rowx, 7, line.partner_id.square_amount, data_left)
                        sheet.write(rowx, 8, line.partner_id.phone, data_left)
                        
                        for lin in line.invoice_line:
                            subtotal +=lin.price_subtotal
                            sheet.write(rowx, 9, lin.price_subtotal-lin.doc_amount, data_right)
                            sheet.write(rowx, 10, 0.0, data_left)
                            doc_amount +=lin.doc_amount
                            sheet.write(rowx, 11, lin.doc_amount, data_right)
                        if pre_account_id !=False:
                            for lines in account_obj.browse(cr, uid, pre_account_id):
                                if lines.type =='out_invoice':
                                    if line.partner_id.id == lines.partner_id.id:
                                        pre_amount = lines.amount_total
                                elif lines.type =='in_invoice':
                                    if line.partner_id.id == lines.partner_id.id:
                                        pre_amount = -lines.amount_total
                                
                        sheet.write(rowx, 12, line.amount_tax+line.amount_untaxed+pre_amount, data_right)
                        rowx +=1
                    
        elif data['bank_type'] =='hhb' or  data['bank_type'] =='capitron':
            rowx = 0
            sheet.write(rowx,  0, u'Билл огноо', data_center)
            sheet.write(rowx,  1, u'Билл дугаар', data_center)
            sheet.write(rowx,  2, u'Код', data_center)
            sheet.write(rowx,  3, u'Нэр', data_center)
            sheet.write(rowx,  4, u'Утас', data_center)
            sheet.write(rowx,  5, u'Имэйл', data_center)
            sheet.write(rowx,  6, u'Байр', data_center)
            sheet.write(rowx,  7, u'Тоот', data_center)
            sheet.write(rowx,  8, u'Талбай', data_center)
            sheet.write(rowx,  9, u'Халсан хоног', data_center)
            sheet.write(rowx,  10, u'Билл үүссэн огноо', data_center)
            sheet.write(rowx,  11, u'Эхлэх огноо', data_center)
            sheet.write(rowx,  12, u'Дуусах огноо', data_center)
            sheet.write(rowx,  13, u'Халаалт', data_center)
            sheet.write(rowx,  14, u'Суурь', data_center)
            sheet.write(rowx,  15, u'Төлбөрт үйлчилгээ', data_center)
            sheet.write(rowx,  16, u'НӨАТ', data_center)
            sheet.write(rowx,  17, u'Билл дүн', data_center)
            sheet.write(rowx,  18, u'Өмнөх үлдэгдэл', data_center)
            sheet.write(rowx,  19, u'Нийт төлөх төлбөр', data_center)
            
            rowx = 1
            if data['type']=='self':
                for line in account_obj.browse(cr, uid, account_id):
                    total_amount = 0.0
                    if line.partner_id.type == 'self' or line.partner_id.type == 'work':
                        doc_amount = 0.0
                        subtotal = 0.0
                        pre_amount = 0.0
                        sheet.write(rowx, 0, line.date_invoice, data_left)
                        sheet.write(rowx, 1, line.name, data_left)
                        sheet.write(rowx, 2, line.partner_id.code, data_left)
                        sheet.write(rowx, 3, u'%s'%line.partner_id.name, data_left)
                        sheet.write(rowx, 4, line.partner_id.phone, data_right)
                        sheet.write(rowx, 5, u'%s'%line.partner_id.email or '', data_left)
                        sheet.write(rowx, 6, line.partner_id.street, data_left)
                        sheet.write(rowx, 7, line.partner_id.street2, data_left)
                        sheet.write(rowx, 8, line.partner_id.square_amount, data_left)
    
                        for lin in line.invoice_line:
                            subtotal +=lin.price_subtotal
                            sheet.write(rowx, 9, lin.warmed_day, data_left)
                            sheet.write(rowx, 10, line.date_invoice, data_left)
                            sheet.write(rowx, 11, period.date_start, data_left)
                            sheet.write(rowx, 12, period.date_stop, data_right)
                            sheet.write(rowx, 13, lin.price_subtotal-lin.doc_amount, data_right)
                            sheet.write(rowx, 14, lin.doc_amount, data_right)
                            sheet.write(rowx, 15, 0, data_right)
                            sheet.write(rowx, 16, line.amount_tax, data_right)
                            sheet.write(rowx, 17, lin.price_subtotal, data_right)
                            if pre_account_id !=False:
                                for lines in account_obj.browse(cr, uid, pre_account_id):
                                    if lines.type =='out_invoice':
                                        if line.partner_id.id == lines.partner_id.id:
                                            pre_amount = lines.amount_total
                                    elif lines.type =='in_invoice':
                                        if line.partner_id.id == lines.partner_id.id:
                                            pre_amount = -lines.amount_total
                            sheet.write(rowx, 18, pre_amount, data_left)
                            sheet.write(rowx, 19, line.amount_tax+line.amount_untaxed+pre_amount, data_right)
                        rowx +=1
            elif data['type']=='company':
                for line in account_obj.browse(cr, uid, account_id):
                    total_amount = 0.0
                    if line.partner_id.type == 'company':
                        doc_amount = 0.0
                        subtotal = 0.0
                        pre_amount = 0.0
                        sheet.write(rowx, 0, line.date_invoice, data_left)
                        sheet.write(rowx, 1, line.name, data_left)
                        sheet.write(rowx, 2, line.partner_id.code, data_left)
                        sheet.write(rowx, 3, u'%s'%line.partner_id.name, data_left)
                        sheet.write(rowx, 4, line.partner_id.phone, data_right)
                        sheet.write(rowx, 5, u'%s'%line.partner_id.email or '', data_left)
                        sheet.write(rowx, 6, line.partner_id.street, data_left)
                        sheet.write(rowx, 7, line.partner_id.street2, data_left)
                        sheet.write(rowx, 8, line.partner_id.square_amount, data_left)
    
                        for lin in line.invoice_line:
                            subtotal +=lin.price_subtotal
                            sheet.write(rowx, 9, lin.warmed_day, data_left)
                            sheet.write(rowx, 10, line.date_invoice, data_left)
                            sheet.write(rowx, 11, period.date_start, data_left)
                            sheet.write(rowx, 12, period.date_stop, data_right)
                            sheet.write(rowx, 13, lin.price_subtotal-lin.doc_amount, data_right)
                            sheet.write(rowx, 14, lin.doc_amount, data_right)
                            sheet.write(rowx, 15, 0, data_right)
                            sheet.write(rowx, 16, line.amount_tax, data_right)
                            sheet.write(rowx, 17, lin.price_subtotal, data_right)
                            if pre_account_id !=False:
                                for lines in account_obj.browse(cr, uid, pre_account_id):
                                    if lines.type =='out_invoice':
                                        if line.partner_id.id == lines.partner_id.id:
                                            pre_amount = lines.amount_total
                                    elif lines.type =='in_invoice':
                                        if line.partner_id.id == lines.partner_id.id:
                                            pre_amount = -lines.amount_total
                            sheet.write(rowx, 18, pre_amount, data_left)
                            sheet.write(rowx, 19, line.amount_tax+line.amount_untaxed+pre_amount, data_right)
                        rowx +=1
        
        
        
        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 3 * inch
        sheet.col(2).width = 3 * inch
        sheet.col(3).width = 5 * inch
        sheet.col(4).width = 5 * inch
        sheet.col(5).width = 2 * inch
        sheet.col(6).width = 3 * inch
        sheet.col(7).width = 4 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.row(4).height = 1000

        

        return {'data':book, 'directory_name':u'Төлбөрийн банкны тайлан', 
                }

    