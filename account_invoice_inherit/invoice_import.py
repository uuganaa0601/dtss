# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import base64
import os
import math
from openerp import tools
from openerp.osv import fields,osv
from openerp.tools.translate import _
from openerp.tools.misc import get_iso_codes

from tempfile import NamedTemporaryFile
from datetime import date
import time, datetime
from datetime import datetime
import string
import xlwt, xlrd, time



def ean_checksum(eancode):
    """returns the checksum of an ean string of length 13, returns -1 if the string has the wrong length"""
    if len(eancode) != 13:
        return -1
    oddsum=0
    evensum=0
    total=0
    eanvalue=eancode
    reversevalue = eanvalue[::-1]
    finalean=reversevalue[1:]

    for i in range(len(finalean)):
        if i % 2 == 0:
            oddsum += int(finalean[i])
        else:
            evensum += int(finalean[i])
    total=(oddsum * 3) + evensum

    check = int(10 - math.ceil(total % 10.0)) %10
    return check

def check_ean(eancode):
    """returns True if eancode is a valid ean13 string, or null"""
    if not eancode:
        return True
    if len(eancode) != 13:
        return False
    try:
        int(eancode)
    except:
        return False
    return ean_checksum(eancode) == int(eancode[-1])

def sanitize_ean13(ean13):
    """Creates and returns a valid ean13 from an invalid one"""
    if not ean13:
        return "0000000000000"
    ean13 = re.sub("[A-Za-z]","0",ean13);
    ean13 = re.sub("[^0-9]","",ean13);
    ean13 = ean13[:13]
    if len(ean13) < 13:
        ean13 = ean13 + '0' * (13-len(ean13))
    return ean13[:-1] + str(ean_checksum(ean13))

class product_import(osv.osv_memory):
    _name = "product.import"

    _columns = {
        'budget': fields.binary('Импортлох файл'),
        'bank': fields.selection([('haan','Хаан банк'),
                                           ('hhb','Худалдаа хөгжлийн банк'),
                                           ('tb','Төрийн банк'),
                                           ('golomt','Голомт банк'),
                                           ('capital','Капитал банк'),
                                           ('capitron','Капитрон банк'),
                                           ('khas','Хас банк'),
                                           ('tsalin','Цалин тооцоо')], 'Банк', required=True),
    }
    
    def act_product(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)[0]
        fileobj = NamedTemporaryFile('w+')
        fileobj.write(base64.decodestring(this.budget))
        fileobj.seek(0)
         
        if not os.path.isfile(fileobj.name):
            raise osv.except_osv(u'Алдаа',u'Мэдээллийн файлыг уншихад алдаа гарлаа.\nЗөв файл эсэхийг шалгаад дахин оролдоно уу!')
        book = xlrd.open_workbook(fileobj.name)
        sheet = book.sheet_by_index(0)
        product_obj = self.pool.get('product.product')
        product_tmp = self.pool.get('product.template')
        trans_obj = self.pool.get('ir.translation')
        categ_obj =  self.pool.get('product.category')
        uom_obj = self.pool.get('product.uom')
        
        voucher_line_obj = self.pool.get('account.voucher.line')
        move_line_pool = self.pool.get('account.move.line')
        move_obj = self.pool.get('account.move')
        
        partner_obj = self.pool.get('res.partner')
        period_obj = self.pool.get('account.period')
        journal_obj = self.pool.get('account.journal')
        bank_obj = self.pool.get('bank.move')
        bank_line_obj = self.pool.get('bank.move.line')
        part_bank_obj = self.pool.get('res.partner.bank')
        nrows = sheet.nrows
        partner1 = False
        rowi = 1
        journal_id = journal_obj.search(cr, uid, [('bank','=',this.bank)])
        journal = journal_obj.browse(cr, uid, journal_id)
        bank_id = bank_obj.create(cr, uid, {
                                'company_id':journal.company_id.id,
                                'reg_date':time.strftime('%Y-%m-%d'),
                                'user_id':uid,
                                'state':'draft',
                                'bank_id':journal.id,
                        })
        for l in range (nrows - 1) :
            row = sheet.row(rowi)
            date = row[0].value
            partner_value = row[1].value
            total_price = row[2].value
            bank = row[3].value
            code = ''
            if type(partner_value) == float:
                vals = str(partner_value).replace(".0","")
                val = vals.split()
            elif partner_value.isspace():
                val = vals.split(" ")               
            else:
                if type(partner_value)==str:
                    vals = partner_value.replace(".0","")
                    val = vals.split()
                else:
                    vals = partner_value.replace(u"№:","")
                    val = vals.split()
            if type(bank) == float:
                bnk = str(bank).replace(".0","")
                banks = bnk.split()
            elif bank.isspace():
                banks = bank.split(" ")
            elif type(bank)==str:
                bnk = bank.replace(".0","")
                banks = bnk.split()
            
            partner = partner_obj.search(cr, uid, [('code','in',val)])
            if partner ==[]:
                partner1 = False
            else:
                partner1 = partner[0]
            
            
                
                
            p_ean13 = False
            def_code = ''
            new_code = ''
            default_code = ''
            uom_id = 1
            diff_price = 0.0
            count = 0
            if type(date)==float:
                dt = datetime.fromordinal(datetime(1900, 1, 1).toordinal() + int(date) - 2)
                tt = dt.timetuple()
                cus_date = dt
            else:
                date = date.replace(".", "/") 
                if len(date)> 10:
                   date = date[0:10]
                   cus_date = date
                else:
                   cus_date = date
            
            to_str = str(u'%s'%total_price)
            total = to_str.replace(",","")
            total = total.replace("-","")
            if total == "":
                total = 0.0
            if partner_value !=[] and total_price:
                if partner1 != False:
                   if total >= 0.0:
                       bank_line_obj.create(cr, uid, {
                                                    'bank_move_id':bank_id,
                                                    'pay_date':cus_date,
                                                    'partner_id':partner1,
                                                    'pay_amount':float(total),
                                                    'is_cus': True,
                                                    })
                else:
                    if total >= 0.0:
                        bank_line_obj.create(cr, uid, {
                                                    'bank_move_id':bank_id,
                                                    'pay_date':cus_date,
                                                    'partner_id':partner1,
                                                    'pay_amount':float(total),
                                                    'is_cus': False,
                                                    })
                        
            rowi += 1
        return True



class bill_employee(osv.osv):
     _name = "bill.employee"
     
     _columns = {
          'name': fields.char('Дугаар'),
          'date': fields.date('Огноо'),
          'period_id': fields.many2one('account.period','Мөчлөг'),
          'line_ids': fields.one2many('emp.line','bill_emp_id'),
          'state':fields.selection([('draft', 'Ноорог'),('pull', 'Ажилчдын мэдээлэл татсан'),('push', 'Баталгаажсан')], 'Төлөв'),
          'journal_id': fields.many2one('account.journal','Журнал',required=True),
         }
     _defaults = {
           'name': lambda obj, cr, uid, context: '/',
           'state': 'draft'       
                  }
  
     def create(self, cr, uid, vals, context=None):
        if vals.get('name','/')=='/':
            vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'bill.employee') or '/'
        context = dict(context or {}, mail_create_nolog=True)
        order =  super(bill_employee, self).create(cr, uid, vals, context=context)
        return order
     
     def action_to_pull(self, cr, uid, ids, *args):
        
        for bill in self.browse(cr, uid, ids):
            partner_obj = self.pool.get('res.partner')
            inv_obj = self.pool.get('account.invoice')
            emp_ids = partner_obj.search(cr,uid,[('type', '=', 'work')])
            if emp_ids:
               for emp_id in emp_ids:
                   total = 0
                   line_obj = self.pool.get('emp.line')
                   inv_ids = inv_obj.search(cr,uid,[('partner_id', '=', emp_id),('date_invoice', '>=', bill.period_id.date_start),('date_invoice', '<=', bill.period_id.date_stop),('state','=','open')])
                   
                   if inv_ids:
                      for inv_id in inv_obj.browse(cr, uid,inv_ids):
                          total = inv_id.amount_total
                          line_obj.create(cr, uid,{
                                    'bill_emp_id': bill.id,
                                    'emp_id': emp_id,
                                    'total':total,
                                    
                                    } )
            else:
                raise osv.except_osv(u'Анхааруулга',u'Үүссэн харилцагч дунд ажилтан байхгүй байна. Хаягийн төрөл талбарт ажилтан гэж сонгосон харилцагч нар энд дуудагдана.!')
                   
        self.write(cr, uid, ids, {'state': 'pull'})
        return True
    
     def action_to_push(self, cr, uid, ids, context=None):
        for bill in self.browse(cr, uid, ids):
            if not bill.line_ids:
               raise osv.except_osv(u'Анхааруулга',u'Ажилчдын мэдээлэл хоосон байна.!')
            for line in bill.line_ids:
                invoice_obj = self.pool.get('account.invoice') 
                voucher_obj = self.pool.get('account.voucher')
                voucher_line_obj = self.pool.get('account.voucher.line') 
                move_line_pool = self.pool.get('account.move.line')
                period_obj = self.pool.get('account.period')
                journal_obj = self.pool.get('account.journal')
                inv_ids = invoice_obj.search(cr, uid, [('partner_id','=',line.emp_id.id),('state','=','open')],order='date_invoice asc')
                if not inv_ids:
                   raise osv.except_osv(u'Анхааруулга',u'Ажилтан дээр үүссэн нэхэмжлэл байхгүй байна.!')
                diff_price = 0.0
                for inv_id in invoice_obj.browse(cr, uid,inv_ids):
                    account_type = self.pool['account.account'].browse(cr, uid, inv_id.account_id.id, context=context).type
                    line_ids = move_line_pool.search(cr, uid, [('state','=','valid'), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', line.emp_id.id)], context=context)
                    date1 = datetime.now().strftime ("%Y%m%d"),
                    period_id = period_obj.search(cr, uid, [('date_start','<=', date1),('date_stop','>=', date1)])
                    journal_id = journal_obj.search(cr, uid, [('type','=', 'cash')])
                    journal = journal_obj.browse(cr, uid, journal_id[0])
                    if line.total >= inv_id.amount_total:
                       #zasah
                       vou_id = voucher_obj.create(cr, uid,{
                                'partner_id': line.emp_id.id,
                                'amount': line.total,
                                'date': date1,
                                'writeoff_amount':0.0,
                                'period_id': period_id[0],
                                'journal_id': bill.journal_id.id,
                                'account_id': bill.journal_id.default_debit_account_id.id,
                                'state':'posted',
                                'type':'receipt',
                                'pre_line':True,
                                } )
                       diff_price = line.total
                       voucher_line_obj.create(cr, uid, {
                                                    'voucher_id':vou_id,
                                                    'partner_id':line.emp_id.id,
                                                    'amount':diff_price,
                                                    'account_id':inv_id.account_id.id,
                                                    'move_line_id':line_ids[0],
                                                    'type':'cr'
                                                    })
                    else:
                       vou_id = voucher_obj.create(cr, uid,{
                                'partner_id': line.emp_id.id,
                                'amount': inv_id.amount_total,
                                'date': date1,
                                'period_id': period_id[0],
                                'journal_id': bill.journal_id.id,
                                'writeoff_amount':0.0,
                                'account_id': bill.journal_id.default_debit_account_id.id,
                                'state':'posted',
                                'pre_line':True,
                                'type':'receipt'
                                
                                } )
                       diff_price = inv_id.amount_total-line.total
                    
                       voucher_line_obj.create(cr, uid, {
                                                    'voucher_id':vou_id,
                                                    'partner_id':line.emp_id.id,
                                                    'amount':diff_price,
                                                    'account_id':inv_id.account_id.id,
                                                    'move_line_id':line_ids[0],
                                                    'type':'cr'
                                                    })
                    context = {
                                'payment_expected_currency': inv_id.currency_id.id,
                                'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv_id.partner_id).id,
                                'default_amount': inv_id.type in ('out_refund', 'in_refund') and -inv_id.residual or inv_id.residual,
                                'default_reference': inv_id.name,
                                'close_after_process': True,
                                'invoice_type': inv_id.type,
                                'invoice_id': inv_id.id,
                                'default_type': inv_id.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                'type': inv_id.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
                                }
                    
                    voucher_obj.button_proforma_voucher(cr, uid, [vou_id], context=context)
        self.write(cr, uid, ids, {'state': 'push'})
        return True
    
     def onchange_date(self, cr, uid, ids, date,  context=None):
        if date:
           period_obj = self.pool.get('account.period')  
           period_id = period_obj.search(cr, uid, [('date_start','<=', date),('date_stop','>=', date)])
           return {'value': {
                             'period_id': period_id[0]
                             }}

class emp_line(osv.osv):
     _name = "emp.line"
     
     _columns = {
          'bill_emp_id': fields.many2one('bill.employee','Бүртгэлийн дугаар'),
          'emp_id': fields.many2one('res.partner','Ажилтан'),
          'total': fields.float('Төлж буй дүн'),
          'pre_amount':fields.float(u'Эхний үлдэгдэл'),
          'paid_amount':fields.float(u'Төлөх дүн'),
          'diff_amount':fields.float(u'Зөрүү дүн'),
 
         }

