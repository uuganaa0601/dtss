# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import base64
import os
import math
from openerp import tools
from openerp.osv import fields,osv
from openerp.tools.translate import _
from openerp.tools.misc import get_iso_codes
import calendar

from tempfile import NamedTemporaryFile
from datetime import datetime, timedelta, date
import datetime
import xlwt, xlrd, time
from openerp.addons.account_voucher.invoice import invoice

class bank_move(osv.osv):
    _name = 'bank.move'
    
    
    def _get_total_amount(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context):
            total_amount = 0.0
            for lines in line.line_id1:
                total_amount +=lines.pay_amount
            res[line.id] = total_amount
        return res
    _columns = {
                'name':fields.char(u'Дугаар'),
                'company_id':fields.many2one('res.company',u'Компани'),
                'reg_date':fields.date(u'Огноо',required=True),
                'user_id':fields.many2one('res.users',u'Бүртгэсэн ажилтан'),
                'line_id':fields.one2many('bank.move.line','bank_move_id','Bank move line',domain=[('is_cus','=',False)]),
                'line_id1':fields.one2many('bank.move.line','bank_move_id','Bank move line',domain=[('is_cus','=',True)]),
                'bank_id':fields.many2one('account.journal',u'Журнал',domain=[('type','=','bank')]),
                'state':fields.selection([('draft',u'Ноорог'),
                                          ('more_payment',u'Илүү төлөлт шалгасан'),
                                          ('confirm',u'Батлагдсан')],u'Төлөв'),
                'total_amount':fields.function(_get_total_amount,type="float",string=u'Нийт'),
                }
    
    _defaults = {
                'name':lambda self, cr, uid, c:self.pool.get('ir.sequence').get(cr, uid, 'bank.move', context=c),
                'state':'draft',
                'company_id': lambda self, cr, uid, c: self.pool.get('res.company')._company_default_get(cr, uid, 'vat.information', context=c),
                'reg_date':lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
                'user_id':lambda self, cr, uid, c: uid,
                }
    
    
    def payment_in_voucher_line_obj(self, cr, uid, voucher_id,pay_amount, payment_amount, line,inv_ids,move_line,amount_unreconciled):
        voucher_line_obj = self.pool.get('account.voucher.line')
        voucher_line_id = voucher_line_obj.create(cr, uid, {
                                            'voucher_id':voucher_id,
                                            'partner_id':line.partner_id.id,
                                            'amount':pay_amount,
                                            'amount_original':payment_amount,
                                            'account_id':inv_ids.account_id.id,
                                            'move_line_id':move_line,
                                            'amount_unreconciled': amount_unreconciled,
                                            'type':'dr'
                                            })
        
        return voucher_line_id
    
    def payment_in_voucher_obj(self, cr, uid, line, pay_amount,period_id,obj,journal_id):
        voucher_obj = self.pool.get('account.voucher')
        voucher_id = voucher_obj.create(cr, uid,{
                                        'partner_id': line.partner_id.id,
                                        'amount': pay_amount,
                                        'date': line.pay_date,
                                        'writeoff_amount':0.0,
                                        'period_id': period_id,
                                        'journal_id': journal_id,
                                        'account_id': obj.bank_id.default_debit_account_id.id,
                                        'state':'posted',
                                        'type':'payment',
                                        'pre_line':True,
                                        })
        return voucher_id
    
    
    
    
    def payment_voucher_line_obj(self, cr, uid, voucher_id,pay_amount, payment_amount, line,inv_ids,journal_id,move_line,amount_unreconciled):
        voucher_line_obj = self.pool.get('account.voucher.line')
        journal_obj = self.pool.get('account.journal')
        journal_id = journal_obj.browse(cr, uid, journal_id)
        recon = False
        if pay_amount == amount_unreconciled:
            recon = True
        voucher_line_id = voucher_line_obj.create(cr, uid, {
                                            'voucher_id':voucher_id,
                                            'partner_id':line.partner_id.id,
                                            'amount':pay_amount,
                                            'amount_original':payment_amount,
                                            'account_id':inv_ids.account_id.id,
                                             'currency_id': journal_id.company_id.currency_id.id,
                                            'move_line_id':move_line,
                                            'reconcile':recon,
                                            'amount_unreconciled': amount_unreconciled,
                                            'type':'cr'
                                            })
        
        return voucher_line_id
    
    def payment_voucher_obj(self, cr, uid, line, pay_amount,period_id,obj,journal_id):
        voucher_obj = self.pool.get('account.voucher')
        journal_obj = self.pool.get('account.journal')
        journal_ids = journal_obj.browse(cr, uid, journal_id)
        voucher_id = voucher_obj.create(cr, uid,{
                                        'partner_id': line.partner_id.id,
                                        'amount': pay_amount,
                                        'date': line.pay_date,
                                        'writeoff_amount':0.0,
                                        'period_id': period_id,
                                        'journal_id': journal_id,
                                        'currency_id': journal_ids.company_id.currency_id.id,
                                        'account_id': obj.bank_id.default_debit_account_id.id,
                                        'state':'posted',
                                        'type':'receipt',
                                        'pre_line':True,
                                        })
        return voucher_id
    
    def action_more_payment(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids)
        inv_obj = self.pool.get('account.invoice')
        move_line_obj = self.pool.get('account.move.line')
        journal_obj = self.pool.get('account.journal')
        voucher_obj = self.pool.get('account.voucher')
        period_obj = self.pool.get('account.period')
        for line in obj.line_id1:
            period_id = period_obj.search(cr, uid, [('date_start','<=', line.pay_date),('date_stop','>=', line.pay_date)])
            in_invoice_id = inv_obj.search(cr, uid, [('partner_id','=',line.partner_id.id),('state','=','open'),('type','=','in_invoice'),('period_id','<',period_id[0])])
            out_invoice_id = inv_obj.search(cr, uid, [('partner_id','=',line.partner_id.id),('state','=','open'),('type','=','out_invoice'),('period_id','<',period_id[0])])
            now_out_invoice_id = inv_obj.search(cr, uid, [('partner_id','=',line.partner_id.id),('state','=','open'),('type','=','out_invoice'),('period_id','=',period_id[0])])
            more_amount = 0.0
            out_amount = 0.0
            diff_amount = 0.0
            pay_amount = 0.0
            
            if in_invoice_id:
                for inv_line in inv_obj.browse(cr, uid, in_invoice_id):
                    if line.partner_id.id == inv_line.partner_id.id:
                        account_type = self.pool['account.account'].browse(cr, uid, inv_line.account_id.id, context=context).type
                        line_ids = move_line_obj.search(cr, uid, [('state','=','valid'),('period_id','=',inv_line.period_id.id), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', inv_line.partner_id.id)], context=context)
                        journal_id = journal_obj.search(cr, uid, [('type','=', 'cash'),('code','=','MRP')])
                        if journal_id:
                            journal = journal_obj.browse(cr, uid, journal_id[0])
                        else:
                            raise osv.except_osv(u'Анхааруулга',u'Илүү төлөлтийн журнал үүсгэнэ үү.!')
                        
                        more_amount -=inv_line.amount_total
                        voucher_id = self.payment_in_voucher_obj(cr, uid, line,inv_line.amount_total, period_id[0],obj,journal_id[0])
                        amount_unrecon = inv_line.amount_total-inv_line.amount_total
                        voucher_line = self.payment_in_voucher_line_obj(cr, uid,voucher_id,inv_line.amount_total,inv_line.amount_total, line,inv_line,line_ids[0],amount_unrecon)                
                        context = {
                                    'payment_expected_currency': inv_line.currency_id.id,
                                    'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv_line.partner_id).id,
                                    'default_amount': inv_line.type in ('out_refund', 'in_refund') and -inv_line.residual or inv_line.residual,
                                    'default_reference': inv_line.name,
                                    'close_after_process': True,
                                    'invoice_type': inv_line.type,
                                    'invoice_id': inv_line.id,
                                    'default_type': inv_line.type in ('in_invoice','out_refund') and 'receipt' or 'payment',
                                    'type': inv_line.type in ('in_invoice','out_refund') and 'receipt' or 'payment'
                                    }
                    voucher_obj.button_proforma_voucher(cr, uid, [voucher_id], context=context)
                    
                for out_line in inv_obj.browse(cr, uid, out_invoice_id):
                    if line.partner_id.id == inv_line.partner_id.id:
                        if out_line.amount_total > more_amount:
                            account_type = self.pool['account.account'].browse(cr, uid, out_line.account_id.id, context=context).type
                            line_ids = move_line_obj.search(cr, uid, [('state','=','valid'),('period_id','=',out_line.period_id.id), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', inv_line.partner_id.id)], context=context)
                            journal_id = journal_obj.search(cr, uid, [('type','=', 'cash'),('code','=','MRP')])
                            journal = journal_obj.browse(cr, uid, journal_id[0])
                            amount_recon = out_line.amount_total-more_amount
                            voucher_id = self.payment_voucher_obj(cr, uid, line,more_amount, period_id[0],obj,journal_id[0])
                            voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,more_amount,out_line.amount_total, line,out_line,journal_id[0],line_ids[0],amount_recon)                
                           
                            context = {
                                        'payment_expected_currency': out_line.currency_id.id,
                                        'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(out_line.partner_id).id,
                                        'default_amount': out_line.type in ('out_refund', 'in_refund') and -out_line.residual or out_line.residual,
                                        'default_reference': out_line.name,
                                        'close_after_process': True,
                                        'invoice_type': out_line.type,
                                        'invoice_id': out_line.id,
                                        'default_type': out_line.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                        'type': out_line.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
                                        }
                            
                            voucher_obj.button_proforma_voucher(cr, uid, [voucher_id], context=context)
                        else:
                            if diff_amount >=out_line.amount_total:
                                diff_amount = diff_amount-out_line.amount_total
                                account_type = self.pool['account.account'].browse(cr, uid, out_line.account_id.id, context=context).type
                                line_ids = move_line_obj.search(cr, uid, [('state','=','valid'),('period_id','=',out_line.period_id.id), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', inv_line.partner_id.id)], context=context)
                                journal_id = journal_obj.search(cr, uid, [('type','=', 'cash'),('code','=','MRP')])
                                journal = journal_obj.browse(cr, uid, journal_id[0])
                                amount_recon = out_line.amount_total-out_line.amount_total
                                voucher_id = self.payment_voucher_obj(cr, uid, line,out_line.amount_total, period_id[0],obj,journal_id[0])
                                voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,out_line.amount_total,out_line.amount_total, line,out_line,journal_id[0],line_ids[0],amount_recon)                
                               
                                context = {
                                            'payment_expected_currency': out_line.currency_id.id,
                                            'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(out_line.partner_id).id,
                                            'default_amount': out_line.type in ('out_refund', 'in_refund') and -out_line.residual or out_line.residual,
                                            'default_reference': out_line.name,
                                            'close_after_process': True,
                                            'invoice_type': out_line.type,
                                            'invoice_id': out_line.id,
                                            'default_type': out_line.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                            'type': out_line.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
                                            }
                                
                                voucher_obj.button_proforma_voucher(cr, uid, [voucher_id], context=context)
                                if diff_amount >0.0:
                                    invoice_ids = invoice_obj.create(cr, uid, {
                                                                'inspector_id':out_line.partner_id.inspector_id.id or False,
                                                                'name':out_line.partner_id.code,
                                                                'origin': obj.name,
                                                                'date_invoice':out_line.pay_date,
                                                                'type': 'in_invoice',
                                                                'period_id': out_line.period_id.id,
                                                                'state':'open',
                                                                'account_id': out_line.partner_id.property_account_receivable.id,
                                                                'partner_id': out_line.partner_id.id,
                                                                'journal_id': journal_id[0],
                                                                'payment_term': out_line.payment_term and out_line.payment_term.id or False,
                                                                'fiscal_position': out_line.fiscal_position.id,
                                                                'company_id': lined.company_id.id,
                                                                'user_id': out_line.user_id and out_line.user_id.id or False
                                        })
                                    for lin in lined.invoice_line:
                                        invoice_line_obj.create(cr, uid, {
                                                                        'invoice_id':invoice_ids,
                                                                        'name': lin.name,
                                                                        'sequence': out_line.sequence,
                                                                        'origin': obj.name,
                                                                        'account_id': out_line.account_id.id,
                                                                        'price_unit': diff_amount,
                                                                        'quantity': 1.0,
                                                                        'uos_id': out_line.uos_id.id,
                                                                        'product_id': out_line.product_id.id or False,
                                                                        })
                                    
                                
                            else:
                                account_type = self.pool['account.account'].browse(cr, uid, out_line.account_id.id, context=context).type
                                line_ids = move_line_obj.search(cr, uid, [('state','=','valid'),('period_id','=',out_line.period_id.id), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', inv_line.partner_id.id)], context=context)
                                journal_id = journal_obj.search(cr, uid, [('type','=', 'cash'),('code','=','MRP')])
                                journal = journal_obj.browse(cr, uid, journal_id[0])
                                amount_recon = out_line.amount_total-more_amount
                                voucher_id = self.payment_voucher_obj(cr, uid, line,more_amount, period_id[0],obj,journal_id[0])
                                voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,more_amount,out_line.amount_total, line,out_line,journal_id[0],line_ids[0],amount_recon)                
                               
                                context = {
                                            'payment_expected_currency': out_line.currency_id.id,
                                            'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(out_line.partner_id).id,
                                            'default_amount': out_line.type in ('out_refund', 'in_refund') and -out_line.residual or out_line.residual,
                                            'default_reference': out_line.name,
                                            'close_after_process': True,
                                            'invoice_type': out_line.type,
                                            'invoice_id': out_line.id,
                                            'default_type': out_line.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                            'type': out_line.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
                                            }
                                
                                voucher_obj.button_proforma_voucher(cr, uid, [voucher_id], context=context)
            if out_invoice_id:
                for lin in inv_obj.browse(cr, uid,out_invoice_id):
                    if lin.partner_id.id==line.partner_id.id:
                        more_amount +=lin.amount_total
            if now_out_invoice_id:
                for lines in inv_obj.browse(cr, uid,now_out_invoice_id):
                    if lines.partner_id.id==line.partner_id.id:
                        pay_amount += lines.amount_total
                        if more_amount <0.0:
                            account_type = self.pool['account.account'].browse(cr, uid, lines.account_id.id, context=context).type
                            line_ids = move_line_obj.search(cr, uid, [('state','=','valid'),('period_id','=',lines.period_id.id), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', lines.partner_id.id)], context=context)
                            journal_id = journal_obj.search(cr, uid, [('type','=', 'cash'),('code','=','MRP')])
                            journal = journal_obj.browse(cr, uid, journal_id[0])
                            amount_recon = lines.amount_total+more_amount
                            voucher_id = self.payment_voucher_obj(cr, uid, line,more_amount*(-1), period_id[0],obj,journal_id[0])
                            voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,more_amount*(-1),lines.amount_total, line,lines,journal_id[0],line_ids[0],amount_recon)                
                            
                            context = {
                                        'payment_expected_currency': lines.currency_id.id,
                                        'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(lines.partner_id).id,
                                        'default_amount': lines.type in ('out_refund', 'in_refund') and -lines.residual or lines.residual,
                                        'default_reference': lines.name,
                                        'close_after_process': True,
                                        'invoice_type': lines.type,
                                        'invoice_id': lines.id,
                                        'default_type': lines.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                        'type': lines.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
                                        }
                             
                            voucher_obj.button_proforma_voucher(cr, uid, [voucher_id], context=context)
                        
                        
            self.pool.get('bank.move.line').write(cr, uid, line.id,{ 'paid_amount':pay_amount,
                                                                    'pre_amount':more_amount})
        self.write(cr, uid,ids, {'state':'more_payment',
                                })
        return True
                        
                
                
    
    def action_confirm(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids)
        voucher_obj = self.pool.get('account.voucher')
        voucher_line_obj = self.pool.get('account.voucher.line')
        move_line_pool = self.pool.get('account.move.line')
        product_obj = self.pool.get('product.product')
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        period_obj = self.pool.get('account.period')
        journal_obj = self.pool.get('account.journal')
        bank_line_obj = self.pool.get('bank.move.line')
        for line in obj.line_id1:
            account_invoice = []
            voucher_ids = voucher_obj.search(cr, uid, [('partner_id','=',line.partner_id.id),('date','=',line.pay_date),('amount','=',line.pay_amount)])
            if not voucher_ids:
                period_id = period_obj.search(cr, uid, [('date_start','<=', line.pay_date),('date_stop','>=', line.pay_date)])
                pre_period_id = period_obj.search(cr, uid,[('id','<',period_id[0])] ,order='date_start desc',limit=1)
                inv_ids = invoice_obj.search(cr, uid, [('partner_id','=',line.partner_id.id),('state','=','open'),('type','=','out_invoice'),('period_id','=',period_id[0])],order='date_invoice asc')
                pre_inv_ids = invoice_obj.search(cr, uid, [('partner_id','=',line.partner_id.id),('state','=','open'),('type','=','out_invoice'),('period_id','<',period_id[0])],order='date_invoice asc')                
                if not inv_ids and not pre_inv_ids:
                    draft_inv_ids = invoice_obj.search(cr, uid,[('state','=','draft'),('partner_id','=',line.partner_id.id),('type','=','out_invoice')])
                    if draft_inv_ids:
                        raise osv.except_osv(u'Анхааруулга',u'%s харилцагч дээр үүссэн нэхэмжлэл батлагдаагүй байгаа эсэхийг шалгана уу.!'%line.partner_id.name)
                    else:
                        receipt_amount = 0.0
                        doc_amount = 0.0
                        invoice_ids = invoice_obj.create(cr, uid, {
                                                                 'inspector_id':line.partner_id.inspector_id.id or False,
                                                                 'name':line.partner_id.code,
                                                                 'origin': obj.name,
                                                                 'date_invoice':line.pay_date,
                                                                 'type': 'in_invoice',
                                                                 'period_id': period_id[0],
                                                                 'state':'open',
                                                                 'account_id': line.partner_id.property_account_payable.id,
                                                                 'partner_id': line.partner_id.id,
                                                                 'journal_id': obj.bank_id.id,
                                                                 'payment_term': n_inv_ids.payment_term and n_inv_ids.payment_term.id or False,
                                                                 'fiscal_position': n_inv_ids.fiscal_position.id,
                                                                 'company_id': obj.company_id.id,
                                                                 'user_id': n_inv_ids.user_id and n_inv_ids.user_id.id or False
                                    })
                        
                        if line.partner_id.is_day == True:
                            now = line.pay_date
                            days = calendar.monthrange(int(now[:4]), int(now[5:7]))[1]
                            product_id = product_obj.search(cr, uid,[('local_type','=','day')])
                            product = product_obj.browse(cr, uid, product_id)
                                                   
                            invoice_line_obj.create(cr, uid, {
                                                             'name':product.name,
                                                            'invoice_id':invoice_ids,
                                                            'product_id':product_id[0],
                                                            'price_unit':line.pay_amount,
                                                            'more_and_less_amount':0.0,
                                                            'company_id':obj.company_id.id,
                                                            #'invoice_line_tax_id': [(6, 0, [x.id for x in product.taxes_id])],
                                                             'uos_id':product.uom_id.id,
                                                             'receipt_amount':receipt_amount,
                                                             'doc_amount':doc_amount,
                                                            'quantity':1.0,
                                                            })
                        if line.partner_id.is_size == True:
                            now = line.dpay_date
                            days = calendar.monthrange(int(now[:4]), int(now[5:7]))[1]
                            product_id = product_obj.search(cr, uid,[('local_type','=','com_day')])
                            product = product_obj.browse(cr, uid, product_id)
                            invoice_line_obj.create(cr, uid, {
                                                             'name':product.name,
                                                            'invoice_id':invoice_ids,
                                                            'product_id':product_id[0],
                                                            'price_unit':line.pay_amount,
                                                            'more_and_less_amount':0.0,
                                                            'company_id':obj.company_id.id,
                                                            #'invoice_line_tax_id': [(6, 0, [x.id for x in product.taxes_id])],
                                                             'uos_id':product.uom_id.id,
                                                             'receipt_amount':receipt_amount,
                                                             'doc_amount':doc_amount,
                                                            'quantity':1.0,
                                                            })
                        if line.partner_id.is_counter == True or line.partner_id.is_dhg == True:
                            product_id = product_obj.search(cr, uid,[('local_type','=','counter')])
                            product = product_obj.browse(cr, uid, product_id)
                            invoice_line_obj.create(cr, uid, {
                                                             'name':product.name,
                                                            'invoice_id':invoice_ids,
                                                            'product_id':product_id[0],
                                                            'price_unit':line.pay_amount,
                                                            'more_and_less_amount':0.0,
                                                            'company_id':obj.company_id.id,
                                                            #'invoice_line_tax_id': [(6, 0, [x.id for x in product.taxes_id])],
                                                             'uos_id':product.uom_id.id,
                                                             'receipt_amount':receipt_amount,
                                                             'doc_amount':doc_amount,
                                                            'quantity':1.0,
                                                            })
                            
                        invoice_obj.write(cr, uid, n_inv_ids.id,{'more_amount':diff_price})
                diff_price = 0.0
                voucher_list = []
                paid_amount = 0.0
                total_amount = 0.0
                balance = 0.0
                for n_inv_ids in invoice_obj.browse(cr, uid, pre_inv_ids):
                    account_type = self.pool['account.account'].browse(cr, uid, n_inv_ids.account_id.id, context=context).type
                    now_line_ids = move_line_pool.search(cr, uid, [('state','=','valid'),('period_id','=',period_id[0]), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', line.partner_id.id)], context=context)
                    line_ids = move_line_pool.search(cr, uid, [('state','=','valid'),('period_id','<',period_id[0]), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', line.partner_id.id)], context=context)

                    journal_id = journal_obj.search(cr, uid, [('type','=', 'sale')])
                    journal = journal_obj.browse(cr, uid, journal_id[0])
                    if line.partner_id.id == n_inv_ids.partner_id.id:
                        if len(pre_inv_ids) > 1:
                            account_invoice.append(n_inv_ids.id)
                            paid_amount +=n_inv_ids.balance
                            now_payment_amount = line.pre_amount
                            if line.pay_amount >= n_inv_ids.balance:
                               pay_amount = n_inv_ids.balance
                               amount_recon = 0.0
                               voucher_id = self.payment_voucher_obj(cr, uid, line,pay_amount, period_id[0],obj,obj.bank_id.id)
                               diff_price = line.pay_amount-pay_amount
                               balance = diff_price
                               voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,pay_amount, pay_amount,line,n_inv_ids,journal_id[0],line_ids[0],amount_recon)
                            else:
                               pay_amount = n_inv_ids.balance
                               amount_recon = n_inv_ids.balance-line.pay_amount
                               voucher_id = self.payment_voucher_obj(cr, uid, line,line.pay_amount, period_id[0],obj,obj.bank_id.id)
                               voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,line.pay_amount,pay_amount, line,n_inv_ids,journal_id[0],line_ids[0],amount_recon)                
                               
                            context = {
                                        'payment_expected_currency': n_inv_ids.currency_id.id,
                                        'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(n_inv_ids.partner_id).id,
                                        'default_amount': n_inv_ids.type in ('out_refund', 'in_refund') and -n_inv_ids.residual or n_inv_ids.residual,
                                        'default_reference': n_inv_ids.name,
                                        'close_after_process': True,
                                        'invoice_type': n_inv_ids.type,
                                        'invoice_id': n_inv_ids.id,
                                        'default_type': n_inv_ids.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                        'type': n_inv_ids.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
                                        }
                            
                            voucher_list.append(voucher_id)
                            voucher_obj.button_proforma_voucher(cr, uid, [voucher_id], context=context)
                        else:
                            if line.pre_amount > 0.0:
                                now_pay_amount = line.pay_amount
                            else:
                                now_pay_amount = line.pay_amount
                            paid_amount +=n_inv_ids.balance
                            account_invoice.append(n_inv_ids.id)
                            if diff_price > 0.0:
                                if diff_price < now_pay_amount:
                                   pay_amount = n_inv_ids.balance
                                   amount_recon = n_inv_ids.balance-diff_price
                                   voucher_id = self.payment_voucher_obj(cr, uid, line,diff_price, period_id[0],obj,obj.bank_id.id)
                                   voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,diff_price,pay_amount, line,n_inv_ids,journal_id[0],line_ids[0],amount_recon)                
                                else:
                                   pay_amount = n_inv_ids.balance
                                   amount_recon = 0.0
                                   diff_price = diff_price - n_inv_ids.balance
                                   voucher_id = self.payment_voucher_obj(cr, uid, line,pay_amount, period_id[0],obj,obj.bank_id.id)
                                   voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,pay_amount,pay_amount, line,n_inv_ids,journal_id[0],line_ids[0],amount_recon)                
                                   if diff_price>0.0 and not inv_ids:
                                        #journal_ids = bank_id
                                        invoice_ids = invoice_obj.create(cr, uid, {
                                                                'inspector_id':n_inv_ids.partner_id.inspector_id.id or False,
                                                                'name':n_inv_ids.partner_id.code,
                                                                'origin': obj.name,
                                                                'date_invoice':n_inv_ids.pay_date,
                                                                'type': 'in_invoice',
                                                                'period_id': n_inv_ids.period_id.id,
                                                                'state':'open',
                                                                'account_id': n_inv_ids.partner_id.property_account_payable.id,
                                                                'partner_id': n_inv_ids.partner_id.id,
                                                                'journal_id': obj.bank_id.id,
                                                                'payment_term': n_inv_ids.payment_term and n_inv_ids.payment_term.id or False,
                                                                'fiscal_position': n_inv_ids.fiscal_position.id,
                                                                'company_id': lined.company_id.id,
                                                                'user_id': n_inv_ids.user_id and n_inv_ids.user_id.id or False
                                        })
                                        for lin in lined.invoice_line:
                                            invoice_line_obj.create(cr, uid, {
                                                                            'invoice_id':invoice_ids,
                                                                            'name': lin.name,
                                                                            'sequence': n_inv_ids.sequence,
                                                                            'origin': obj.name,
                                                                            'account_id': n_inv_ids.account_id.id,
                                                                            'price_unit': diff_amount,
                                                                            'quantity': 1.0,
                                                                            'uos_id': n_inv_ids.uos_id.id,
                                                                            'product_id': n_inv_ids.product_id.id or False,
                                                                            })
                                        invoice_obj.write(cr, uid, n_inv_ids.id,{'more_amount':diff_price})
                                   
                                   
                            else:
                                if now_pay_amount >= n_inv_ids.balance:
                                    print "Aaaaaaaaaaaaaaaaa",now_pay_amount,n_inv_ids.balance
                                    pay_amount = n_inv_ids.balance
                                    amount_recon = 0.0
                                    diff_price = line.pay_amount-pay_amount
                                    voucher_id = self.payment_voucher_obj(cr, uid, line,pay_amount, period_id[0],obj,obj.bank_id.id)
                                    voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,pay_amount, pay_amount,line,n_inv_ids,journal_id[0],line_ids[0],amount_recon)
                                    if diff_price > 0.0 and not inv_ids:
                                        for lined in invoice_obj.browse(cr, uid, n_inv_ids.id):
                                            invoice_ids = invoice_obj.create(cr, uid, {
                                                                        'inspector_id':line.partner_id.inspector_id.id or False,
                                                                        'name':line.partner_id.code,
                                                                        'origin': obj.name,
                                                                        'date_invoice':line.pay_date,
                                                                        'type': 'in_invoice',
                                                                        'period_id': period_id[0],
                                                                        'state':'open',
                                                                        'account_id': line.partner_id.property_account_payable.id,
                                                                        'partner_id': line.partner_id.id,
                                                                        'journal_id': obj.bank_id.id,
                                                                        'payment_term': lined.payment_term and lined.payment_term.id or False,
                                                                        'fiscal_position': lined.fiscal_position.id,
                                                                        'company_id': lined.company_id.id,
                                                                        'user_id': lined.user_id and lined.user_id.id or False
                                                })
                                            for lin in lined.invoice_line:
                                                invoice_line_obj.create(cr, uid, {
                                                                                'invoice_id':invoice_ids,
                                                                                'name': lin.name,
                                                                                'sequence': lin.sequence,
                                                                                'origin': obj.name,
                                                                                'account_id': lin.account_id.id,
                                                                                'price_unit': diff_price,
                                                                                'quantity': 1.0,
                                                                                'uos_id': lin.uos_id.id,
                                                                                'product_id': lin.product_id.id or False,
                                                                                })
                                        invoice_obj.write(cr, uid, n_inv_ids.id,{'more_amount':diff_price})
                                else:
                                    amount_recon = n_inv_ids.balance-line.pay_amount
                                    voucher_id = self.payment_voucher_obj(cr, uid, line,line.pay_amount, period_id[0],obj,obj.bank_id.id)
                                    if line_ids !=False and line_ids !=None and line_ids!=[]:
                                        voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,line.pay_amount, n_inv_ids.amount_total,line,n_inv_ids,journal.id,line_ids[0],amount_recon)    
                            context = {
                                        'payment_expected_currency': n_inv_ids.currency_id.id,
                                        'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(n_inv_ids.partner_id).id,
                                        'default_amount': n_inv_ids.type in ('out_refund', 'in_refund') and -n_inv_ids.residual or n_inv_ids.residual,
                                        'default_reference': n_inv_ids.name,
                                        'close_after_process': True,
                                        'invoice_type': n_inv_ids.type,
                                        'invoice_id': n_inv_ids.id,
                                        'default_type': n_inv_ids.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                        'type': n_inv_ids.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
                                        }
                            voucher_list.append(voucher_id)
                            voucher_obj.button_proforma_voucher(cr, uid, [voucher_id], context=context)
                if inv_ids:
                    for inv_id in invoice_obj.browse(cr, uid,inv_ids):
                        account_type = self.pool['account.account'].browse(cr, uid, inv_id.account_id.id, context=context).type
                        line_ids = move_line_pool.search(cr, uid, [('state','=','valid'), ('period_id','=',period_id[0]),('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', line.partner_id.id)], context=context)
                        journal_id = journal_obj.search(cr, uid, [('type','=', 'sale')])
                        journal = journal_obj.browse(cr, uid, journal_id[0])
                        if line.partner_id.id == inv_id.partner_id.id:
                            if pre_inv_ids:
                                paid_amount +=inv_id.balance
                                total_amount +=inv_id.amount_total
                                if diff_price > 0.0:
                                   account_invoice.append(inv_id.id)
                                   if diff_price <= inv_id.balance:
                                       pay_amount = diff_price
                                       amount_recon = inv_id.balance-pay_amount
                                       voucher_id = self.payment_voucher_obj(cr, uid, line,pay_amount, period_id[0],obj,obj.bank_id.id)
                                       voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,pay_amount,inv_id.balance, line,inv_id,journal_id[0],line_ids[0],amount_recon)  
                                   else:
                                       pay_amount = inv_id.balance
                                       amount_recon = 0.0
                                       voucher_id = self.payment_voucher_obj(cr, uid, line,pay_amount, period_id[0],obj,obj.bank_id.id)
                                       voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,pay_amount,inv_id.balance, line,inv_id,journal_id[0],line_ids[0],amount_recon)  
                                       diff_price = diff_price-inv_id.balance
                                       if diff_price>0.0 and inv_ids:
                                            for lined in invoice_obj.browse(cr, uid, inv_ids[0]):
                                                invoice_ids = invoice_obj.create(cr, uid, {
                                                                            'inspector_id':line.partner_id.inspector_id.id or False,
                                                                            'name':line.partner_id.code,
                                                                            'origin': obj.name,
                                                                            'date_invoice':line.pay_date,
                                                                            'type': 'in_invoice',
                                                                            'period_id': period_id[0],
                                                                            'state':'open',
                                                                            'account_id': line.partner_id.property_account_payable.id,
                                                                            'partner_id': line.partner_id.id,
                                                                            'journal_id': obj.bank_id.id,
                                                                            'payment_term': lined.payment_term and lined.payment_term.id or False,
                                                                            'fiscal_position': lined.fiscal_position.id,
                                                                            'company_id': lined.company_id.id,
                                                                            'user_id': lined.user_id and lined.user_id.id or False
                                                    })
                                                for lin in lined.invoice_line:
                                                    invoice_line_obj.create(cr, uid, {
                                                                                    'invoice_id':invoice_ids,
                                                                                    'name': lin.name,
                                                                                    'sequence': lin.sequence,
                                                                                    'origin': obj.name,
                                                                                    'account_id': lin.account_id.id,
                                                                                    'price_unit': diff_price,
                                                                                    'quantity': 1.0,
                                                                                    'uos_id': lin.uos_id.id,
                                                                                    'product_id': lin.product_id.id or False,
                                                                                   
                                                                                    })
                                            invoice_obj.write(cr, uid, inv_id.id,{'more_amount':diff_price})
                                
                                    
                                   
                                   context = {
                                                'payment_expected_currency': inv_id.currency_id.id,
                                                'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv_id.partner_id).id,
                                                'default_amount': inv_id.type in ('out_refund', 'in_refund') and -inv_id.residual or inv_id.residual,
                                                'default_reference': inv_id.name,
                                                'close_after_process': True,
                                                'invoice_type': inv_id.type,
                                                'invoice_id': inv_id.id,
                                                'default_type': inv_id.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                                'type': inv_id.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
                                                }
                                   voucher_list.append(voucher_id)
                                   voucher_obj.button_proforma_voucher(cr, uid, [voucher_id], context=context)
                                
                            else:
                                account_invoice.append(inv_id.id)
                                paid_amount +=inv_id.balance
                                total_amount +=inv_id.amount_total
                                if line.pre_amount > 0.0:
                                    now_pay_amount = line.pay_amount
                                else:
                                    now_pay_amount = line.pay_amount
                                paid = line.paid_amount
                                if now_pay_amount >= inv_id.balance:
                                    pay_amount = inv_id.balance
                                    diff_price = line.pay_amount-pay_amount
                                    amount_recon = 0
                                    voucher_id = self.payment_voucher_obj(cr, uid, line, pay_amount, period_id[0],obj,obj.bank_id.id)
                                    voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id, pay_amount, pay_amount,line,inv_id,journal_id[0],line_ids[0],amount_recon)
                                    if diff_price > 0.0:
                                        for lined in invoice_obj.browse(cr, uid, inv_ids[0]):
                                            invoice_ids = invoice_obj.create(cr, uid, {
                                                                        'inspector_id':line.partner_id.inspector_id.id or False,
                                                                        'name':line.partner_id.code,
                                                                        'origin': obj.name,
                                                                        'date_invoice':line.pay_date,
                                                                        'type': 'in_invoice',
                                                                        'period_id': period_id[0],
                                                                        'state':'draft',
                                                                        'account_id': line.partner_id.property_account_payable.id,
                                                                        'partner_id': line.partner_id.id,
                                                                        'journal_id': obj.bank_id.id,
                                                                        'payment_term': lined.payment_term and lined.payment_term.id or False,
                                                                        'fiscal_position': lined.fiscal_position.id,
                                                                        'company_id': lined.company_id.id,
                                                                        'user_id': lined.user_id and lined.user_id.id or False
                                                })
                                            for lin in lined.invoice_line:
                                                invoice_line_obj.create(cr, uid, {
                                                                                'invoice_id':invoice_ids,
                                                                                'name': lin.name,
                                                                                'sequence': lin.sequence,
                                                                                'origin': obj.name,
                                                                                'account_id': lin.account_id.id,
                                                                                'price_unit': diff_price,
                                                                                'quantity': 1.0,
                                                                                'uos_id': lin.uos_id.id,
                                                                                'product_id': lin.product_id.id or False,
                                                                                })
                                            invoice = invoice_obj.browse(cr, uid, invoice_ids, context=context)
                                            invoice.signal_workflow('invoice_open')
                                            invoice_obj.write(cr, uid, inv_id.id,{'more_amount':diff_price})
                                        
            
                                else:
                                    amount_recon = inv_id.balance-line.pay_amount
                                    voucher_id = self.payment_voucher_obj(cr, uid, line,line.pay_amount, period_id[0],obj,obj.bank_id.id)
                                    voucher_line = self.payment_voucher_line_obj(cr, uid,voucher_id,line.pay_amount, inv_id.amount_total,line,inv_id,journal_id[0],line_ids[0],amount_recon)    
                                   
                                context = {
                                            'payment_expected_currency': inv_id.currency_id.id,
                                            'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv_id.partner_id).id,
                                            'default_amount': inv_id.type in ('out_refund', 'in_refund') and -inv_id.residual or inv_id.residual,
                                            'default_reference': inv_id.name,
                                            'close_after_process': True,
                                            'invoice_type': inv_id.type,
                                            'invoice_id': inv_id.id,
                                            'default_type': inv_id.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                            'type': inv_id.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
                                            }
                                voucher_list.append(voucher_id)
                                voucher_obj.button_proforma_voucher(cr, uid, [voucher_id], context=context)

                bank_line_obj.write(cr, uid, line.id,{'invoice_ids':[(6,0,account_invoice)],
                                                     # 'paid_amount':paid_amount,
                                                      'voucher_ids':[(6,0, voucher_list)],
                                                    #  'more_amount':,
                                                      'diff_amount':total_amount+line.pre_amount-line.pay_amount})                 
            self.write(cr, uid, ids, {
                                     'state': 'confirm'})
        #print "assssssssssssssssssss",llllllllllllllll
        return True
    
    def action_cancel(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids)
        voucher_obj = self.pool.get('account.voucher')
        journal_obj = self.pool.get('account.journal')
        invoice_obj = self.pool.get('account.invoice')
        bank_line_obj = self.pool.get('bank.move.line')
        period_obj = self.pool.get('account.period')
        for line in obj.line_id1:
            period_id = period_obj.search(cr, uid, [('date_start','<=', line.pay_date),('date_stop','>=', line.pay_date)])
            journal_id = journal_obj.search(cr, uid,[('type','=', 'cash'),('code','=','MRP')])
            vouch_id = voucher_obj.search(cr, uid,[('journal_id','=',journal_id[0])])
            if vouch_id:
                for lis in voucher_obj.browse(cr, uid, vouch_id):
                    voucher_obj.cancel_voucher(cr, uid, lis.id)
            if line.voucher_ids:
                for li in line.voucher_ids:
                    voucher_obj.cancel_voucher(cr, uid, li.id)
            out_invoice = invoice_obj.search(cr, uid, [('partner_id','=',line.partner_id.id),('period_id','<=',period_id[0]),('type','=','out_invoice')])
            if out_invoice:
                for li in invoice_obj.browse(cr, uid, out_invoice):
                    invoice_obj.write(cr, uid, li.id,{'more_amount':0.0})
            bank_line_obj.write(cr, uid, line.id,{'invoice_ids':[(5,)],
                                                  'diff_amount':0.0})
            #invoice_id = invoice_obj.search(cr, uid, [('partner_id','=',line.partner_id.id),('period_id','=',period_id[0]),('type','=','in_invoice')])
            #if invoice_id:
            #    invoice_obj.action_cancel_draft(cr,uid, invoice_id[0])
            #    print "Aaaaaaaaaaaaaaaaaaa",invoice_id[0]
            #    invoice_obj.unlink(cr, uid,invoice_id)
        self.write(cr, uid, ids, {'state':'draft'
                                  })
        return True
    
    
    def unlink(self, cr, uid, ids, context=None):
        bank_move_data = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []
        for s in bank_move_data:
            if s['state'] in ['draft']:
                unlink_ids.append(s['id'])
                sid = self.browse(cr, uid, s['id'])
                bank_line_obj = self.pool.get('bank.move.line')
                for line in sid.line_id:
                    bank_line_obj.unlink(cr, uid,line.id)
                for line1 in sid.line_id1:
                    bank_line_obj.unlink(cr, uid,line1.id)
            else:
                raise osv.except_osv(_(u'Буруу үйлдэл'), _(u'Батлагдсан бичилт устгах боломжгүй'))
        return super(bank_move, self).unlink(cr, uid, unlink_ids, context=context)
    
class bank_move_line(osv.osv):
    _name = 'bank.move.line'
    
    _columns = {
                'pay_date':fields.date(u'Төлсөн огноо'),
                'bank_move_id':fields.many2one('bank.move','Bank move'),
                'company_id':fields.many2one('res.company','Company'),
                'partner_id':fields.many2one('res.partner',u'Харилцагч'),
                'voucher_ids':fields.many2many('account.voucher','voucher_bank_move_line_rel','voucher_ids','bank_move_line',u'Төлбөр'),
                'invoice_ids':fields.many2many('account.invoice','invoice_bank_move_line_rel','invoice_ids','bank_move_line',u'Төлсөн нэхэмжлэх'),
                'pre_amount':fields.float(u'Эхний үлдэгдэл'),
                'paid_amount':fields.float(u'Төлөх дүн'),
                'pay_amount':fields.float(u'Төлж буй дүн'),
                'diff_amount':fields.float(u'Зөрүү дүн'),
                'is_cus': fields.boolean(u'Хоосон харилцагч'),
                
            }
    
    _defaults = {
                'is_cus':False,
                    }
    def action_check(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids)
        voucher_obj = self.pool.get('account.voucher')
        voucher_line_obj = self.pool.get('account.voucher.line') 
        move_line_pool = self.pool.get('account.move.line')
        invoice_obj = self.pool.get('account.invoice')
        period_obj = self.pool.get('account.period')
        journal_obj = self.pool.get('account.journal')
        bank_line_obj = self.pool.get('bank.move.line')
        if obj.partner_id:
            for line in obj:
                self.write(cr, uid, line.id,{
                                            'is_cus':True})
                        


        else:
             raise osv.except_osv(u'Анхааруулга',u'Харилцагц талбарт утга оруулана уу')
        return  True
    
    