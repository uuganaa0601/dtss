# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time
import calendar

import openerp
from openerp import SUPERUSER_ID, api
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round
from openerp.tools.safe_eval import safe_eval as eval
import openerp.addons.decimal_precision as dp
from openerp.osv.orm import except_orm
import json
#import vatbridge

class account_invoice(osv.osv):
    _inherit = 'account.invoice'
    
    @api.one
    @api.depends('invoice_line.price_subtotal', 'tax_line.amount')
    def _compute_amount(self):
        res = {}
        price = 0.0
        qty = 0.0
        tax = 0.0
        more_amount = 0.0
        for inv in self.browse(self.ids):
            for line in inv.invoice_line:
                sub = 0.0
                qty = line.quantity
                if line.product_id.local_type == 'day' or line.product_id.local_type == 'com_day':
                    if line.invoice_id.partner_id.type == 'work' or line.invoice_id.partner_id.type == 'self':
                        if line.warm_day == 0.0:
                            price = (line.quantity*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))
                        else:
                            if line.invoice_id.partner_id.is_ail == True:
                                price = (line.quantity*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))/line.warm_day*line.warmed_day+line.doc_amount+line.loss_amount
                            else:
                                price = (line.quantity*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))/line.warm_day*line.warmed_day+line.doc_amount+line.loss_amount

                    else:
                        if line.warm_day == 0.0:
                            price = (line.quantity*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))
                        else:
                            price = (line.quantity*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))/line.warm_day*line.warmed_day+line.doc_amount+line.loss_amount+line.receipt_amount
                elif line.product_id.local_type == 'counter':
                    qty = (line.warmed_day-line.warm_day)*line.product_id.per
                    if qty != 0.0:
                        if line.invoice_id.partner_id.type == 'work' or line.invoice_id.partner_id.type == 'self':
                            price = ((line.price_unit * (1 - (line.discount or 0.0) / 100.0))*line.quantity)+line.loss_amount
                        else:
                            price = ((line.price_unit * (1 - (line.discount or 0.0) / 100.0))*line.quantity)+line.loss_amount
                        sub = (price/100.0)*line.product_id.warm_loss_per
                        price = price+sub+line.doc_amount+line.receipt_amount
                    else:
                        price = line.price_unit
                else:
                    price = line.price_unit
                if qty == 0.0:
                    for c in line.invoice_line_tax_id.compute_all(price/1.0,
                    1.0, line.product_id, inv.partner_id)['taxes']:
                        tax  += c.get('amount', 0.0)
                else:
                    for c in line.invoice_line_tax_id.compute_all(price/qty,
                    qty, line.product_id, inv.partner_id)['taxes']:
                        tax  += c.get('amount', 0.0)
        self.amount_tax = tax
        self.amount_untaxed = price
        self.amount_total = self.amount_untaxed+tax
    
    
    def _get_paid_amount(self, cr, uid, ids,name, args, context=None):
        res = {}
        
        for inv in self.browse(cr, uid, ids, context=context):
            total = 0.0
            if inv.payment_ids:
                for line in inv.payment_ids:
                    total +=line.credit
            res[inv.id] = total
        return res
    
    def _get_days(self, cr, uid, ids, name, args, context=None):
        res = {}
        days = 0.0
        for inv in self.browse(cr, uid, ids, context):
            if inv.state=='open':
                if inv.date_invoice:
                    before = datetime.strptime(inv.date_invoice,'%Y-%m-%d')
                    now = datetime.now() 
                    day = before - now
                    days = float(day.days)
                    if days < 0.0:
                        days = days*(-1)
                        if days < 40:
                            days = 0
                        if days > 40:
                            days = days-40    
            res[inv.id] = days
        return res
    
    
    @api.one
    @api.depends(
        'move_id.line_id.reconcile_id.line_id',
        'move_id.line_id.reconcile_partial_id.line_partial_ids',
    )
    def _compute_payments(self):
        partial_lines = lines = self.env['account.move.line']
        new_partial_lines = lines = self.env['account.move.line']
        for line in self.move_id.line_id:
            if line.account_id != self.account_id:
                continue
            if line.reconcile_id:
                lines |= line.reconcile_id.line_id
            elif line.reconcile_partial_id:
                lines |= line.reconcile_partial_id.line_partial_ids            
            partial_lines += line
            for li in (lines - partial_lines):
                for acc in li.move_id.line_id:
                    if acc.credit >0.0:
                        new_partial_lines +=acc
                
        self.payment_ids = (new_partial_lines).sorted()
        
        

    def _get_balance_amount(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context):
            balance = 0.0
            for li in line.payment_ids:
                balance += li.credit
            res[line.id] = line.amount_total-balance
        return res 
    
    def _get_line_amount(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context):
            balance = 0.0
            for li in line.invoice_line:
                balance += li.price_subtotal
            res[line.id] = balance
        return res 
    
    def _get_base_amount(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context):
            balance = 0.0
            for li in line.invoice_line:
                if li.doc_amount:
                    balance += li.doc_amount
                elif li.receipt_amount:
                    balance += li.receipt_amount
            res[line.id] = balance
        return res 
        
    def _get_all_amount(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context):
            balance = 0.0
            for li in line.invoice_line:
                balance +=li.price_subtotal
            res[line.id] = balance
        return res 
    
    
    
    def _get_line_loss_amount(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context):
            balance = 0.0
            for li in line.invoice_line:
                balance += li.line_loss_amount
            res[line.id] = balance
        return res 

    
    _columns = {
                'amount_untaxed': fields.function(_compute_amount,string='Subtotal', type='float',digits=dp.get_precision('Account'),
                    store=True, readonly=True, track_visibility='always'),
                'amount_tax':fields.function(_compute_amount,string='Tax', type='float',digits=dp.get_precision('Account'),
                    store=True, readonly=True, ),
                'amount_total':fields.function(_compute_amount,string='Total', type='float',digits=dp.get_precision('Account'),
                    store=True, readonly=True),
                'paid_amount':fields.function(_get_paid_amount,string=u'Төлсөн дүн',type='float',  readonly=True, store=True),
                'base_amount':fields.function(_get_base_amount,string=u"Суурь хураамж",type="float"),
                'line_loss_amount':fields.function(_get_line_loss_amount,string="Shugam aldagdal",type="float"),
                'all_amount':fields.function(_get_all_amount,string="Бүгд дүн", type="float",readonly=True, store=True),
                'tstp_id':fields.many2one('product.product',u'ЦТП'),
                'inspector_id':fields.many2one('hr.employee',u'Байцаагч'),
                'day':fields.function(_get_days, type='integer',string=u'Алдангийн хоног'),
                'line_amount':fields.function(_get_line_amount,type='float',string=u"Бүгд дүн"),
                'balance':fields.function(_get_balance_amount, type='float',string=u'Үлдэгдэл'),
                'qrdata':fields.char('QR Data'),
                'dtdd':fields.char('DTDD'),
                'lottery':fields.char('lottery'),
                'more_amount':fields.float(u'Илүү төлөлт'),
        
        }
    _defaults = {
                'more_amount':0.0,
        }


    @api.one
    def print_receipt(self):
        """ Print the invoice and mark it as sent, so that we can see more
            easily the next step of the workflow
        """
        
        vat_data = self.env['vat.data']
        vat_line = self.env['vat.data.line']
        vat_info = self.env['vat.information']
        vals = {}
        stock = []
        bank = []
        billtype = False
        tax_amount = 0.0
        noncash = 0.0
        info_id = False
        citytax = 0.0
        customerno = ''
        barcode = ''
        suff = ''
        district_code = ''
        pre_month = ''
        
        info_ids = vat_info.search([('company_id','=',self.company_id.id)])
        vat_data_id = vat_data.search([('invoice_id','=',self.id)])
        if vat_data_id.id !=[] and vat_data_id.id !=False :
             raise osv.except_osv(_('Warning!'), _(u"%s нэхэмжлэл татварлуу илгээгдсэн байна."%self.number))
        else:
            if self.partner_id.customer == True:
                if self.journal_id.type == 'sale':
                    if info_ids !=[]:
                        info_id = info_ids.id
                        if self.id:
                            if self.amount_total > 0.0:
                                for line in self.invoice_line:
                                    price = 0.0
                                    if line.discount > 0.0:
                                         discount_price = (line.price_unit/100)*line.discount
                                         price = line.price_unit-discount_price
                                    else:
                                        price = line.price_unit
                                    if line.invoice_line_tax_id:
                                        taxes = line.invoice_line_tax_id
                                        if taxes:
                                             tax_res = taxes.compute_all(line.price_unit, line.quantity,
                                                 product=line.product_id, partner=line.partner_id)
                                        for tax in tax_res['taxes']:
                                            tax_amount = tax['amount']
                                        if line.product_id.default_code != False:
                                             barcode = line.product_id.default_code
                                        else:
                                            barcode = line.product_id.categ_id.code

                                    lines = {
                                            "code":"123",
                                              "name":"%s"%line.product_id.name,
                                              "measureUnit":"%s"%line.uos_id.name,
                                             "qty":"1.00",
                                             "unitPrice":"%.2f"%line.price_subtotal,
                                              "totalAmount":"%.2f"%line.price_subtotal,
                                             "cityTax":"%.2f"%citytax,
                                             "vat":"%.2f"%tax_amount,
                                             "barCode":"%s"%barcode,
                                        }
                                    stock.append(lines)
                                if self.partner_id.billtype !=None:
                                    billtype = self.partner_id.billtype
                                else:
                                     raise osv.except_osv(_(u'Анхааруулга!'),_("Та татварын төрөл сонгоно уу"))
                                if self.partner_id.billtype == '1':
                                    customerno = ''
                                else:
                                    customerno = self.partner_id.vat

                                if self.company_id.district_code.code !=None:
                                     district_code = self.company_id.district_code.code
                                else:
                                     raise osv.except_osv(_(u'Анхааруулга!'),_("Та татварын дүүргийн код оруулана уу"))
                                names = self.number.replace("/", "0")
                                suff = names[7:14]
                                rrn = "123456789123"
                                bankid = "05"
                                banks = {
                                        "rrn":'%s'%rrn,
                                        "bankId":'%s'%bankid,
                                        "terminalId":"aa111111",
                                        "approvalCode":"none12",
                                        "amount":"%.2f"%noncash,
                                        }
                                bank.append(banks)
                                #if data_obj.is_pre_month == True:
                                #pre_month = self.date_invoice[:7]

                                vals = {
                                      "amount":"%.2f"%self.amount_total,
                                      "vat":"%.2f"%self.amount_tax,
                                      "cashAmount":"%.2f"%self.amount_total,
                                      "nonCashAmount":"%.2f"%noncash,
                                      "reportMonth":"%s"%pre_month,
                                      "cityTax":"%.2f"%citytax,
                                      "districtCode":"%s"%district_code,
                                      "billType":"%s"%str(billtype),
                                      "customerNo":"%s"%customerno,
                                      "posNo":"0004",
                                      "billIdSuffix":"%s"%suff,
                                      "returnBillId":"",
                                      "stocks":stock,
                                      "bankTransactions":bank,
                                        }

                                jsondata= json.dumps(vals, ensure_ascii=False).encode('utf8')
                                new_datas = vatbridge.putData(jsondata)
                                new_data = json.loads(new_datas)
                                if new_data['success'] == True:
                                    send_data_id = {
                                          'invoice_id':self.id,
                                           'active':False,
                                          'vat_info_id':info_id,
                                           'amount':self.amount_total,
                                          'vat':self.amount_tax,
                                          'cashAmount':self.amount_total,
                                          'nonCashAmount':noncash,
                                          'partner_id':self.partner_id.id,
                                          'cityTax':citytax,
                                           'districtCode':new_data['districtCode'],
                                           'customerNo':new_data['customerNo'],
                                          'posNo':new_data['posNo'],
                                           'billIdSuffix':new_data['billIdSuffix'],
                                          'returnBillId':'',
                                           'registerNo':new_data['registerNo'],
                                          'billId':new_data['billId'],
                                          'date':new_data['date'],
                                         # 'invoiceId':new_data['invoiceId'],
                                           #'lotteryWarningMsg':new_data['lotteryWarningMsg'],
                                          # 'warningMsg':new_data['warningMsg'],
                                          #'internalCode':,
                                          'lottery':new_data['lottery'],
                                          'qrData':new_data['qrData'],
                                          'billType':new_data['billType'],
                                            }
                                    vat_data_id = vat_data.create(send_data_id)
                                    for line in new_data['stocks']:
                                        vat_line.create({
                                                                 'vat_id':vat_data_id.id,
                                                                 'code':line['code'],
                                                                 'name':line['name'],
                                                                 'measureUnit':line['measureUnit'],
                                                                 'qty':line['qty'],
                                                                 'unitPrice':line['unitPrice'],
                                                                 'totalAmount':line['totalAmount'],
                                                                 'cityTax':line['cityTax'],
                                                                 'vat':line['vat'],
                                                                 'barCode':line['barCode'],
                                                                })
                                    self.write({'qrdata':new_data['qrData'],
                                                'lottery':new_data['lottery'],
                                                'dtdd':new_data['billId'],
                                                })
                                else:
                                     raise osv.except_osv(_(u'Анхааруулга!'),_(u"%s"%new_data['message']))
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'account_invoice_inherit.receipt_report')
    
    
    
    @api.multi
    def unlink(self):
        for invoice in self:
            self.write({'internal_number':''})
            if invoice.state not in ('draft', 'cancel'):
                raise Warning(_('You cannot delete an invoice which is not draft or cancelled. You should refund it instead.'))
            elif invoice.internal_number:
                raise Warning(_('You cannot delete an invoice after it has been validated (and received a number).  You can set it back to "Draft" state and modify its content, then re-confirm it.'))
            for li in invoice.invoice_line:
                li.unlink()
                
        return super(account_invoice, self).unlink()

class account_invoice_line(osv.osv):
    _inherit = 'account.invoice.line'
    
    def _compute_price(self):
        total = 0.0
        sub = 0.0
        if self.product_id.local_type == 'day' or self.product_id.local_type == 'com_day':
            if self.invoice_id.partner_id.type == 'work' or self.invoice_id.partner_id.type == 'self':
                if self.warm_day == 0.0:
                    total = (self.quantity*(self.price_unit * (1 - (self.discount or 0.0) / 100.0)))
                else:
                    total = (self.quantity*(self.price_unit * (1 - (self.discount or 0.0) / 100.0)))/self.warm_day*self.warmed_day+self.doc_amount+self.loss_amount

            else:
                if self.warm_day == 0.0:
                    total = (self.quantity*(self.price_unit * (1 - (self.discount or 0.0) / 100.0)))
                else:
                    if self.invoice_id.partner_id.is_ail == True:
                        total = (self.quantity*(self.price_unit * (1 - (self.discount or 0.0) / 100.0)))/self.warm_day*self.warmed_day+self.doc_amount+self.loss_amount+self.receipt_amount
                    else:
                        total = (self.quantity*(self.price_unit * (1 - (self.discount or 0.0) / 100.0)))/self.warm_day*self.warmed_day+self.loss_amount+self.receipt_amount
        elif self.product_id.local_type == 'counter':
            if self.warm_day != 0.0:
                qty = (self.warmed_day-self.warm_day)*self.product_id.per
                if self.invoice_id.partner_id.type == 'work' or self.invoice_id.partner_id.type == 'self':
                    total = ((self.price_unit * (1 - (self.discount or 0.0) / 100.0))*self.quantity)+self.loss_amount
                else:
                    total = ((self.price_unit * (1 - (self.discount or 0.0) / 100.0))*self.quantity)+self.loss_amount
                sub = (total/100.0)*self.product_id.warm_loss_per
                total = total+sub+self.receipt_amount
            else:
                total = self.price_unit
        else:
            total = self.price_unit
        if total >=0.0 and self.quantity > 0.0:
            taxes = self.invoice_line_tax_id.compute_all(total/self.quantity, self.quantity, product=self.product_id, partner=self.invoice_id.partner_id)
            self.price_subtotal = taxes['total']
        else:
            self.price_subtotal = 0.0
        if self.invoice_id:
            self.price_subtotal = self.invoice_id.currency_id.round(self.price_subtotal)
    
    
    def _get_loss_amount(self,cr, uid, ids, field_name, arg, context=None):
        res = {}
        DATE_FORMAT = "%Y-%m-%d"
        total = 0.0
        for line in self.browse(cr, uid, ids):
            if line.product_id.local_type == 'counter' or line.product_id.local_type == 'day' or line.product_id.local_type == 'com_day' or line.product_id.local_type == 'elect':
                invoice_id = self.pool.get('account.invoice').search(cr, uid,[('partner_id','=',line.invoice_id.partner_id.id),('type','=','out_invoice'),('date_invoice','<',line.invoice_id.date_invoice),('state','not in',('draft','done','cancel'))],order='date_invoice asc',limit=1)
                if invoice_id !=[]:
                    new_day = 0.0
                    invoice = self.pool.get('account.invoice').browse(cr, uid, invoice_id)
                    before = datetime.strptime(invoice.date_invoice,'%Y-%m-%d')
                    now = datetime.strptime(line.invoice_id.date_invoice, '%Y-%m-%d')
                    day = before - now
                    if float(day.days)< 0.0:
                        new_day = float(day.days)*(-1)
                    if new_day >= 40.0:
                        loss_day = new_day-40.0
                        total = (line.more_and_less_amount*line.loss_day)/100.0*line.product_id.loss_percent
                else:
                     total = 0.0
                res[line.id] = total
        return res


    #@api.v8
    #def _compute_subtotal(self):
    #    self.subtotal_amount = self.price_subtotal-self.doc_amount-self.receipt_amount
    
    _columns = {
                'loss_amount':fields.function(_get_loss_amount,type='float',string=u'Алданги',store=True, readonly=True),
                'warm_day':fields.float(u'Халаах хоног/Эхний заалт'),
                'warmed_day':fields.float(u'Халаасан хоног/Эцсийн заалт'),
                'diff_amount':fields.float(u'Зөрүү'),
                'loss_day':fields.integer(u'Алдангийн хоног'),
                'more_and_less_amount':fields.float(u'Илүү дутуу дүн'),
                'line_loss_amount':fields.float(u'Шугамын алдагдал'),
                'doc_amount':fields.float(u'Суурь хураамж'),
                'receipt_amount':fields.float(u'Баримт'),
                'price_subtotal':fields.function(_compute_price,string='Amount', type="float", digits= dp.get_precision('Account'),
        store=True, readonly=True),
                'subtotal_amount':fields.float('Subtotal Amount',readonly=True),
                
        }
    
    
    
    @api.multi
    def product_id_change(self, product, uom_id, qty=0, name='', type='out_invoice',
            partner_id=False, fposition_id=False, price_unit=False, currency_id=False,
            company_id=None):
    
        context = self._context
        company_id = company_id if company_id is not None else context.get('company_id', False)
        self = self.with_context(company_id=company_id, force_company=company_id)

        if not partner_id:
            raise except_orm(_('No Partner Defined!'), _("You must first select a partner!"))
        if not product:
            if type in ('in_invoice', 'in_refund'):
                return {'value': {}, 'domain': {'uos_id': []}}
            else:
                return {'value': {'price_unit': 0.0}, 'domain': {'uos_id': []}}

        values = {}
        product_obj = self.env['product.product']
        product_id = product_obj.browse(product)
        

        part = self.env['res.partner'].browse(partner_id)
        fpos = self.env['account.fiscal.position'].browse(fposition_id)

        if part.lang:
            self = self.with_context(lang=part.lang)
        product = self.env['product.product'].browse(product)

        values['name'] = product.partner_ref
        if type in ('out_invoice', 'out_refund'):
            account = product.property_account_income or product.categ_id.property_account_income_categ
        else:
            account = product.property_account_expense or product.categ_id.property_account_expense_categ
        account = fpos.map_account(account)
        if account:
            values['account_id'] = account.id

        if type in ('out_invoice', 'out_refund'):
            taxes = product.taxes_id or account.tax_ids
            if product.description_sale:
                values['name'] += '\n' + product.description_sale
        else:
            taxes = product.supplier_taxes_id or account.tax_ids
            if product.description_purchase:
                values['name'] += '\n' + product.description_purchase

        fp_taxes = fpos.map_tax(taxes)
        values['invoice_line_tax_id'] = fp_taxes.ids

        if type in ('in_invoice', 'in_refund'):
            if price_unit and price_unit != product.standard_price:
                values['price_unit'] = price_unit
            else:
                values['price_unit'] = self.env['account.tax']._fix_tax_included_price(product.standard_price, taxes, fp_taxes.ids)
        else:
            values['price_unit'] = self.env['account.tax']._fix_tax_included_price(product.lst_price, taxes, fp_taxes.ids)

        values['uos_id'] = product.uom_id.id
        if uom_id:
            uom = self.env['product.uom'].browse(uom_id)
            if product.uom_id.category_id.id == uom.category_id.id:
                values['uos_id'] = uom_id

        domain = {'uos_id': [('category_id', '=', product.uom_id.category_id.id)]}

        company = self.env['res.company'].browse(company_id)
        currency = self.env['res.currency'].browse(currency_id)

        if company and currency:
            if company.currency_id != currency:
                values['price_unit'] = values['price_unit'] * currency.rate

            if values['uos_id'] and values['uos_id'] != product.uom_id.id:
                values['price_unit'] = self.env['product.uom']._compute_price(
                    product.uom_id.id, values['price_unit'], values['uos_id'])
        if part.is_day == True or part.is_size == True:
            if product_id.local_type == 'day' or product_id.local_type == 'com_day':
                values['quantity'] = part.square_amount
        else:
            if product_id.local_type == 'counter':
                values['koff_amount'] = product_id.per
        if part.type == 'nat_company' or part.type =='company':
            if part.is_ail == True:
                 if product_id.local_type == 'day' or product_id.local_type == 'counter' or product_id.local_type == 'com_day':
                    if 0 < part.square_amount <= 40.0:
                        values['doc_amount'] = 3000.0
                    elif 40.0 < part.square_amount < 80.0:
                        values['doc_amount'] = 5000.0
                    else:
                        values['doc_amount'] = 10000.0
            if product_id.local_type == 'day' or product_id.local_type == 'counter' or product_id.local_type == 'com_day':
                values['receipt_amount'] = 90.91
            else:
                values['receipt_amount'] = 0.0
        else:
            if part.is_day == True or part.is_size == True:
                if product_id.local_type == 'day':
                    if 0 < part.square_amount <= 40.0:
                        values['doc_amount'] = 3000
                    elif 40.0 < part.square_amount < 80.0:
                        values['doc_amount'] = 5000
                    else:
                        values['doc_amount'] = 10000
        
        if product_id.local_type =='day' or product_id.local_type == 'com_day':
            now = date_order
            days = calendar.monthrange(int(now[:4]), int(now[5:7]))[1]
            values['warm_day'] = days

        return {'value': values, 'domain': domain}
    #===========================================================================
    # 
    # @api.multi
    # def unlink(self):
    #     for line in self:
    #         return super(account_invoice_line, self).unlink(line.id)
    #===========================================================================

    
    @api.model
    def move_line_get_item(self, line):
        if line.invoice_id.partner_id.type =='company' or line.invoice_id.partner_id.type =='nat_company':
            return {
                'type': 'src',
                'name': line.name.split('\n')[0][:64],
                'price_unit': line.price_unit,
                'quantity': line.quantity,
                'price': line.price_subtotal+line.doc_amount,
                'account_id': line.account_id.id,
                'product_id': line.product_id.id,
                'uos_id': line.uos_id.id,
                'account_analytic_id': line.account_analytic_id.id,
                'taxes': line.invoice_line_tax_id,
            }
        else:
            return {
                    'type': 'src',
                    'name': line.name.split('\n')[0][:64],
                    'price_unit': line.price_unit,
                    'quantity': line.quantity,
                    'price': line.price_subtotal,
                    'account_id': line.account_id.id,
                    'product_id': line.product_id.id,
                    'uos_id': line.uos_id.id,
                    'account_analytic_id': line.account_analytic_id.id,
                    'taxes': line.invoice_line_tax_id,
                }
    
class account_invoice_tax(osv.osv):
    _inherit = 'account.invoice.tax'

    @api.v8
    def compute(self, invoice):
        tax_grouped = {}
        currency = invoice.currency_id.with_context(date=invoice.date_invoice)
        company_currency = invoice.company_id.currency_id
        price = 0.0
        for line in invoice.invoice_line:
            if line.product_id.local_type == 'day' or line.product_id.local_type == 'com_day':
                if line.invoice_id.partner_id.type == 'work' or line.invoice_id.partner_id.type == 'self':
                    if line.warm_day== 0.0:
                        price = (line.quantity*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))
                    else:
                        price = (line.quantity*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))/line.warm_day*line.warmed_day+line.doc_amount+line.loss_amount
                else:
                    if line.warm_day ==0.0:
                        price = (line.quantity*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))
                    else:
                        price = (line.quantity*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))/line.warm_day*line.warmed_day+line.doc_amount+line.loss_amount+line.receipt_amount
            elif line.product_id.local_type == 'counter':
                qty = (line.warmed_day-line.warm_day)*line.product_id.per
                if line.invoice_id.partner_id.type == 'work' or line.invoice_id.partner_id.type == 'self':
                    price = ((line.price_unit * (1 - (line.discount or 0.0) / 100.0))*line.quantity)+line.loss_amount
                else:
                    price = ((line.price_unit * (1 - (line.discount or 0.0) / 100.0))*line.quantity)+line.loss_amount+line.receipt_amount
                sub = (price/100)*line.product_id.warm_loss_per
                price = price+sub+line.doc_amount
            else:
                price = line.price_unit
            
            taxes = line.invoice_line_tax_id.compute_all(
                (price/line.quantity * (1 - (line.discount or 0.0) / 100.0)),
                line.quantity, line.product_id, invoice.partner_id)['taxes']
            for tax in taxes:
                val = {
                    'invoice_id': invoice.id,
                    'name': tax['name'],
                    'amount': tax['amount'],
                    'manual': False,
                    'sequence': tax['sequence'],
                    'base': currency.round(tax['price_unit'] * line['quantity']),
                }
                if invoice.type in ('out_invoice','in_invoice'):
                    val['base_code_id'] = tax['base_code_id']
                    val['tax_code_id'] = tax['tax_code_id']
                    val['base_amount'] = currency.compute(val['base'] * tax['base_sign'], company_currency, round=False)
                    val['tax_amount'] = currency.compute(val['amount'] * tax['tax_sign'], company_currency, round=False)
                    val['account_id'] = tax['account_collected_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_collected_id']
                else:
                    val['base_code_id'] = tax['ref_base_code_id']
                    val['tax_code_id'] = tax['ref_tax_code_id']
                    val['base_amount'] = currency.compute(val['base'] * tax['ref_base_sign'], company_currency, round=False)
                    val['tax_amount'] = currency.compute(val['amount'] * tax['ref_tax_sign'], company_currency, round=False)
                    val['account_id'] = tax['account_paid_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_paid_id']

                # If the taxes generate moves on the same financial account as the invoice line
                # and no default analytic account is defined at the tax level, propagate the
                # analytic account from the invoice line to the tax line. This is necessary
                # in situations were (part of) the taxes cannot be reclaimed,
                # to ensure the tax move is allocated to the proper analytic account.
                if not val.get('account_analytic_id') and line.account_analytic_id and val['account_id'] == line.account_id.id:
                    val['account_analytic_id'] = line.account_analytic_id.id

                key = (val['tax_code_id'], val['base_code_id'], val['account_id'])
                if not key in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['base'] += val['base']
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base_amount'] += val['base_amount']
                    tax_grouped[key]['tax_amount'] += val['tax_amount']

        for t in tax_grouped.values():
            t['base'] = currency.round(t['base'])
            t['amount'] = currency.round(t['amount'])
            t['base_amount'] = currency.round(t['base_amount'])
            t['tax_amount'] = currency.round(t['tax_amount'])

        return tax_grouped
    

    
class account_journal(osv.osv):
    _inherit = 'account.journal'
    
    _columns = {    
                'bank_id':fields.char('Bank id'),
                'bank_pos_terminal_id':fields.char('Bank pos terminal id'),
                'approve_code':fields.char('Approve code'),
                'bank': fields.selection([('haan','Хаан банк'),
                                           ('hhb','Худалдаа хөгжлийн банк'),
                                           ('tb','Төрийн банк'),
                                           ('golomt','Голомт банк'),
                                           ('capital','Капитал банк'),
                                           ('capitron','Капитрон банк'),
                                           ('khas','Хас банк'),
                                           ('tsalin','Цалин тооцоо')], 'Банк'),
            }