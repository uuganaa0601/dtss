# -*- coding: utf-8 -*-
##############################################################################
#
#    
#
#    
#
##############################################################################

{
    "name": "Account invoice inherit module",
    "version": "1.0",
    "author": "Erdenebold",
    "category": "Account",
    "description": """This module sale inherit.""",
    "depends" : [
                 "base",
                 "account",
                 "account_voucher",
                 'report'
                 ],
    'update_xml': [ 
                    "data/district_data_import.xml",
                    "absract_report_view.xml",
                    "account_invoice_view.xml",
                    "invoice_import_view.xml",
                    "bill_employee_sequence.xml",
                    "views/invoice_receipt_report.xml",
                    "views/receipt_report.xml",
                    "views/calculation_form.xml",
                    "vat_information_view.xml",
                   # "between_payment_view.xml",
                    "res_partner_view.xml",
                    "bank_move_line_view.xml",
                    "report/invoice_new_report_view.xml",
                    "report/invoice_new_payable_report_view.xml",
                    "report/invoice_payment_report_view.xml",
                    "wizard/account_report_aged_partner_balance_view.xml",
                    "wizard/account_bank_report_view.xml",
                    "views/report_agedpartnernewbalance.xml",
                    "report.xml",
                   ],

    'installable': True,
    'auto_install': False, 
    "icon": '/account_invoice_inherit/static/src/img/mn_icon.png',
}