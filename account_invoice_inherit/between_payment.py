# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import base64
import os
import math
from openerp import tools
from openerp.osv import fields,osv
from openerp.tools.translate import _
from openerp.tools.misc import get_iso_codes

from tempfile import NamedTemporaryFile
from datetime import datetime, timedelta, date
import datetime
import xlwt, xlrd, time
from openerp.addons.account_voucher.invoice import invoice

class between_payment(osv.osv):
    _name = 'between.payment'
    
    _columns = {
                'name':fields.char(u'Дугаар',readonly=True),
                'company_id':fields.many2one('res.company',u'Компани'),
                'period_id':fields.many2one('account.period',u'Мөчлөг'),
                'journal_id':fields.many2one('account.journal',u'Журнал'),
                'partner_type':fields.selection([('self',u'Айл өрх'),
                                                 ('nat_company',u'Төсөвт байгууллага'),
                                                 ('company',u'ААН байгууллага')],u'Харилцагчийн төрөл'),
                'line_ids':fields.one2many('between.payment.line','parent_id',u'Line'),
                'reg_date':fields.date(u'Бүртгэсэн огноо'),
                'reg_user_id':fields.many2one('res.users',u'Бүртгэсэн хэрэглэгч'),
                'state':fields.selection([('draft',u'Ноорог'),
                                          ('confirm',u'Батлагдсан')],u'Төлөв'),
        }
    _defaults = {
                'state':'draft',
                'company_id':lambda self, cr, uid, c: self.pool.get('res.company')._company_default_get(cr, uid, 'between.payment', context=c),
                'reg_date':lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
                'reg_user_id':lambda self, cr, uid, c: uid,
                
        }
    def action_import(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids)
        invoice_obj = self.pool.get('account.invoice')
        cr.execute("select rp.id from res_partner as rp left join account_invoice as ai on ai.partner_id=rp.id where rp.type = %s and ai.type in ('in_invoice','out_invoice') group by rp.id"%obj.partner_type)
        partner = cr.fetchall()
        if partner:
            for part in partner:
                print "part",part
                invoice_id = invoice_obj.search(cr, uid, [('period_id','<=',obj.period_id.id),('type','in',('in_invoice','out_invoice'),('partner_id','='))])
                
        
        return True
    
    def action_confirm(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids)
        voucher_obj = self.pool.get('account.voucher')
        voucher_line_obj = self.pool.get('account.voucher.line')
        for line in obj.line_ids:
            print line
        return True
    
class between_payment_line(osv.osv):
    _name = 'between.payment.line'
    
    def get_diff_amount(self, cr, uid, ids, context=None):
        res = {}
        return res
    
    _columns = {
                'parent_id':fields.many2one('between.payment',u'Эцэг'),
                'company_id':fields.many2one('res.company',u'Компани'),
                'partner_id':fields.many2one('res.partner',u'Харилцагч'),
                'in_invoice_ids':fields.many2many("account.invoice",'between_line_in_invoice_rel','in_invoice_ids','invoice_id',u'Илүү төлбөр'),
                'out_invoice_ids':fields.many2many("account.invoice",'between_line_out_invoice_rel','out_invoice_ids','invoice_id',u'Гарсан төлбөр'),
                'more_amount':fields.float(u'Илүү төлөлт'),
                'now_amount':fields.float(u'Гарсан төлбөр'),
                'diff_amount':fields.function(get_diff_amount,type='float',string=u'Зөрүү'),
        }
    _defaults = {
                'company_id':lambda self, cr, uid, c: self.pool.get('res.company')._company_default_get(cr, uid, 'between.payment', context=c)
        }