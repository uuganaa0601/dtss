# -*- coding: utf-8 -*-
##############################################################################
#
#    Development by Erdenebold
#    Copyright (C) 2014 Erdenebold. All Rights Reserved
#
#    Email : erdenebold10@gmail.com
#    Phone : 976 + 96693490
#
##############################################################################
from openerp.osv import fields, osv
from openerp.osv.orm import except_orm
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp.osv.expression import get_unaccent_wrapper
from datetime import date
import time, datetime
import datetime
from openerp import tools, api
from operator import itemgetter

class res_partner(osv.osv):
    _inherit = 'res.partner'
    
    def _credit_debit_get(self, cr, uid, ids, field_names, arg, context=None):
        ctx = context.copy()
        ctx['all_fiscalyear'] = True
        query = self.pool.get('account.move.line')._query_get(cr, uid, context=ctx)
        cr.execute("""SELECT l.partner_id, a.type, SUM(l.debit-l.credit)
                      FROM account_move_line l
                      LEFT JOIN account_account a ON (l.account_id=a.id)
                      WHERE a.type IN ('receivable','payable')
                      AND l.partner_id IN %s
                      AND """ + query + """
                      GROUP BY l.partner_id, a.type
                      """,
                   (tuple(ids),))
        maps = {'receivable':'credit', 'payable':'debit' }
        res = {}
        for id in ids:
            res[id] = {}.fromkeys(field_names, 0)
        for pid,type,val in cr.fetchall():
            if val is None: val=0
            res[pid][maps[type]] = (type=='receivable') and val or -val
        return res
    
    def _asset_difference_search(self, cr, uid, obj, name, type, args, context=None):
        if not args:
            return []
        having_values = tuple(map(itemgetter(2), args))
        where = ' AND '.join(
            map(lambda x: '(SUM(bal2) %(operator)s %%s)' % {
                                'operator':x[1]},args))
        query = self.pool.get('account.move.line')._query_get(cr, uid, context=context)
        cr.execute(('SELECT pid AS partner_id, SUM(bal2) FROM ' \
                    '(SELECT CASE WHEN bal IS NOT NULL THEN bal ' \
                    'ELSE 0.0 END AS bal2, p.id as pid FROM ' \
                    '(SELECT (debit-credit) AS bal, partner_id ' \
                    'FROM account_move_line l ' \
                    'WHERE account_id IN ' \
                            '(SELECT id FROM account_account '\
                            'WHERE type=%s AND active) ' \
                    'AND reconcile_id IS NULL ' \
                    'AND '+query+') AS l ' \
                    'RIGHT JOIN res_partner p ' \
                    'ON p.id = partner_id ) AS pl ' \
                    'GROUP BY pid HAVING ' + where), 
                    (type,) + having_values)
        res = cr.fetchall()
        if not res:
            return [('id','=','0')]
        return [('id','in',map(itemgetter(0), res))]

    def _credit_search(self, cr, uid, obj, name, args, context=None):
        return self._asset_difference_search(cr, uid, obj, name, 'receivable', args, context=context)

    def _debit_search(self, cr, uid, obj, name, args, context=None):
        return self._asset_difference_search(cr, uid, obj, name, 'payable', args, context=context)
    
    def _display_name_compute(self, cr, uid, ids, name, args, context=None):
        context = dict(context or {})
        context.pop('show_address', None)
        context.pop('show_address_only', None)
        context.pop('show_email', None)
        context.pop('code', None)
        return dict(self.name_get(cr, uid, ids, context=context))
    
    def _get_code(self, cr, uid, ids, field_names, arg, context=None):
        res = {}
        for line in self.browse(cr, uid,ids):
            
            if line.code == False:
                pre_partner_id = self.search(cr, uid, [('code','!=',False),('type','=',line.type)],order='id desc',limit=1)
                if pre_partner_id:
                    if line.id != pre_partner_id[0]:
                        pre_partner = self.browse(cr, uid, pre_partner_id)
                        code = pre_partner.code
                        if code !=False:
                            new_code ='%s'%(int(code)+1)
                        else:
                            new_code = False
                        if line.type =='nat_company':
                            if new_code == False:
                                res[line.id] = 100001
                            else:
                                res[line.id] = new_code
                        elif line.type == 'work' or line.type =='self':
                            if new_code == False:
                                res[line.id] = 300001
                            else:
                                res[line.id] = new_code
                        elif line.type == 'company':
                            if new_code == False:
                                res[line.id] = 200001
                            else:
                                res[line.id] = new_code
                else:
                    if line.type =='nat_company':
                       res[line.id] = 100001
                    elif line.type == 'work' or line.type =='self':
                       res[line.id] = 300001
                    elif line.type == 'company':
                       res[line.id] = 200001
        return res
    def _display_address(self, cr, uid, address, without_company=False, context=None):

        '''
        The purpose of this function is to build and return an address formatted accordingly to the
        standards of the country where it belongs.

        :param address: browse record of the res.partner to format
        :returns: the address formatted in a display that fit its country habits (or the default ones
            if not country is specified)
        :rtype: string
        '''

        # get the information that will be injected into the display format
        # get the address format
        address_format = address.country_id.address_format or \
              "%(street)s\n%(street2)s\n%(city)s %(state_code)s %(zip)s\n%(country_name)s\n%(code)s"
        args = {
            'state_code': address.state_id.code or '',
            'state_name': address.state_id.name or '',
            'country_code': address.country_id.code or '',
            'country_name': address.country_id.name or '',
            'company_name': address.parent_name or '',
            'code': address.code or '',
        }
        for field in self._address_fields(cr, uid, context=context):
            args[field] = getattr(address, field) or ''
        if without_company:
            args['company_name'] = ''
        elif address.parent_id:
            address_format = '%(company_name)s\n' + address_format
        return address_format % args
    
    #--------------------------- def name_get(self, cr, uid, ids, context=None):
            #----------------------------------------------- if context is None:
                #-------------------------------------------------- context = {}
            #---------------------------------- if isinstance(ids, (int, long)):
                #--------------------------------------------------- ids = [ids]
            #---------------------------------------------------------- res = []
            #--------- for record in self.browse(cr, uid, ids, context=context):
                #-------------------------------------------- name = record.name
                #---------------- if record.parent_id and not record.is_company:
                    #-------------- name = "%s, %s" % (record.parent_name, name)
                #-------------------------- if context.get('show_address_only'):
                    # name = self._display_address(cr, uid, record, without_company=True, context=context)
                #------------------------------- if context.get('show_address'):
                    # name = name + "\n" + self._display_address(cr, uid, record, without_company=True, context=context)
                #--------------------------------------- if context.get('code'):
                    # name = name + "\n" + self._display_address(cr, uid, record, without_company=True, context=context)
                #------------------------------ name = name.replace('\n\n','\n')
                #------------------------------ name = name.replace('\n\n','\n')
                #---------------- if context.get('show_email') and record.email:
                    #------------------- name = "%s <%s>" % (name, record.email)
                #--------------------------------- res.append((record.id, name))
            #-------------------------------------------------------- return res
        
    def name_get(self, cr, uid, ids, context=None):
        if isinstance(ids, (list, tuple)) and not len(ids):
            return []
        if isinstance(ids, (long, int)):
            ids = [ids]
        reads = self.read(cr, uid, ids, ['name','street','code'], context=context)
        res = []
        for record in reads:
            name = record['name']
            if record['street'] and record['code']:
                name = name +' / '+ record['street']+' / '+record['code']
            res.append((record['id'], name))
        return res      
    
    def name_search(self, cr, uid, name, args=None, operator='ilike', context=None, limit=100):
        if not args:
            args = []
        if name and operator in ('=', 'ilike', '=ilike', 'like', '=like'):

            self.check_access_rights(cr, uid, 'read')
            where_query = self._where_calc(cr, uid, args, context=context)
            self._apply_ir_rules(cr, uid, where_query, 'read', context=context)
            from_clause, where_clause, where_clause_params = where_query.get_sql()
            where_str = where_clause and (" WHERE %s AND " % where_clause) or ' WHERE '

            # search on the name of the contacts and of its company
            search_name = name
            if operator in ('ilike', 'like'):
                search_name = '%%%s%%' % name
            if operator in ('=ilike', '=like'):
                operator = operator[1:]

            unaccent = get_unaccent_wrapper(cr)

            query = """SELECT id
                         FROM res_partner
                      {where} ({code} {operator} {percent}
                           OR {display_name} {operator} {percent})
                     ORDER BY {display_name}
                    """.format(where=where_str, operator=operator,
                               code=unaccent('code'),
                               display_name=unaccent('display_name'),
                               percent=unaccent('%s'))

            where_clause_params += [search_name, search_name]
            if limit:
                query += ' limit %s'
                where_clause_params.append(limit)
            cr.execute(query, where_clause_params)
            ids = map(lambda x: x[0], cr.fetchall())

            if ids:
                return self.name_get(cr, uid, ids, context)
            else:
                return []
        return super(res_partner,self).name_search(cr, uid, name, args, operator=operator, context=context, limit=limit)
    




    
    _columns = {
                'contract_number':fields.char(u'Гэрээний дугаар'),
                'invoice_ids':fields.one2many('account.invoice','partner_id','Invoices'),
                'code':fields.function(_get_code,type='char',store=True,string=u'Код'),
                'inspector_id':fields.many2one('hr.employee',u'Байцаагч'),
                'type':fields.selection([('work',u'Ажилтан'),
                                         ('self',u'Хувь хүн'),
                                         ('company',u'Аж ахуй нэгж'),
                                         ('nat_company',u'Төсөвт байгууллага')],u'Төрөл',reuired=True),
                'is_counter':fields.boolean(u'Тоолууртай эсэх /Гкал/'),
                'is_day':fields.boolean(u'Талбай эсэх /м2/'),
                'is_ail':fields.boolean(u'Айл өрхөөр тооцоолох'),
                'is_size':fields.boolean(u'Эзэлхүүн эсэх /m3/'),
                'is_dhg':fields.boolean(u'ДХГ /Гкал/'),
                'billtype':fields.selection([('1','1'),
                                             ('3','3')],'Bill type',required=True),
                'square_amount':fields.float(u'Талбай',required=True),
                'vat_number':fields.integer(u'Татварын код'),
                'tstp_id':fields.many2one('product.product',u'ЦТП',domain=[('is_ztb', '=', True)],require=True),
                
                'credit': fields.function(_credit_debit_get,
                                            fnct_search=_credit_search, string='Total Receivable', multi='dc', help="Total amount this customer owes you."),
                'debit': fields.function(_credit_debit_get, fnct_search=_debit_search, string='Total Payable', multi='dc', help="Total amount you have to pay to this supplier."),
                'payment_history_ids':fields.one2many('account.move.line','partner_id','Payment history',domain=[('credit','!=',0.0),('journal_id.type','!=', 'sale')]),
                'change_history_ids':fields.one2many('partner.change.history','partner_id','Partner'),
                }
    
    _defaults = {
                'type':'self',
        }
    @api.multi
    def write(self, vals):
        # res.partner must only allow to set the company_id of a partner if it
        # is the same as the company of all users that inherit from this partner
        # (this is to allow the code from res_users to write to the partner!) or
        # if setting the company_id to False (this is compatible with any user
        # company)
        if vals.get('website'):
            vals['website'] = self._clean_website(vals['website'])
        if vals.get('name'):
            users = self.env['res.users'].browse(self._uid)
            self.env['partner.change.history'].create({'name':vals.get('name'),
                                                       'last_name':self.name,
                                                       'date':datetime.datetime.now(),
                                                       'reg_user_id':self._uid,
                                                       'desc':u'%s өдөр харилцагчийн нэр %s өөрчлөв.'%(datetime.datetime.now().date(),users.name),
                                                       'partner_id':self.ids[0]})
        if vals.get('company_id'):
            company = self.env['res.company'].browse(vals['company_id'])
            for partner in self:
                if partner.user_ids:
                    companies = set(user.company_id for user in partner.user_ids)
                    if len(companies) > 1 or company not in companies:
                        raise osv.except_osv(_("Warning"),_("You can not change the company as the partner/user has multiple user linked with different companies."))

        result = super(res_partner, self).write(vals)
        for partner in self:
            self._fields_sync(partner, vals)
        return result
    
    
    
class partner_change_history(osv.osv):
    _name = 'partner.change.history'
    
    _columns = {
                'company_id':fields.many2one('res.company',u'Компани'),
                'name':fields.char(u'Шинэ нэр'),
                'last_name':fields.char(u'Хуучин нэр'),
                'date':fields.date(u'Огноо'),
                'reg_user_id':fields.many2one('res.users',u'Өөрчилсөн хэрэглэгч'),
                'desc':fields.char(u'Тайлбар'),
                'partner_id':fields.many2one('res.partner','Partner'),
                
        }
    
class res_company(osv.osv):
    _inherit = 'res.company'


    _columns = {
                'district_code':fields.many2one('res.country.state','District code',required=True),
                
        }