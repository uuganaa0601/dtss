# -*- coding: utf-8 -*-
##############################################################################
#
#    ShineERP, Enterprise Management Solution    
#    Copyright (C) 2007-2014 ShineERP Co.,ltd (<http://www.serp.mn>). All Rights Reserved
#
#    Address : Suhkbaatar District, National ITPark, ShineERP LLC
#    Email : info@serp.mn
#    Phone : 976 + 11-318043
#
##############################################################################

from lxml import etree
from openerp.osv import fields, osv
from openerp.tools.translate import _
from operator import itemgetter
import xlrd, base64, time, datetime, xlwt, openerp.netsvc, datetime
from lxml import etree
from operator import itemgetter

from datetime import date
from datetime import timedelta
from datetime import datetime

from pytz import timezone
from dateutil import parser

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class hr_hour_record(osv.osv):
    """
        АЖИЛЧДЫН ЦАГИЙН БҮРТГЭЛ
    """
    _name = "hr.hour.record"
    _description = "Employee hour record"
    
    _columns = {
        'department': fields.many2one('hr.department','Department'),
        'name':fields.char('Working time register'),
        'company_id':   fields.many2one('res.company', 'Company', required=True, ),
        'start_date':   fields.date('Start date',required=True, ),
        'end_date':     fields.date('End date',required=True, ),
        'holiday_day':fields.float('Holiday day'),
        'line_id':fields.one2many('hr.hour.record.line', 'parentd_id', 'Hour record'),
        'state':        fields.selection([
                            ('draft', 'Draft'),
                            ('confirmed', 'Confirmed'),
                        ], 'States', track_visibility='onchange',readonly=True, help="Help...", select=True, ),
                }
    _defaults = {
        'company_id': lambda obj,cr,uid,c=None:obj.pool.get('res.company')._company_default_get(cr,uid,'hr.hour.record',context=c),
        'start_date':time.strftime('%Y-%m-01'),
        'state': 'draft',
        'name':lambda obj, cr, uid, context: '/',
    }

    def hour_record(self, cr, uid, ids, context=None):
        hr_obj = self.pool.get('hr.hour.record')
        hr_data = self.browse(cr, uid, ids)[0]
        emp_info = self.pool.get('hr.hour.record.line')
        emp_obj = self.pool.get('hr.employee')
        att_obj = self.pool.get('hr.attendance')
        contract_obj = self.pool.get('hr.contract')
        holiday_obj = self.pool.get('hr.holidays')
        
        self.delete_lines(cr, uid, ids, context)
        emp_id = []
        if hr_data.department.id !=[]:
            emp_id = emp_obj.search(cr, uid, [('company_id','=',hr_data.company_id.id),('department_id','=',hr_data.department.id)], order='department_id')
        else:
            emp_id = emp_obj.search(cr, uid, [('company_id','=',hr_data.company_id.id)], order='department_id')
        start_time = datetime.strptime(hr_data.start_date,DATE_FORMAT)
        end_time = datetime.strptime(hr_data.end_date,DATE_FORMAT)
        days = []
        idate = datetime.strptime(hr_data.start_date,DATE_FORMAT)
        while idate <= datetime.strptime(hr_data.end_date,DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        for line in emp_obj.browse(cr, uid, emp_id):
            due_time=0
            due_day=0
            total_work_time = 0.0
            in_time = 0.0
            out_time = 0.0
            n_time = 0.00
            out_n_time = 0.0
            fr_n_time = 0.0
            n_first_hour = 0.0
            n_first_to = 0.0
            n_second_hour = 0.0
            n_second_to = 0.0
            n_lost_minute = 0.0
            first_hour = 0.0
            first_to = 0.0
            second_hour = 0.0
            second_to = 0.0
            lost_minute = 0.0
            work_day = 0.0
            over_time = 0.0
            first_to2 = 0.0
            first_hour2 = 0.0
            second_hour2 = 0.0
            second_to2 = 0.0
            
            
            att_id = att_obj.search(cr, uid, [('employee_id','=',line.id),('name','>=',hr_data.start_date),('name','<=',hr_data.end_date)])
            if att_id:
                for att_line1 in att_obj.browse(cr, uid, att_id):
                    if att_line1.action == 'sign_out':
                        contract_id  = contract_obj.search(cr, uid, [('employee_id','=',line.id)])
                        if contract_id:
                            index3 = 1
                            contract =  contract_obj.browse(cr, uid, contract_id)
                            if contract.working_hours.is_night_shift == False:
                                for li in contract.working_hours.attendance_ids:
                                    if int(datetime.strptime(att_line1.name, "%Y-%m-%d %H:%M:%S").weekday()+1) == int(li.name):
                                        if index3 == 1:
                                            first_hour = float(li.hour_from)
                                            first_to = float(li.hour_to)
                                        else:
                                            second_hour = float(li.hour_from)
                                            second_to = float(li.hour_to)
                                        index3 +=1
                                
                                
                                
                                atten_time0 = datetime.strptime(str(att_line1.name), '%Y-%m-%d %H:%M:%S')
                               
                                
                                d = datetime.strptime(str(att_line1.name), '%Y-%m-%d %H:%M:%S')
                                dat = d.replace(minute=0, hour=06, second=0)
                                night_time = d.replace(minute=0, hour=0,second=0)
                                
                                datas = datetime.strptime(str(dat), '%Y-%m-%d %H:%M:%S')
                                atten_time01 = atten_time0 + timedelta(hours=8)
                                parts1 = str(atten_time01).split(' ')
                                hour_parts1 = parts1[1].split(':')
                                hour1 = int(hour_parts1[0])
                                
                                #if float(second_to) <= float(hour1):
                                if contract.working_hours.is_factory == True:
                                    in_time +=att_line1.worked_hours
                                else:
                                    if int(datetime.strptime(att_line1.name, "%Y-%m-%d %H:%M:%S").weekday()+1) == 6 or int(datetime.strptime(att_line1.name, "%Y-%m-%d %H:%M:%S").weekday()+1) == 7:
                                        in_time +=att_line1.worked_hours
                                    else:
                                        in_time +=att_line1.worked_hours-1
                                        
                                if float(22) <=float(hour1) <=float(23):
                                    if float(att_line1.worked_hours)<float(9):
                                        print "emp_id0",line.id
                                        ds = datetime.strptime(str(att_line1.name), '%Y-%m-%d %H:%M:%S')
                                        datas = ds.replace(minute=0, hour=22, second=0)
                                        datass = datetime.strptime(str(datas), '%Y-%m-%d %H:%M:%S')
                                        atten_time01 = atten_time0 + timedelta(hours=8)
                                        att = atten_time01-datass
                                        n_time +=((att.seconds)/60)/60.0
                                
                                elif float(0) <=float(hour1) <=float(9):
                                    if float(3)<float(att_line1.worked_hours)<float(8):
                                        print "emp_id",line.id
                                        
                                        ds = datetime.strptime(str(att_line1.name), '%Y-%m-%d %H:%M:%S')
                                        datas = ds.replace(minute=0, hour=22, second=0)
                                        datass = datetime.strptime(str(datas), '%Y-%m-%d %H:%M:%S')
                                        atten_time01 = atten_time0 + timedelta(hours=8)
                                        att = atten_time01-datass
                                        n_time +=((att.seconds)/60)/60.0
                                        
                                    elif float(att_line1.worked_hours)>float(8):
                                        print "emp_id2",line.id
                                        dst = datetime.strptime(str(att_line1.name), '%Y-%m-%d %H:%M:%S')
                                        datss = dst.replace(minute=0, hour=22, second=0)
                                        datsss = datetime.strptime(str(datss), '%Y-%m-%d %H:%M:%S')
                                        atten_time02 = atten_time0 + timedelta(hours=8)
                                        attt = atten_time02-datsss
                                        new_time =((attt.seconds)/60)/60.0
                                        if new_time < 8:
                                            n_time +=((attt.seconds)/60)/60.0
                                        else:
                                            n_time +=8

                                work_day +=1
                                
                    
                            
                        
                contract_id  = contract_obj.search(cr, uid, [('employee_id','=',line.id)])
                if contract_id:
                    contract =  contract_obj.browse(cr, uid, contract_id)
                    if contract.working_hours.is_night_shift == False:
                        
                        index = 1
                        index2 = 1
                        for att_line in att_obj.browse(cr, uid, att_id):
                            if att_line.action == 'sign_in':
                                atten_time1 = datetime.strptime(str(att_line.name), '%Y-%m-%d %H:%M:%S')
                                atten_time = atten_time1 + timedelta(hours=8)
                                parts = str(atten_time).split(' ')
                                hour_parts = parts[1].split(':')
                                minute = int(hour_parts[1])
                                hour = int(hour_parts[0])
                                for lined in contract.working_hours.attendance_ids:                                     
                                    if int(datetime.strptime(att_line.name, "%Y-%m-%d %H:%M:%S").weekday()+1) == int(lined.name):
                                        if index == 1:
                                            first_hour = float(lined.hour_from)
                                            first_to = float(lined.hour_to)
                                        else:
                                            second_hour = float(lined.hour_from)
                                            second_to = float(lined.hour_to)
                                        index +=1
                                if float(first_hour) <= float(hour) <= float(first_to):
                                    lost_minute += float(minute)
                                elif float(first_hour)>= hour:
                                    over_time += float(minute)
                                if float(second_to) < float(hour):
                                    n_time
                            else:
                                atten_time2 = datetime.strptime(str(att_line.name), '%Y-%m-%d %H:%M:%S')
                                atten_time2 = atten_time2 + timedelta(hours=9)
                                parts2 = str(atten_time2).split(' ')
                                hour_parts2 = parts2[1].split(':')
                                minute2 = int(hour_parts2[1])
                                hour2 = int(hour_parts2[0])
                                for lined2 in contract.working_hours.attendance_ids:                                     
                                    if int(datetime.strptime(att_line.name, "%Y-%m-%d %H:%M:%S").weekday()+1) == int(lined2.name):
                                        if index2 == 1:
                                            first_hour2 = float(lined2.hour_from)
                                            first_to2 = float(lined2.hour_to)
                                        else:
                                            second_hour2 = float(lined2.hour_from)
                                            second_to2 = float(lined2.hour_to)
                                        index2 +=1
                                if float(first_hour2) <= float(hour2) <= float(first_to2):
                                    over_time += float(minute2)
                                
                                
                                
                    else:

                        n_index = 1
                        for att_line in att_obj.browse(cr, uid, att_id):
                            if att_line.action == 'sign_in':
                                atten_time1 = datetime.strptime(str(att_line.name), '%Y-%m-%d %H:%M:%S')
                                atten_time = atten_time1 + timedelta(hours=9)
                                parts = str(atten_time).split(' ')
                                hour_parts = parts[1].split(':')
                                minute = int(hour_parts[1])
                                hour = int(hour_parts[0])
                                for linee in contract.working_hours.attendance_ids:                                     
                                    if int(datetime.strptime(att_line.name, "%Y-%m-%d %H:%M:%S").weekday()+1) == int(linee.name):
                                        if n_index == 1:
                                            n_first_hour = float(linee.hour_from)
                                            n_first_to = float(linee.hour_to)
                                        else:
                                            n_second_hour = float(linee.hour_from)
                                            n_second_to = float(linee.hour_to)
                                        n_index +=1
                                if n_first_hour <= float(hour) <= n_first_to:
                                    n_lost_minute += float(minute)
                        
            payroll_time = 0.0
            unpayroll_time = 0.0
            shift_time = 0.0
            out_time = 0.0
            official_time = 0.0
            sick_time = 0.0
            shift_day = 0.0
            lost_time = 0.0
            fr_n_time = 0.0
            
            
            
            holiday_id1 = holiday_obj.search(cr, uid, [('employee_id','=',line.id),('date_from','>=',hr_data.start_date),('state','=','validate')])
            if holiday_id1:
                for lines1 in holiday_obj.browse(cr, uid, holiday_id1):
                    if lines1.holiday_status_id.holiday_type == 'shift_holiday':
                        shift_time = lines1.number_of_days_temp
                    
            
            holiday_id = holiday_obj.search(cr, uid, [('employee_id','=',line.id),('date_from','>=',hr_data.start_date),('date_to','<=',hr_data.end_date),('state','=','validate')])
            if holiday_id:
                
                for lines in holiday_obj.browse(cr, uid, holiday_id):
                    
                    if lines.date_from !=False:
                        from_date = datetime.strptime(str(lines.date_from), '%Y-%m-%d %H:%M:%S')
                        ch_from_date = from_date + timedelta(hours=8)
                        to_date = datetime.strptime(str(lines.date_to), '%Y-%m-%d %H:%M:%S')
                        ch_to_date = to_date + timedelta(hours=8)
                        diff_day = ch_to_date-ch_from_date
                        diff_time = ch_to_date-ch_from_date
                        parts = str(diff_time).split(',')
                        
                        from_parts = str(ch_from_date).split(' ')
                        if len(from_parts)==1:
                            from_hours = from_parts[0].split(':')
                        else:
                            from_hours = from_parts[1].split(':')
                        end_parts = str(ch_to_date).split(' ')
                        if len(end_parts)==1:
                            end_hours = end_parts[0].split(':')
                        else:
                            end_hours = end_parts[1].split(':')
                        
                        if len(parts) == 1:
                            hours = parts[0].split(':')
                        else:
                            hours = parts[1].split(':')
                        if int(diff_day.days) >= 1 and hours[0] >= 8:
                            if lines.holiday_status_id.holiday_type == 'payroll_holiday':
                                if float(22) <=float(from_hours[0]) <=float(23):
                                    ds = datetime.strptime(str(lines.date_from), '%Y-%m-%d %H:%M:%S')
                                    datas = ds.replace(minute=0, hour=22, second=0)
                                    datass = datetime.strptime(str(datas), '%Y-%m-%d %H:%M:%S')
                                    atten_time01 = ch_to_date + timedelta(hours=8)
                                    att = atten_time01-datass
                                    out_n_time +=((att.seconds)/60)/60.0
                                elif float(0) <=float(from_hours[0]) <=float(9):
                                    ds = datetime.strptime(str(lines.date_from), '%Y-%m-%d %H:%M:%S')
                                    datas = ds.replace(minute=0, hour=22, second=0)
                                    datass = datetime.strptime(str(datas), '%Y-%m-%d %H:%M:%S')
                                    atten_time01 = ch_to_date + timedelta(hours=8)
                                    att = atten_time01-datass
                                    out_n_time +=((att.seconds)/60)/60.0
                                elif float(22) <=float(end_hours[0]) <=float(23):
                                    ds2 = datetime.strptime(str(ch_to_date), '%Y-%m-%d %H:%M:%S')
                                    datas2 = ds2.replace(minute=0, hour=22, second=0)
                                    datass2 = datetime.strptime(str(datas2), '%Y-%m-%d %H:%M:%S')
                                    atten_time0114 = to_date.replace(minute=0, hour=00, second=0)
                                    attds2 = atten_time0114-datass2
                                    out_n_time +=((attds2.seconds)/60.0)/60.0
                                else:
                                    print "aaaddddddaa",line.id
                                    payroll_time += diff_day.days*8
                                    
                            elif lines.holiday_status_id.holiday_type == 'unpayroll_holiday':
                                
                                unpayroll_time += diff_day.days*8
                            elif lines.holiday_status_id.holiday_type == 'out_work':
                                out_time +=diff_day.days*8
                            elif lines.holiday_status_id.holiday_type == 'official_work':
                                official_time +=diff_day.days*8
                            elif lines.holiday_status_id.holiday_type == 'sick_holiday':
                                sick_time +=diff_day.days*8
                                
                                
                        else:
                            if lines.holiday_status_id.holiday_type == 'payroll_holiday':
                                print "alaaa",from_hours[0]
                                if float(22) <=float(from_hours[0]) <=float(23):
                                    if float(end_hours[0]) <=float(6):
                                        ds = datetime.strptime(str(ch_from_date), '%Y-%m-%d %H:%M:%S')
                                        datas = ds.replace(minute=0, hour=22, second=0)
                                        datass = datetime.strptime(str(datas), '%Y-%m-%d %H:%M:%S')
                                        atten_time011 = to_date + timedelta(hours=8)
                                        attd = atten_time011-datass
                                        out_n_time +=((attd.seconds)/60)/60.0
                                        print "aaddd",line.id
                                    else:
                                        
                                        ds3 = datetime.strptime(str(ch_to_date), '%Y-%m-%d %H:%M:%S')
                                        datas3 = ds3.replace(minute=0, hour=6, second=0)
                                        datass3 = datetime.strptime(str(datas3), '%Y-%m-%d %H:%M:%S')
                                        atten_time0115 = from_date + timedelta(hours=8)
                                        attds5 = datass3-atten_time0115
                                        out_n_time +=((attds5.seconds)/60.0)/60.0
                                        print "laaasdd",line.id,out_n_time,datass3,atten_time0115
                                    
                                    if float(0) <=float(end_hours[0]) <=float(6):
                                        ds3 = datetime.strptime(str(ch_to_date), '%Y-%m-%d %H:%M:%S')
                                        datas3 = ds3.replace(minute=0, hour=6, second=0)
                                        datass3 = datetime.strptime(str(datas3), '%Y-%m-%d %H:%M:%S')
                                        atten_time0115 = to_date + timedelta(hours=8)
                                        attds5 = datass3-atten_time0115
                                        out_n_time +=((attds5.seconds)/60.0)/60.0
                                        print "lineee"
                                #---------- if float(23) <=float(from_hours[0]):
                                    #-------- if float(9) <=float(end_hours[0]):
                                        # ds3 = datetime.strptime(str(ch_to_date), '%Y-%m-%d %H:%M:%S')
                                        # datas3 = ds3.replace(minute=0, hour=6, second=0)
                                        # datass3 = datetime.strptime(str(datas3), '%Y-%m-%d %H:%M:%S')
                                        # atten_time0115 = to_date + timedelta(hours=8)
                                        #------- attds5 = datass3-atten_time0115
                                        # out_n_time +=((attds5.seconds)/60.0)/60.0
                                
                                elif float(22) <=float(end_hours[0]) <=float(23):
                                    ds = datetime.strptime(str(ch_to_date), '%Y-%m-%d %H:%M:%S')
                                    datas = ds.replace(minute=0, hour=22, second=0)
                                    datass = datetime.strptime(str(datas), '%Y-%m-%d %H:%M:%S')
                                    atten_time011 = to_date + timedelta(hours=8)
                                    attd = atten_time011-datass
                                    out_n_time +=(((attd.seconds)/60.0)/60.0)
                                    print "out time",line.id,out_n_time,atten_time011,datass
                                if float(0) <=float(from_hours[0]) <=float(6):
                                    ds = datetime.strptime(str(ch_from_date), '%Y-%m-%d %H:%M:%S')
                                    datas = ds.replace(minute=0, hour=06, second=0)
                                    datass8 = datetime.strptime(str(datas), '%Y-%m-%d %H:%M:%S')
                                    atten_time012 = from_date+timedelta(hours=8)
                                    atts = datass8 - atten_time012
                                    out_n_time +=((atts.seconds)/60.0)/60.0
                                    print "linaa",line.id,out_n_time,datass8,atten_time012
                                    if float(22) <=float(end_hours[0]) <=float(23):
                                        dsd = datetime.strptime(str(ch_to_date), '%Y-%m-%d %H:%M:%S')
                                        datasf = dsd.replace(minute=0, hour=22, second=0)
                                        datassd = datetime.strptime(str(datasf), '%Y-%m-%d %H:%M:%S')
                                        atten_time0111 = to_date + timedelta(hours=8)
                                        attds = atten_time0111-datassd
                                        out_n_time +=((attds.seconds)/60.0)/60.0
                                    
                                if float(10) <=float(end_hours[0]) <=float(18):
                                    ds1 = datetime.strptime(str(from_date), '%Y-%m-%d %H:%M:%S')
                                    datas1 = ds1 + timedelta(hours=8)
                                    datass1 = datetime.strptime(str(datas1), '%Y-%m-%d %H:%M:%S')
                                    atten_time0113 = to_date + timedelta(hours=8)
                                    attds1 = atten_time0113 - datass1
                                    payroll_time +=((attds1.seconds)/3600.0)
                                else:
                                    if float(22) <=float(end_hours[0]) <=float(23):
                                        ds3 = datetime.strptime(str(from_date), '%Y-%m-%d %H:%M:%S')
                                        datas3 = ds3 + timedelta(hours=8)
                                        datass3 = datetime.strptime(str(datas3), '%Y-%m-%d %H:%M:%S')
                                        atten_time0115 = to_date + timedelta(hours=8)
                                        datass4 = datetime.strptime(str(atten_time0115), '%Y-%m-%d %H:%M:%S')
                                        attds3 = datass4-datass3
                                        
                                        payroll_time +=((attds3.seconds)/3600)
                                        
                                        
                                    elif float(0) <=float(end_hours[0]) <=float(6):
                                        ds2 = datetime.strptime(str(from_date), '%Y-%m-%d %H:%M:%S')
                                        datas2 = ds2 + timedelta(hours=8)
                                        datass2 = datetime.strptime(str(datas2), '%Y-%m-%d %H:%M:%S')
                                        atten_time0114 = to_date + timedelta(hours=8)
                                        attds2 = atten_time0114-datass2
                                        payroll_time +=((attds2.seconds)/60.0)/60.0
                                        
                                        
                            elif lines.holiday_status_id.holiday_type == 'unpayroll_holiday':
                                
                                unpayroll_time += int(hours[0])
                            elif lines.holiday_status_id.holiday_type == 'out_work':
                                out_time +=int(hours[0])
                            elif lines.holiday_status_id.holiday_type == 'official_work':
                                official_time +=int(hours[0])
                            elif lines.holiday_status_id.holiday_type == 'sick_holiday':
                                sick_time +=int(hours[0])
                        
                    

                
                
            
            
            cr.execute("select working_hours from hr_contract where employee_id=%s order by id desc limit 1"%(line.id))
            timetable = cr.fetchone()
            if timetable == None and not timetable:
               raise osv.except_osv((u'Алдаа!'), (u"%s энэ ажилтаны гэрээ үүсээгүй эсвэл гэрээ нь дээр цагийн хуваарь оруулаагүй байна !")%(line.name))
            for day in days:
                date1 = day.strftime('%Y-%m-%d')
                cr.execute('''
                    select hour_from, hour_to from resource_calendar_attendance where calendar_id=%s and dayofweek='%s' order by hour_from
                '''%(timetable[0], date.weekday(datetime.strptime(date1, DATE_FORMAT))))
                fetched=cr.dictfetchall()
                if fetched:
                    due_day += 1
                    for worktime in fetched:
                        due_time += worktime['hour_to'] - worktime['hour_from']
                        
                        
            if hr_data.holiday_day > 0.0:
                total_t = due_time/due_day
                due_day = due_day-hr_data.holiday_day
                due_time = due_day*total_t 
                
            
            total_time = in_time+payroll_time+official_time+sick_time
            over_time = total_time - due_time
            if over_time > 0.0:
                over_time = over_time
                lost_time = 0.0
            else:
                lost_time = -over_time
                over_time = 0.0
            
            
            if line.id==26:
                print "",n_time
            emp_info.create(cr, uid, {
                                      'work_day':work_day,
                                      'employee_id': line.id,
                                      'parentd_id': ids[0],
                                      'position': line.job_id.id,
                                      'due_day': due_day,
                                      'due_time': due_time,
                                      'shift_holiday':shift_time,
                                      'outer_night_time':out_n_time,
                                      'night_time':n_time+fr_n_time,
                                      'sick_time':sick_time,
                                      'miss_time':lost_time,
                                      'free_time1':payroll_time,
                                      'free_time2':unpayroll_time,
                                      'more_time':over_time,
                                      'more_time1':official_time,
                                      'work_time':in_time-n_time,
                                      'total_time':total_time}, context=context)
#                                           })
    def delete_lines(self, cr, uid, ids, context=None):
        self_obj = self.pool.get('hr.hour.record')
        line_pool = self.pool.get('hr.hour.record.line')
        for invoice in self_obj.browse(cr, uid, ids, context=context):
            line_ids = [line.id for line in invoice.line_id]
            line_pool.unlink(cr, uid, line_ids, context=context)
        return True
    
    def workdaysub(uid,start_date, end_date):
        '''
        Calculate the number of working days between two dates inclusive
        (start_date <= end_date).
    
        The actual working days can be set with the optional whichdays
        parameter
        (default is MON-FRI)
        '''
        (MON, TUE, WED, THU, FRI, SAT, SUN) = range(7)
        whichdays=(MON,TUE,WED,THU,FRI)
        delta_days = (end_date - start_date).days + 1
        full_weeks, extra_days = divmod(delta_days, 7)
        # num_workdays = how many days/week you work * total # of weeks
        num_workdays = (full_weeks + 1) * len(whichdays)
        # subtract out any working days that fall in the 'shortened week'
        for d in range(1, 8 - extra_days):
                    if (end_date + timedelta(d)).weekday() in whichdays:
                                    num_workdays -= 1
        return num_workdays
    def action_cancel(self, cr, uid, ids, context=None):
        return {
            self.write(cr, uid, ids[0], {'state' : 'draft',
                                          })
        }
    def action_confirm(self, cr, uid, ids, context=None):
         hr_obj = self.pool.get('hr.hour.record')
         seq = self.pool.get('ir.sequence').get(cr, uid, 'hr.hour.record', context=context)
         self.write(cr, uid, ids[0], {
                                      'name':seq,
                                      'state' : 'confirmed',
                                  })

         return True
    # Override
    def action_print(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        datas = {
            'ids': context.get('active_ids', [])
        }
        datas['model'] = 'hr.hour.record'
        datas['form'] = self.read(cr, uid, ids)[0]
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hr.hour.record.report',
            'datas': datas,
            'nodestroy': True
        }
    def action_compute(self,cr, uid, ids,context=None):
        obj = self.browse(cr, uid, ids)
        line_obj = self.pool.get('hr.hour.record.line')
        total_time = 0.0
        for line in obj.line_id:
            total_time =line.work_time+line.night_time+line.free_time1
            line_obj.write(cr, uid, line.id,{
                                             'total_time':total_time,
                                             })
            
        return True
    


class hr_hour_record_line(osv.osv):
    '''
    Ажилтаны цагийн бүртгэлийн мөр
    '''
    _name = "hr.hour.record.line"
    _order = "position"
    _description = 'Information on a employee'
    
    _columns = {
        'employee_id': fields.many2one('hr.employee', 'Employee name'),
        'position': fields.many2one('hr.job','Position'),
        'due_day': fields.integer('due day'),
        'due_time': fields.integer('due time'),
        'work_day': fields.integer('work day'),
        'work_time': fields.float('work time'),
        'sick_time': fields.float('sick time'),
        'outer_night_time':fields.float('outer night time'),
        'shift_holiday':fields.float('Shift holiday'),
        'miss_time': fields.float('miss time'),
        'night_time':fields.float('Night work time'),
        'free_time1': fields.float('free time1'),
        'free_time2': fields.float('free time2'),
        'more_time': fields.float('more time'),
        'more_time1': fields.float('Legal holiday'),
        'late_minut': fields.float('late minut'),
        'total_time':fields.float('Total compute time'),
        'fine_size': fields.integer('Attendance  bonus'),
        'execution_percent': fields.integer('execution percent'),
        'parentd_id': fields.many2one('hr.hour.record'),
        }
    
    def onchange_change_time(self, cr, uid, ids, day):
        obj = self.browse(cr, uid, ids)
        due_time = 0.0
        miss_time = 0.0
        over_time = 0.0
        diff_time = 0.0
        if day:
            due_time = day*8
            diff_time = obj.work_time-due_time
            if diff_time > 0.0:
                over_time = diff_time
            else:
                miss_time = -diff_time
            
        
        res = {'value':{
                        'due_time':due_time,
                        'miss_time':miss_time,
                        'more_time':over_time,
                            }}
        return res
    
    def onchange_work_time(self, cr, uid, ids, work):
        obj = self.browse(cr, uid, ids)
        miss_time = 0.0
        over_time = 0.0
        diff_time = 0.0
        if work:
            diff_time = work-obj.due_time
            if diff_time > 0.0:
                over_time = diff_time
            else:
                miss_time = -diff_time
            
        
        res = {'value':{
                        'work_time':work,
                        'miss_time':miss_time,
                        'more_time':over_time,
                            }}
        return res
   

class hr_holidays_status(osv.osv):
    _inherit = 'hr.holidays.status'
    
    
    _columns = {
                'holiday_type':fields.selection([('payroll_holiday','Payroll holiday'),
                                         ('unpayroll_holiday','Unpayroll holiday'),
                                         ('shift_holiday','Shift holiday'),
                                         ('sick_holiday','Sick holiday'),
                                         ('official_work','Officical work'),
                                         ('out_work','Out work')],'Holiday type',required=True),

                }
    _defaults = {
                 'holiday_type':'payroll_holiday',
                 }
    
    