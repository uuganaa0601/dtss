# -*- coding: utf-8 -*-
##############################################################################
#
#    
#
#    
#
##############################################################################

{
    "name": "Hr inherit module",
    "version": "1.0",
    "author": "Uuganbayar",
    "category": "Hr",
    "description": """This module hr inherit.""",
    "depends" : [
                 'hr',
                 "base",
                 'account',
                 'hr_attendance',
                 'hr_timesheet',
                 'hr_holidays',
                 'hr_contract',
                 'hr_recruitment',
                 ],
    'update_xml': [
                   "wizard/change_job_position_view.xml",
                   "hr_view.xml",
                   "res_company_view.xml",
                   "hr_hour_record_view.xml",
                   'report/report_hr_view.xml',
                   'security/ir.model.access.csv',
                  # "views/hr_apply_report.xml",
                   ],
    'test': [],
    'installable': True,
    'auto_install': False, 
     "icon": '/hr_inherit/static/src/img/mn_icon.png',
}