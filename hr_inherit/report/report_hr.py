# -*- coding: utf-8 -*-
##############################################################################
#  
##############################################################################

from openerp import tools
from openerp.osv import fields, osv

from openerp.tools.sql import drop_view_if_exists

class report_nemegdel(osv.osv):
    _name = "report.nemegdel"
    _auto = False

    _columns = {
                

        'emp_id':fields.many2one('hr.employee',u'Ажилтан'),
        'basic_type':fields.selection([('urchadvar','Ур чадвар'),
                                          ('udaanjil','Удаан жилийн нэмэгдэл'),
                                          ('albantushaal','Албан тушаал хавсарсан')],'Нэмэгдэл төрөл', required=True),
        'date':fields.char('Өдөр',readonly=True),
        'month':fields.char('Сар',readonly=True),
        'year':fields.char('Жил',readonly=True),
        'job_id': fields.many2one('hr.job','Албан тушаал',readonly=True)
  
    }

    def init(self, cr):
        drop_view_if_exists(cr, 'report_nemegdel')
        cr.execute("""
            create or replace view report_nemegdel as (
               SELECT n.id, n.emp_id, n.basic_type, to_char(n.date, 'YYYY-MM-DD') as date, to_char(n.date, 'YYYY-MM') as month, 
                 to_char(n.date, 'YYYY') as year, e.job_id
                   FROM
                  nemegdel n
                 LEFT JOIN hr_employee e on (e.id = n.emp_id)
            )""")
        
class report_tetgemj(osv.osv):
    _name = "report.tetgemj"
    _auto = False

    _columns = {
                

        'emp_id':fields.many2one('hr.employee',u'Ажилтан', required=True),
        'tetgemjtype_id':fields.many2one('tetgemjtype',u'Тэтгэмжийн төрөл', required=True),
        'basic_type':fields.selection([('worker','Ажилчид'),
                                          ('ondornastan','Өндөр настанд'),
                                          ('otherorg','Гадны байгууллага')],'Үндсэн төрөл', required=True),
        'total':fields.integer(u'Тэтгэмжийн дүн', required=True),
        'date':fields.char('Өдөр',readonly=True),
        'month':fields.char('Сар',readonly=True),
        'year':fields.char('Жил',readonly=True),
        'job_id': fields.many2one('hr.job','Албан тушаал',readonly=True)
  
    }

    def init(self, cr):
        drop_view_if_exists(cr, 'report_tetgemj')
        cr.execute("""
            create or replace view report_tetgemj as (
               SELECT n.id, n.emp_id, n.basic_type, n.tetgemjtype_id, to_char(n.date, 'YYYY-MM-DD') as date, to_char(n.date, 'YYYY-MM') as month, 
                   to_char(n.date, 'YYYY') as year, e.job_id, sum(n.total) as total
                   FROM
                  tetgemj n
                 LEFT JOIN hr_employee e on (e.id = n.emp_id)
                 group by n.emp_id, n.basic_type, n.tetgemjtype_id, n.date,e.job_id,n.id
            )""")
        
class reportaward(osv.osv):
    _name = "reportaward"
    _auto = False

    _columns = {
                

        'employee_id':fields.many2one('hr.employee',u'Ажилтан', required=True),
        'shagnaltype_id':fields.many2one('shagnaltype',u'Шагналын төрөл', required=True),
        'date':fields.char('Өдөр',readonly=True),
        'month':fields.char('Сар',readonly=True),
        'year':fields.char('Жил',readonly=True),
        'job_id': fields.many2one('hr.job','Албан тушаал',readonly=True),
        'department_id': fields.many2one('hr.department','Хэлтэс',readonly=True)
  
    }

    def init(self, cr):
        drop_view_if_exists(cr, 'reportaward')
        cr.execute("""
            create or replace view reportaward as (
               SELECT n.id, n.employee_id, n.shagnaltype_id,  to_char(n.award_date, 'YYYY-MM-DD') as date, to_char(n.award_date, 'YYYY-MM') as month, 
               to_char(n.award_date, 'YYYY') as year, e.job_id, e.department_id
                   FROM
                  award n
                 LEFT JOIN hr_employee e on (e.id = n.employee_id)
            )""")
        

class reportshiitgel(osv.osv):
    _name = "reportshiitgel"
    _auto = False

    _columns = {
                

        'employee_id':fields.many2one('hr.employee',u'Ажилтан', required=True),
        'shiitgeltype_id':fields.many2one('shiitgeltype','Шийтгэлийн төрөл'),
        'shiitgel_id':fields.many2one('shiitgel','Авсан шийтгэл'),
        'date':fields.char('Өдөр',readonly=True),
        'month':fields.char('Сар',readonly=True),
        'year':fields.char('Жил',readonly=True),
        'job_id': fields.many2one('hr.job','Албан тушаал',readonly=True),
        'department_id': fields.many2one('hr.department','Хэлтэс',readonly=True)
  
    }

    def init(self, cr):
        drop_view_if_exists(cr, 'reportshiitgel')
        cr.execute("""
            create or replace view reportshiitgel as (
               SELECT n.id, n.employee_id, n.shiitgeltype_id, n.shiitgel_id, to_char(n.punishment_date, 'YYYY-MM-DD') as date, to_char(n.punishment_date, 'YYYY-MM') as month, 
                to_char(n.punishment_date, 'YYYY') as year, e.job_id, e.department_id
                   FROM
                  punishment n
                 LEFT JOIN hr_employee e on (e.id = n.employee_id)
            )""")

class reportorson(osv.osv):
    _name = "reportorson"
    _auto = False

    _columns = {
                

        'employee_id':fields.many2one('hr.employee',u'Ажилтан', required=True),
        'date':fields.char('Өдөр',readonly=True),
        'month':fields.char('Сар',readonly=True),
        'year':fields.char('Жил',readonly=True),
        'job_id': fields.many2one('hr.job','Албан тушаал',readonly=True),
        'department_id': fields.many2one('hr.department','Хэлтэс',readonly=True)
  
    }

    def init(self, cr):
        drop_view_if_exists(cr, 'reportorson')
        cr.execute("""
            create or replace view reportorson as (
              select id as id,id as employee_id, job_id, department_id, to_char(ajild_orson_ognoo, 'YYYY-MM-DD') as date, to_char(ajild_orson_ognoo, 'YYYY-MM') as month, 
                to_char(ajild_orson_ognoo, 'YYYY') as year from hr_employee where ajild_orson_ognoo is not null 
            )""")
        
class reportgarsan(osv.osv):
    _name = "reportgarsan"
    _auto = False

    _columns = {
                

        'employee_id':fields.many2one('hr.employee',u'Ажилтан', required=True),
        'date':fields.char('Өдөр',readonly=True),
        'month':fields.char('Сар',readonly=True),
        'year':fields.char('Жил',readonly=True),
        'job_id': fields.many2one('hr.job','Албан тушаал',readonly=True),
        'department_id': fields.many2one('hr.department','Хэлтэс',readonly=True),
        'garsanshaltgaan_id':fields.many2one('garsanshaltgaan','Ажлаас гарсан шалтгаан'),
  
    }

    def init(self, cr):
        drop_view_if_exists(cr, 'reportgarsan')
        cr.execute("""
            create or replace view reportgarsan as (
                select n.id as id, n.id as employee_id, n.garsanshaltgaan_id,n.job_id, n.department_id, to_char(n.ajlaas_garsan_ognoo, 'YYYY-MM-DD') as date, to_char(n.ajlaas_garsan_ognoo, 'YYYY-MM') as month, 
                to_char(n.ajlaas_garsan_ognoo, 'YYYY') as year from hr_employee n
                LEFT JOIN resource_resource r on (r.id = n.resource_id)
         where  r.active = False
            )""")
        
class reportudaanjil(osv.osv):
    _name = "reportudaanjil"
    _auto = False

    _columns = {
                
        
        'emp_id':fields.many2one('hr.employee',u'Ажилтан',readonly=True),
        'uls':fields.integer(u'Улсад ажилласан жил',readonly=True),
        'uls_sar':fields.integer(u'Сар',readonly=True),
        'erch':fields.integer(u'ЭХСалбарт ажилласан жил',readonly=True),
        'own':fields.integer(u'ДЦСтанцд ажилласан жил',readonly=True),
  
    }

    def init(self, cr):
        drop_view_if_exists(cr, 'reportudaanjil')
        cr.execute("""
            create or replace view reportudaanjil as (
                select e.id as emp_id, e.id,

                            (e.year_country + ( date('now')- e.date_burtgegdsen)/365) as uls,
                             date_part('month',age(date('now'), e.date_burtgegdsen)) as uls_sar,
                            (e.year_energy_sector + ( date('now')- e.date_burtgegdsen)/365) as erch,
                            (e.year_own_org + ( date('now')- e.date_burtgegdsen)/365) as own
                                   
                            from hr_employee e
                            LEFT JOIN resource_resource r on (r.id = e.resource_id)
                                where  r.active = True 
            )""")
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
