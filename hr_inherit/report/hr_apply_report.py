# -*- coding: utf-8 -*-
##############################################################################
#
#    ShineERP, Enterprise Management Solution    
#    Copyright (C) 2014-2020 ShineERP Co.,ltd (<http://www.serp.mn>). 
#    All Rights Reserved
#
#    Address : Suhkbaatar District, National ITPark, 205, ShineERP LLC
#    Email : info@serp.mn
#    Phone : 976 + 11-89060761, 97119993
#
##############################################################################

from openerp.osv import osv
from openerp.report import report_sxw

class hr_apply_report(report_sxw.rml_parse):
    name = 'report.hr_inherit.hr.apply.report'
    def set_context(self, objects, data, ids, report_type=None):
        new_ids = ids
        return super(hr_apply_report, self).set_context(objects, data, new_ids, report_type=report_type)
    
    def __init__(self, cr, uid, name, context=None):
        if context is None:
            context = {}
        super(hr_apply_report, self).__init__(cr, uid, name, context=context)
        self.context = context

class report_hr_apply(osv.AbstractModel):
    
    ''' Төрийн албан хаагчийн анкет
    '''
    _name = 'report.hr_inherit.hr_apply_report'
    _inherit = 'report.abstract_report'
    _template = 'hr_inherit.report_hr_apply'
    _wrapped_report_class = hr_apply_report