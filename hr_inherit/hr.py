# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-today OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime, date
from lxml import etree
import time

from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
from bsddb.dbtables import _columns

class res_users(osv.osv):
    _inherit = 'res.users'
    
    _columns = {
                'dep_id':fields.many2one('hr.department','Hr department'),
                
                }
    
class hr_holidays_status(osv.osv):
    _inherit = 'hr.holidays.status'
    
    
    _columns = {
                'holiday_type':fields.selection([('payroll_holiday','Payroll holiday'),
                                         ('unpayroll_holiday','Unpayroll holiday'),
                                         ('shift_holiday','Shift holiday'),
                                         ('sick_holiday','Sick holiday'),
                                         ('official_work','Official work'),
                                         ('out_work','Out work')],'Holiday type',required=True),

                }
    _defaults = {
                 'holiday_type':'payroll_holiday',
                 }
class hr_employee(osv.osv):
    _inherit = 'hr.employee'
    
    def _get_age(self, cr, uid, ids, name, args, context=None):
        res = {}
        age = 0
        for emp in self.browse(cr, uid, ids, context=context):
          
            now_age = 0
            year = 0
            month = 0
            day = 0
            year1 = 0
            month1 = 0
            day1 = 0
            diff_year = 0
            diff_month = 0
            diff_day = 0
            if  emp.birthday:
                year = datetime.now().strftime('%Y-%m-%d')[0:4]
                year1 = emp.birthday[0:4]
                month = datetime.now().strftime('%Y-%m-%d')[5:7]
                month1 = emp.birthday[5:7]
                day = datetime.now().strftime('%Y-%m-%d')[8:10]
                day1 = emp.birthday[8:10]
                diff_year = int(year) - int(year1)
                diff_month = int(month) - int(month1)
                diff_day = int(day) - int(day1)
                if diff_month > 0:
                    now_age = diff_year
                elif diff_month == 0:
                    if  diff_day >= 0:
                        now_age = diff_year
                    else:
                        now_age = diff_year -1
                else:
                    now_age = diff_year -1
            
               
        res[emp.id] = now_age
                   
           
        return res
    
    
    _columns = {
                'change_history_id':fields.one2many('change.job.history','employee_id','Change history'),
                'family_id':fields.one2many('family.register','employee_id','Family register'),
                'experience_id':fields.one2many('work.experience','employee_id','Work experience'),
                'professional_id':fields.one2many('professional.training','employee_id','Professional training'),
                'professional2_id':fields.one2many('professional.training','employee_id','Professional training'),
                'professional3_id':fields.one2many('professional.training','employee_id','Professional training'),
                'education_id':fields.one2many('education.information','employee_id','Education information'),
                'education2_id':fields.one2many('education.information','employee_id','Education information'),
                'relative_id':fields.one2many('relative.register','employee_id','Relative information'),
                'street': fields.char('Street'),
                'zip': fields.char('Zip', size=24, change_default=True),
                'city': fields.char('City'),
                'state_id': fields.many2one("res.country.state", 'State', ondelete='restrict'),
                'state2_id': fields.many2one("res.country.state", 'State', ondelete='restrict'),
                'districts':fields.many2one('district','District'),
                'ascriptions':fields.many2one('ascription','Ascription'),
                'lasts_name':fields.char('Lasts name'),
                'nationalitys':fields.many2one('nationality','Nationality'),
                'country_id': fields.many2one('res.country', 'Country', ondelete='restrict'),
                'birt_location':fields.char('Birth location'),
                'phone': fields.char('Phone'),
                'mobile': fields.char('Mobile'),
                'email_address':fields.char('Email address'),
                'post_address':fields.char('Post address'),
                'emdd': fields.char(u'ЭМД №'),
                'ndd': fields.char(u'НДДэвтэр №'),
                'is_army': fields.boolean(u'Цэргийн алба хаасан эсэх'),
                'salary': fields.float(u'Цалин'),
                'ergonomist': fields.selection([('hard', u'Хүнд'),('normal', u'Хэвийн')],u'Хөдөлмөрийн нөхцөл'),
                'year_country': fields.float(u'Улсад ажилласан жил'),
                'date_country': fields.date(u'Улсад ажиллаж эхэлсэн'),
                'date_burtgegdsen': fields.date(u'Бүртгэсэн огноо'),
                'year_energy_sector': fields.float(u'Эрчим хүчний салбарт ажилласан жил'),
                'year_own_org': fields.float(u'ДЦСтанцд ажилласан жил'),
                'year_hard_condition': fields.float(u'Хүнд нөхцөлд ажилласан жил'),
                'year_normal_condition': fields.float(u'Хэвийн нөхцөлд ажилласан жил'),
                #'specialty_rank': fields.selection([('1', '1'),('2', '2'),('3', '3'),('4', '4'),('5', '5'),('6', '6')],'Мэргэжлийн зэрэг инженер'),
                'punishment_id':fields.one2many('punishment','employee_id',u'Шийтгэл'),
                'award_id':fields.one2many('award','employee_id',u'Шагнал'),
                'age' : fields.function(_get_age, type="integer", string=u"Нас"),
                'status': fields.selection([('jinhelsen', u'Жинхэлсэн'),('dagaldan', u'Дагалдан')],u'Ажилд орсон тушаал'),
                'code' : fields.char(u"Код", size=12),
                'register_number' : fields.char(u"Регистерийн дугаар", size=10),
                'ajild_orson_ognoo': fields.date(u'Ажилд орсон огноо'),
                'ajlaas_garsan_ognoo': fields.date(u'Ажлаас гарсан огноо'),
                'zereg_type': fields.selection([('zuuhash', u'Зуух, Түлш ашиглалт'),('zuuhzas', u'Зуух, Түлш засвар'),
                                                ('turash', u'Турбин ашиглалт'),('turzas', u'Турбин засвар'),
                                                ('tsahash', u'Цахилгаан, ДХХА ашиглалт'),('tsazas', u'Цахилгаан, ДХХА засвар'),
                                                ('himiash', u'Хими, Шугам сүлжээ ашиглалт'),('himizas', u'Хими, Шугам сүлжээ засвар')],u'Зэрэг төрөл'),
                'zereg': fields.selection([('i', 'I'),('ii', 'II'),('iii', 'III'),
                                           ('iv', 'IV'),('v', 'V'),('vi', 'VI')],u'Мэргэжлийн зэрэг'),
                'zereg_date': fields.date(u'Зэрэг авсан огноо'),
                'tetgemj_ids':fields.one2many('tetgemj','emp_id',u'Тэтгэмж'),
                'nemegdel_ids':fields.one2many('nemegdel','emp_id',u'Нэмэгдэл'),
                'degree_id':fields.many2one('hr.recruitment.degree',u'Боловсрол'),
                'garsanshaltgaan_id':fields.many2one('garsanshaltgaan',u'Ажлаас гарсан шалтгаан'),
                'blood_cat': fields.selection([('1', '1'),('2', '2'),('3', '3'),('4', '4')],u'Цусны бүлэг'),
                'holboobarihhu_ids':fields.one2many('holboobarihhun','employee_id',u'Холбоо барих хүн'),
                'shalgaltinfo_ids':fields.one2many('shalgaltinfo','employee_id',u'Аюулгүй ажиллагаа Техник ашиглалтын шалгалт'),
                'emuzleg_ids':fields.one2many('emuzleg','employee_id',u'Эрүүл мэндийн үзлэг'),
                'hhhuvtsas_ids':fields.one2many('hhhuvtsas','employee_id',u'Хөдөлмөр хамгааллын хувцас'),
                
                
                
                }
    
    
    
    def hr_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = {}
        obj = self.browse(cr, uid, ids)[0]
        data['name'] = obj.name
        return self.pool['report'].get_action(cr, uid, [], 'hr_inherit.report_hr_apply', data=data, context=context)
    
    def create(self, cr, uid, data, context=None):
        context = dict(context or {})
        if context.get("mail_broadcast"):
            context['mail_create_nolog'] = True
        if 'department_id' in data and not data['department_id']:
            self.pool.get('res.users').write(cr, uid, uid, {
                                                            'dep_id':data['department_id'],
                                                            })
        employee_id = super(hr_employee, self).create(cr, uid, data, context=context)

        if context.get("mail_broadcast"):
            self._broadcast_welcome(cr, uid, employee_id, context=context)
            
        return employee_id
    
    def write(self, cr, uid, ids, vals, context=None):
        obj = self.browse(cr, uid, ids)[0]
        if 'department_id' in vals and not vals['department_id']:
             self.pool.get('res.users').write(cr, uid, obj.user_id.id, {
                                                            'dep_id':vals['department_id'],
                                                            })
            
        return super(hr_employee, self).write(cr, uid, ids, vals, context=context)
    
    def onchange_job_id(self, cr, uid, ids, job_id,  context=None):
        if job_id:
           job_obj = self.pool.get('hr.job')  
           job = job_obj.browse(cr, uid, job_id)
           return {'value': {
                             'ergonomist': job.ergonomist,
                             }}
    

class hr_job(osv.osv):
    _inherit = 'hr.job'
    _columns = {
                'ergonomist': fields.selection([('hard', 'Хүнд'),('normal', 'Хэвийн')],'Хөдөлмөрийн нөхцөл'),
                }
    
class hr_department(osv.osv):
    _inherit = 'hr.department'
    _columns = {
                'uachiglel': fields.char(u'Үйл ажиллагааны чиглэл'),
                'mission': fields.char(u'Зорилго'),
                'ham_huree': fields.char(u'Хамрах хүрээ'),
                }


class nationality(osv.osv):
    _name = 'nationality'
    _description = "Nationality"
    
    _columns = {
                'name':fields.char('Nationality')
                }

class ascription(osv.osv):
    _name = 'ascription'
    _description = "Ascription"
    _columns = {
                'name':fields.char('Ascription name')
                }
class district(osv.osv):
    _name = 'district'
    _description = "District"
    _columns = {
                'name':fields.char('District name'),
                }

class family_register(osv.osv):
    _name = "family.register"
    _description = "Family register"
    _columns = {
                'employee_id':fields.many2one('hr.employee','Employee'),
                'name':fields.char('Your family'),
                'family_name':fields.char('Family name'),
                'birth_date':fields.date('Birth date'),
                'state':fields.char('State'),
                'work_name':fields.char('Now work'),
                }

class shalgaltinfo(osv.osv):
    _name = "shalgaltinfo"
    _columns = {
                'employee_id':fields.many2one('hr.employee',u'Ажилтан'),
                'dun':fields.float(u'Ямар дүн авсан'),
                'hugatsaa':fields.char(u'Шалгалт нэр'),
                'date':fields.date(u'Огноо'),
                }

class emuzleg(osv.osv):
    _name = "emuzleg"
    _columns = {
                'employee_id':fields.many2one('hr.employee',u'Ажилтан'),
                'chiglel':fields.char(u'Ямар чиглэлээр'),
                'where':fields.char(u'Хаана'),
                'date':fields.date(u'Хэзээ'),
                }

class hhhuvtsas(osv.osv):
    _name = "hhhuvtsas"
    _columns = {
                'employee_id':fields.many2one('hr.employee',u'Ажилтан'),
                'chiglel':fields.char(u'Ямар төрлөөр олгогдсон'),
                'date':fields.date(u'Хэзээ'),
                }
    
class holboobarihhun(osv.osv):
    _name = "holboobarihhun"
    _columns = {
                'employee_id':fields.many2one('hr.employee',u'Ажилтан'),
                'name':fields.char(u'Холбоо барих хүн'),
                'hamaaral':fields.char(u'Холбоо хамаарал'),
                'phone':fields.char(u'Утас',size=16),
                }

class relative_register(osv.osv):
    _name = 'relative.register'
    _description = "Relative register"
    _columns = {
                'employee_id':fields.many2one('hr.employee','Employee'),
                'name':fields.char('Who is'),
                'family_name':fields.char('Family name'),
                'birth_date':fields.date('Birth date'),
                'state':fields.char('State'),
                'work_name':fields.char('Now work'),
                }
    
class education_information(osv.osv):
    _name = "education.information"
    _description = "Education information"
    _columns = {
                
                'name':fields.char('School name'),
                'in_date':fields.date('In date'),
                'out_date':fields.date('Out date'),
                'edu_degree':fields.char('Education degree'),
                'edu_number':fields.char('Education number'),
                'edu_date':fields.char('Education date'),
                'location_name':fields.char('Location name'),
                'grade':fields.char('Grade'),
                'date':fields.date('Date'),
                'register_number':fields.char('Register number'),
                'employee_id':fields.many2one('hr.employee','Employee'),
                }
class professional_training(osv.osv):
    _name = 'professional.training'
    _description = "Professional training"
    _columns = {
                'name':fields.char('Training name'),
                'organisation':fields.char('Organisation'),
                'start_date':fields.date('Start date'),
                'end_date':fields.date('End date'),
                'register_number':fields.char('Register number'),
                'position_category':fields.char('Position category'),
                'grade':fields.char('Grade name'),
                'command_date':fields.char('Command date'),
                'certify_number':fields.char('Certify number'),
                'atterneyship':fields.char('Atterneyship'),
                'attern_organisation':fields.char('Atterneyship organisation'),
                'date':fields.date('Date'),
                'diplom_number':fields.char('Diplom number'),
                'employee_id':fields.many2one('hr.employee','Employee'),
                }
    
    
class work_experience(osv.osv):
    _name = "work.experience"
    
    
    def _get_month(self, cr, uid, ids, name, args, context=None):
        res = {}
        month = 0
        for work in self.browse(cr, uid, ids, context=context):
            if  work.work_in_date and work.work_out_date:
                if work.work_in_date < work.work_out_date:
                   date1 = datetime.strptime(work.work_in_date, '%Y-%m-%d')
                   date2 = datetime.strptime(work.work_out_date, '%Y-%m-%d')
                   if date2.day >= date1.day:
                      
                      month = (date2.year - date1.year) * 12 + date2.month - date1.month 
                   else:
                      month = (date2.year - date1.year) * 12 + date2.month - date1.month - 1
                else:
                    raise osv.except_osv(_('Анхааруулга!'),_("Ажилд орсон огнооноос ажлаас гарсан огноо хойш байх ёстой !!"))
              
        if month == 12:
           year = 1
        elif month > 12:
           year1 = int(month/12)
           year = year1 + (month -year1*12)*0.01
        else:
            year = month*0.01
        res[work.id] = year
                   
           
        return res
    
    _description = 'Work experience'
    _columns = {
                'name':fields.char('Work name and position'),
                'job_position':fields.char('Job position'),
                'work_in_date':fields.date('Work in date'),
                'work_out_date':fields.date('Work out date'),
                'employee_id':fields.many2one('hr.employee','Employee'),
                'ergonomist': fields.selection([('hard', u'Хүнд'),('normal', u'Хэвийн')],u'Хөдөлмөрийн нөхцөл'),
                'is_work_country': fields.boolean(u'Улс ажилласан'),
                'is_energy_sector': fields.boolean(u'Эрчим хүчний салбар'),
                'work_month' : fields.function(_get_month, type="float", string=u"Жил", store=True),
                }

class award(osv.osv):
    _name = 'award'
    _columns = {
               'name':fields.char(u'Шагналын нэр'),
               'award_date':fields.date(u'Огноо'),
               #'award_where':fields.char('Хаана'),
               'award_description':fields.char(u'Тайлбар'),
               'employee_id':fields.many2one('hr.employee',u'Ажилтан'),
               'applicant_id':fields.many2one('hr.applicant',u'Горилогч'),
               'shagnaltype_id':fields.many2one('shagnaltype',u'Шагналын төрөл'),
               }

class punishment(osv.osv):
    _name = 'punishment'
    _columns = {
               'name':fields.char(u'Шийтгэл нэр'),
               'punishment_date':fields.date(u'Огноо'),
               #'punishment_where':fields.char('Хаана'),
               'punishment_description':fields.char(u'Тайлбар'),
               'employee_id':fields.many2one('hr.employee',u'Ажилтан'),
               'applicant_id':fields.many2one('hr.applicant',u'Горилогч'),
                'shiitgeltype_id':fields.many2one('shiitgeltype',u'Шийтгэлийн төрөл'),
                'shiitgel_id':fields.many2one('shiitgel',u'Авсан шийтгэл'),
               }
    
    
class self_skills(osv.osv):
    _name = 'self.skills'
    _description = 'Self skills'
    _columns = {
                'name':fields.char('Name'),
                'level':fields.selection([('medium','Medium'),
                                          ('good','Good'),
                                          ('excellent','Excellent')],'Level'),
                'listen_level':fields.selection([('medium','Medium'),
                                          ('good','Good'),
                                          ('excellent','Excellent')],'Level'),
                'read_level':fields.selection([('medium','Medium'),
                                          ('good','Good'),
                                          ('excellent','Excellent')],'Level'),
                'write_level':fields.selection([('medium','Medium'),
                                          ('good','Good'),
                                          ('excellent','Excellent')],'Level'),
                'speak_level':fields.selection([('medium','Medium'),
                                          ('good','Good'),
                                          ('excellent','Excellent')],'Level'),
                'language_name':fields.char('Language name'),
                }
    
class change_job_history(osv.osv):
    _name = 'change.job.history'
    
    _columns = {
                'company_id':fields.many2one('res.company',u'Компани'),
                'change_date':fields.datetime(u'Огноо'),
                'change_work_id':fields.many2one('res.users',u'Бүртгэлийн ажилтан'),
                'job_id':fields.many2one('hr.job',u'Шинэ албан тушаал'),
                'old_job_id':fields.many2one('hr.job',u'Хуучин албан тушаал'),
                'employee_id':fields.many2one('hr.employee',u'Шилжсэн ажилтан'),
                
        }
    
    
class hr_contract(osv.osv):
    _inherit = 'hr.contract'
    
    _columns = {
                'add_wage':fields.float(u'Нэмэгдэл хөлс'),
                }

class shagnaltype(osv.osv):
    _name = 'shagnaltype'
    
    _columns = {
                'name':fields.char(u'Шагналын төрөл', required=True),
                
        }
    
class shiitgeltype(osv.osv):
    _name = 'shiitgeltype'
    
    _columns = {
                'name':fields.char(u'Шийтгэлийн төрөл', required=True),
                
        }
    
class shiitgel(osv.osv):
    _name = 'shiitgel'
    
    _columns = {
                'name':fields.char(u'Шийтгэл', required=True),
                
        }
    
class garsanshaltgaan(osv.osv):
    _name = 'garsanshaltgaan'
    
    _columns = {
                'name':fields.char(u'Ажлаас гарсан шалтгаан', required=True),
                
        }


class tetgemjtype(osv.osv):
    _name = 'tetgemjtype'
    
    _columns = {
                'name':fields.char(u'Тэтгэмжийн төрөл', required=True),
                
        }

class tetgemj(osv.osv):
    _name = 'tetgemj'
    
    _columns = {
                'emp_id':fields.many2one('hr.employee',u'Ажилтан'),
                'tetgemjtype_id':fields.many2one('tetgemjtype',u'Тэтгэмжийн төрөл', required=True),
                'basic_type':fields.selection([('worker',u'Ажилчид'),
                                          ('ondornastan',u'Өндөр настанд'),
                                          ('otherorg',u'Гадны байгууллага')],u'Үндсэн төрөл', required=True),
                'total':fields.integer(u'Тэтгэмжийн дүн', required=True),
                'date':fields.date(u'Огноо',required=True),
                
                
        }

class nemegdel(osv.osv):
    _name = 'nemegdel'
    
    _columns = {
                'emp_id':fields.many2one('hr.employee',u'Ажилтан'),
                'basic_type':fields.selection([('urchadvar',u'Ур чадвар'),
                                          ('udaanjil',u'Удаан жилийн нэмэгдэл'),
                                          ('albantushaal',u'Албан тушаал хавсарсан')],u'Нэмэгдэл төрөл', required=True),
                'date':fields.date(u'Огноо', required=True),
                'note':fields.text(u'Нэмэлт тайлбар'),
                
        }
    

    
    
    
