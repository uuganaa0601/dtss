# -*- coding: utf-8 -*-
##############################################################################
#
#    Tengersoft, Enterprise Management Solution    
#    Copyright (C) 2016-2017 Tengersoft Co.,ltd (<http://www.tengersoft.mn>). All Rights Reserved
#
#    Address : Bayanzurkh District, Borkh center, Tengersoft LLC
#    Email : info@tengersoft.mn
#    Phone : 976 + 99345677
#
##############################################################################

from datetime import timedelta
from lxml import etree
from openerp.osv import fields, osv
from openerp.tools.translate import _
from operator import itemgetter
import xlrd, base64, time, datetime, xlwt, openerp.netsvc, datetime
from StringIO import StringIO

class change_job_position(osv.osv_memory):
    _name = 'change.job.position'
    
    _columns = {
                'employee_id':fields.many2one('hr.employee',u'Шилжих ажилтан'),
                'now_job_id':fields.many2one('hr.job',u'Одоо байгаа албан тушаал'),
                'job_id':fields.many2one('hr.job',u'Шилжих албан тушаал'),
                'change_date':fields.date(u'Огноо',required=True),
                'change_work_id':fields.many2one('res.users',u'Бүртгэлийн ажилтан',readonly=True),
                'company_id':fields.many2one('res.company',u'Компани',required=True,readonly=True)
                }
    
    def _get_employee(self, cr, uid,context=None):
        emp_obj = self.pool.get('hr.employee')
        emp_ids = emp_obj.search(cr, uid, [('user_id', '=', uid)], context=context)
        if emp_ids:
            return emp_ids[0]
    def _get_job(self, cr, uid, context=None):
        emp_obj = self.pool.get('hr.employee')
        job_obj = self.pool.get('hr.job')
        emp_ids = emp_obj.search(cr, uid, [('user_id', '=', uid)], context=context)
        if emp_ids:
            for employee in emp_obj.browse(cr, uid, emp_ids, context=context):
                return  employee.job_id.id

        

    
    _defaults = {
        'company_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),
        'change_date': lambda *a: time.strftime('%Y-%m-%d'),
        'change_work_id':lambda obj, cr, uid, ctx:uid,
        'employee_id':_get_employee,
        'now_job_id':_get_job,
    }
    
    def change_state(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids)
        history_obj = self.pool.get('change.job.history')
        history_obj.create(cr, uid, {
                                    'employee_id':obj.employee_id.id,
                                    'company_id':obj.company_id.id,
                                    'change_date':obj.change_date,
                                    'change_work_id':obj.change_work_id.id,
                                    'job_id':obj.job_id.id,
                                    'old_job_id':obj.now_job_id.id,
                                    })
        
        return True